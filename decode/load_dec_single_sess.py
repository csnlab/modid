import os
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import glob
from constants import *
import pickle
import pandas as pd
from plotting_style import *


"""
This script generates decode figures for individual sessions (as opposed
to combined across sessions, which is done in load_dec_combined.py) 

You need to specify the settings name of the decode 'experiment', together
withe the areas of spikes and lfp (edit: area_spikes and area_lfp can also
be None if you want to run for all available areas). You can additionally 
specify the target_name, animal_id and session_id (if you set them to None, 
plots are generated for all available sessions/targets)

hue_order should be a dictionary where the keys are the names of the plots, 
and, the items are lists with the decode inputs to be included in the plot. 
This allows us to generate plots with different combinations of inputs for 
comparison. 

PLOTS: 

1. Decoding over time. Number of panels depends on the number of keys
in hue_orders
2. Feature importances barplot 
3. Feature importances over time

"""


settings_name = 'june18'

# You can specify an individual animal/session, otherwise we look for all sessions
# for which the decode was run.
animal_id = None # '2009'
session_id = None #'2018-08-22_14-05-50'
area_spikes = None
area_lfp = None
target_name = None #'audioFreqPostNorm'
#target_name = 'visualOriPostNorm'


# plotting parameters
ci=95
plot_decoding_over_time = True
plot_feature_importances_barplot = True
plot_feature_importances_over_time = True
scale_by_population_score = False
discrimination_score_name = 'auc'
plot_format = 'png'
dpi = 400
save_plots = True

# select the combination of inputs you want to plot

# hue_orders = {'rate' : ['rate', 'shuffled_rate'],
#               'phase' : ['lfp', 'shuffled_rate'],
#               'energy' : ['energy', 'shuffled_rate'],
#               'phase_combo' : ['rate+phase', 'rate+shuffled_phase', 'shuffled_rate']}

hue_orders = {'rate' : ['rate', 'shuffled_rate']}


# set up paths
plot_folder = os.path.join(DATA_FOLDER, 'plots', 'dec', settings_name)
results_folder = os.path.join(DATA_FOLDER, 'results', 'dec', settings_name)
result_files = glob.glob('{}/decode*'.format(results_folder))
pars_file = 'parameters_decode_setting_{}.pkl'.format(settings_name)


# to figure which sessions we have run the decode for, we load all the
# results (a bit hacky but takes no time). Then we subselect the ones we
# are interested in
sessions = pd.DataFrame(columns=['animal_id', 'session_id', 'target_name',
                                 'area_spikes', 'area_lfp'])

for file in result_files:
    results_full_path = os.path.join(results_folder, file)
    results = pickle.load(open(results_full_path, 'rb'))

    pars = results['pars']

    sessions.loc[sessions.shape[0], :] = [pars['animal_id'], pars['session_id'],
                                          pars['target_name'], pars['area_spikes'],
                                          pars['area_lfp']]


# subselect sessions:
if area_spikes is not None:
    sessions = sessions[sessions['area_spikes'] == area_spikes]

if area_lfp is not None:
    sessions = sessions[sessions['area_lfp'] == area_lfp]

if target_name is not None:
    sessions = sessions[sessions['target_name'] == target_name]

if animal_id is not None:
    sessions = sessions[sessions['animal_id'] == animal_id]

if session_id is not None:
    sessions = sessions[sessions['session_id'] == session_id]


if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder, exist_ok=True)


for i, row in sessions.iterrows():
    area_spikes = row['area_spikes']
    area_lfp = row['area_lfp']
    animal_id = row['animal_id']
    session_id = row['session_id']
    target_name = row['target_name']

    # LOAD RESULTS
    results_file_name = 'decode_setting_{}_{}_{}_{}_{}_{}.pkl'.format(settings_name,
                                                                      area_spikes, area_lfp,
                                                                      animal_id,
                                                                      session_id, target_name)
    results_full_path = os.path.join(results_folder, results_file_name)
    results = pickle.load(open(results_full_path, 'rb'))

    df = results['decoding_scores']
    impdf = results['unit_scores'][discrimination_score_name]

    if plot_decoding_over_time:
        for hue_name, hue_order in hue_orders.items():
            plot_name = 'dec_{}_{}_{}_{}_{}_{}_{}.{}'.format(hue_name, settings_name,
                                                       area_spikes, area_lfp, animal_id,
                                                        session_id, target_name, plot_format)
            palette = [decoding_inputs_colormap[inp] for inp in hue_order]
            f, ax = plt.subplots(1, 1, figsize=small_decoding_panel_size)
            sns.lineplot(x='time', y='score', hue='input', data=df, ax=ax, ci=ci,
                         hue_order=hue_order, err_kws={'linewidth':0}, palette=palette)
            ax.set_xlabel('Time [s]')
            ax.set_ylabel('Accuracy in decode\n{}'.format(targets_labels_matthijs[target_name].lower()))
            ax.axvline(0, c='grey', alpha=0.8, ls='--')
            ax.set_title('{} - {}'.format(animal_id, session_id))
            ax.legend(loc='upper right')
            sns.despine()
            plt.tight_layout()
            if save_plots:
                f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)


    if plot_feature_importances_barplot:
        plot_name = 'feat_imp_{}_{}_{}_{}_{}_{}.{}'.format(settings_name, area_spikes,
                                                           area_lfp, animal_id,
                                                    session_id, target_name, plot_format)
        f, ax = plt.subplots(1, 1, figsize=[7, 4])
        data = impdf[impdf['time']>=0]
        sns.barplot(data=data, x='feature_name', y='score', hue='feature_type')
        ax.tick_params(axis='x', rotation=90)
        sns.despine()
        ax.set_title('{} - {}'.format(animal_id, session_id))
        ax.set_xlabel('')
        ax.set_ylabel('{}\nfor {}'.format(unit_score_labels[discrimination_score_name],
                                          targets_labels_matthijs[target_name].lower()))
        plt.tight_layout()
        f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)



    if plot_feature_importances_over_time:
        if scale_by_population_score:
            impdf = impdf.groupby(['feature_type', 'feature_name', 'time']).mean().reset_index()
            impdf['score'] = impdf['score'] * impdf['population_score']

        data = impdf[impdf['feature_type'] == 'rate']

        #data['feature_name'] = [int(d[-3:]) for d in data['feature_name']]
        #data = data.groupby(['time', 'feature_type', 'feature_name']).mean().reset_index()

        f, ax = plt.subplots(1, 1, figsize=small_decoding_panel_size_with_legend)

        for feat in data['feature_name'].unique():
            sns.lineplot(x='time', y='score', data=data[data['feature_name']==feat],
                         ax=ax, ci=ci, err_kws={'linewidth': 0}, label=feat)
        ax.set_xlabel('Time [s]')
        ax.set_ylabel('{}\nfor {}'.format(unit_score_labels[discrimination_score_name],
                                          targets_labels_matthijs[target_name].lower()))
        ax.axvline(0, c='grey', alpha=0.8, ls='--')
        ax.set_title('{} - {}'.format(animal_id, session_id))
        box = ax.get_position()
        ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
        ax.legend(loc='center left', bbox_to_anchor=(1, 0.5), frameon=False)
        sns.despine()
        plt.tight_layout(rect=[0, 0, 0.7, 1])


