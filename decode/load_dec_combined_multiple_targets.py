import os
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import glob
from constants import *
import pickle
from plotting_style import *
import pandas as pd

"""
This script generates decode figures which combine results from differernt
sessions (as opposed to plotting individual sessions, which is done in
load_dec_single_sess.py)

You need to specify the settings name of the decode 'experiment', together
withe the name of the decode target and the areas for spikes and lfp. 
You can additionally specify the 
animal_id, if you want to combine only sessions which belong to a specific 
animal. Otherwise, all available sessions are used. 
"""



settings_name = 'PPCcosyne_from0_150_noeq'

# plotting parameters
ci=95
plot_format = 'svg'
dpi = 400
save_plots = True
markers_in_lineplot=False

plot_folder = os.path.join(DATA_FOLDER, 'plots', 'dec', settings_name)
results_folder = os.path.join(DATA_FOLDER, 'results', 'dec', settings_name)
result_files = glob.glob('{}/decode*'.format(results_folder))
pars_file = 'decode_settings_{}.pkl'.format(settings_name)

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder, exist_ok=True)

# to figure which sessions we have run the decode for, we load all the
# results (a bit hacky but takes no time). Then we subselect the ones we
# are interested in
sessions = pd.DataFrame(columns=['animal_id', 'session_id', 'target_name',
                                 'area_spikes', 'area_lfp'])

dfs = []

for file in result_files:
    results_full_path = os.path.join(results_folder, file)
    results = pickle.load(open(results_full_path, 'rb'))

    try:
        df = results['decoding_scores']
        pars = results['pars']
        dfs.append(df)
    except KeyError:
        pass

df = pd.concat(dfs)
df = df.drop(['area_lfp', 'lfp_channel_id'], axis=1)

df['score'] = pd.to_numeric(df['score'])
df['animal_session'] = df['animal_id']+'_'+df['session_id']

# subselect sessions:


sessions = df.drop_duplicates(subset=['animal_id', 'session_id', 'target',
                                      'area_spikes'])
sessions = sessions.drop(['time', 'time_bin', 'input', 'decoder',
                          'score', 'animal_session', 'repeat'],
                         axis=1)

# IMPROVEMENT ABOVE RESPECTIVE SHUFFLE


"""
We first average across repetitions of the k-fold procedure. Then, for every 
animal/session, we compute the difference between decode with input1 and input2
at every time point (e.g. difference between rate decode and shuffled rate decode).

We then have two versions of the figure, one in which we show individual session
lines (coloured by animal), and one in which we aggregate sessions by animal with 
confidence intervals. 
"""
#input1 = 'rate+shuffled_phase'
#input2 = 'rate'

input1 = 'rate'
input2 = 'shuffled_rate'

# --- PLOT 1: OVERVIEW BY TARGET ----------------------------------------------

target_names = ['correctResponse', 'responseSide',
                'hasvisualchange', 'hasaudiochange']

target_names = ['hasvisualchange', 'hasaudiochange']

#target_names = ['audioFreqPostNorm', 'audioFreqChangeNorm']
#AREAS = ['V1']
#target_names = df['target'].unique()
palette = [area_palette[area] for area in AREAS]
n_plots = len(target_names)

f, axes = plt.subplots(2, 3, figsize=[3*small_square_side, 3*small_square_side/2],
                     sharex=True, sharey=True)
ax = axes.flatten(order='F')

for i, target in enumerate(target_names):

    dfx = df[df['target'] == target]

    df2 = dfx.groupby([i for i in dfx.columns if ~np.isin(i, ['repeat', 'score'])]).mean().reset_index()

    x = df2[df2['input'] == input1]
    y = df2[df2['input'] == input2]

    for col in ['animal_id', 'session_id', 'decoder', 'time_bin', 'time']:
        np.testing.assert_array_equal(x[col], y[col])

    dfdiff = x.copy()
    dfdiff['input'] = 'rate_over_shuffled'
    dfdiff['score'] = x['score'].as_matrix() - y['score'].as_matrix()

    dfx = dfdiff[dfdiff['input'] == 'rate_over_shuffled']
    sns.lineplot(x='time', y='score', data=dfx, ax=ax[i], ci=ci,
                 err_kws={'linewidth': 0}, markers=markers_in_lineplot, style='area_spikes',
                 style_order=AREAS,
                 hue='area_spikes',
                 hue_order=AREAS, palette=palette)
    ax[i].set_xlabel('Time [s]')
    ax[i].set_ylabel('Decoding\nimprovement')
    ax[i].axvline(0, c='grey', alpha=0.8, ls='--')
    ax[i].axhline(0, c='grey', alpha=0.8, ls=':')
    #ax.legend(bbox_to_anchor=(1.04, 1))
    ax[i].set_title(targets_labels_matthijs[target])
    ax[i].get_legend().remove()

axes[0, 0].legend(bbox_to_anchor=(1.04, 0.5), loc='center left',
                   frameon=False, title='').texts[0].set_text("")
sns.despine()
plt.tight_layout(rect=(0, 0, 0.8, 1))


plot_name = 'dec_all_sess_all_areas_{}.{}'.format(settings_name, plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi,
            bbox_inches="tight")




# --- PLOT 2: OVERVIEW BY AREA ------------------------------------------------


# target_names = ['visualOriPostChangeNorm', 'visualOriChangeNorm',
#                 'audioFreqPostChangeNorm', 'audioFreqChangeNorm',
#                 'correctResponse', 'trialType']

#target_names = ['correctResponse', 'trialType']
target_names = ['correctResponse', 'responseSide',
                'hasvisualchange', 'hasaudiochange']



palette = [targets_palette_matthijs[target] for target in target_names]

for area in df.area_spikes.unique():

    f, ax = plt.subplots(1, 1, figsize=[5, 2.5])


    df2 = df.groupby([i for i in df.columns if ~np.isin(i, ['repeat',
                                                              'score'])]).mean().reset_index()

    x = df2[df2['input'] == input1]
    y = df2[df2['input'] == input2]

    for col in ['animal_id', 'session_id', 'decoder', 'time_bin', 'time', 'target']:
        np.testing.assert_array_equal(x[col], y[col])

    dfdiff = x.copy()
    dfdiff['input'] = 'rate_over_shuffled'
    dfdiff['score'] = x['score'].as_matrix() - y['score'].as_matrix()

    dfx = dfdiff[dfdiff['input'] == 'rate_over_shuffled']
    sns.lineplot(x='time', y='score', data=dfx, ax=ax, ci=ci,
                 err_kws={'linewidth': 0}, markers=markers_in_lineplot,
                 hue='target',
                 hue_order=target_names, palette=palette)
    ax.set_xlabel('Time [s]')

    ax.legend(bbox_to_anchor=(1.04, 0.5), loc='center left',
                       frameon=False, title='').texts[0].set_text("")

    for ii, tar in enumerate(target_names):
        ax.get_legend().get_texts()[ii+1].set_text(targets_labels_matthijs[tar])
    ax.get_legend().get_texts()[0].set_text('Decoding\ntarget')

    sns.despine()

    ax.set_ylabel('Decoding accuracy\nover shuffled')
    ax.axvline(0, c='grey', alpha=0.8, ls='--')
    ax.axhline(0, c='grey', alpha=0.8, ls=':')
    #ax.legend(bbox_to_anchor=(1.04, 1))
    plt.tight_layout(rect=(0, 0, 0.67, 1))

    plot_name = 'dec_all_sess_area_{}_{}.{}'.format(area, settings_name,
                                                      plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi,
              bbox_inches="tight")

if False:
    # --- SINGLE UNITS --------------------------------------------------------
    import itertools
    from sklearn import preprocessing
    scale_by_population_score = False
    discrimination_score = 'auc'

    impdfs = []

    for file in result_files:
        results_full_path = os.path.join(results_folder, file)
        results = pickle.load(open(results_full_path, 'rb'))

        try:
            df = results['unit_scores'][discrimination_score]
            pars = results['pars']
            impdfs.append(df)
        except KeyError:
            pass

    impdf = pd.concat(impdfs)

    impdf = impdf[(impdf['time'] >= 0) & (impdf['time'] < 0.4)]

    impdf = impdf.groupby(['animal_id', 'session_id', 'area_spikes', 'feature_name', 'target_name']).mean().reset_index()

    if scale_by_population_score:
        impdf['score'] = impdf['score'] * impdf['population_score']



    target_names = ['visualOriPostNorm', 'visualOriChangeNorm',
                    'audioFreqPostNorm', 'audioFreqChangeNorm',
                    'trialType', 'correctResponse']

    mat = np.zeros([len(target_names), len(target_names)])



    areas = impdf['area_spikes'].unique()

    overlap_mats = {k : np.zeros([len(target_names), len(target_names)]) for k in areas}
    n_mats = {k : np.zeros([len(target_names), len(target_names)]) for k in areas}


    for i, row in sessions.drop_duplicates(subset=['animal_id', 'session_id', 'area_spikes']).iterrows():
        animal_id = row['animal_id']
        session_id = row['session_id']
        area = row['area_spikes']

        df_sel = impdf[(impdf['animal_id'] == animal_id)
                     & (impdf['session_id'] == session_id)
                     & (impdf['area_spikes'] == area)]
        targets = df_sel['target_name'].unique()

        combos = list(itertools.combinations(targets, 2))

        for tar1, tar2 in combos:

            dftar1 = df_sel[df_sel['target_name'] == tar1]
            dftar2 = df_sel[df_sel['target_name'] == tar2]

            np.testing.assert_array_equal(dftar1['feature_name'].values,
                                          dftar2['feature_name'].values)

            vec1 = dftar1['score'].values
            vec2 = dftar2['score'].values

            vec1 = preprocessing.normalize(vec1.reshape(-1, 1), norm='l2', axis=0)
            vec2 = preprocessing.normalize(vec2.reshape(-1, 1), norm='l2', axis=0)

            overlap = np.nansum(vec1 * vec2) / vec1.shape[0]

            overlap_mats[area][target_names.index(tar1), target_names.index(tar2)] += overlap
            overlap_mats[area][target_names.index(tar2), target_names.index(tar1)] += overlap

            n_mats[area][target_names.index(tar1), target_names.index(tar2)] += 1
            n_mats[area][target_names.index(tar2), target_names.index(tar1)] += 1


    ticklabels = [targets_labels_matthijs[t] for t in target_names]
    f, ax = plt.subplots(1, 3, sharex=True, sharey=True, figsize=[6*small_square_side, 2*small_square_side])

    for i, area in enumerate(areas):
        overlap_mat_mean = overlap_mats[area] / n_mats[area]
        sns.heatmap(overlap_mat_mean, ax=ax[i], cmap=area_cmap[area], annot=True,  linewidths=.8,
                    xticklabels=ticklabels, yticklabels=ticklabels)
    for axx in ax:
        axx.tick_params(axis=u'both', which=u'both',length=0)
        axx.set_xticklabels(ticklabels, rotation=40)

    plt.tight_layout()



    # an alternative method

    overlap_mats = {k : np.zeros([len(target_names), len(target_names)]) for k in areas}
    n_mats = {k : np.zeros([len(target_names), len(target_names)]) for k in areas}

    unit_ids = impdf['feature_name'].unique()

    for unit_id in unit_ids:
        df_sel = impdf[impdf['feature_name'] == unit_id]
        targets = df_sel['target_name'].values
        area = df_sel['area_spikes'].iloc[0]

        combos = list(itertools.combinations(targets, 2))

        for tar1, tar2 in combos:
            sc1 = df_sel.loc[df_sel['target_name'] == tar1, 'score'].values \
                  #* df_sel.loc[df_sel['target_name'] == tar1, 'population_score'].values
            sc2 = df_sel.loc[df_sel['target_name'] == tar2, 'score'].values \
                  #* df_sel.loc[df_sel['target_name'] == tar2, 'population_score'].values

            overlap_mats[area][target_names.index(tar1), target_names.index(tar2)] += (sc1*sc2)[0]
            overlap_mats[area][target_names.index(tar2), target_names.index(tar1)] += (sc1*sc2)[0]

            n_mats[area][target_names.index(tar1), target_names.index(tar2)] += 1
            n_mats[area][target_names.index(tar2), target_names.index(tar1)] += 1


    ticklabels = [targets_labels_matthijs[t] for t in target_names]
    f, ax = plt.subplots(1, 3, sharex=True, sharey=True, figsize=[6*small_square_side, 2*small_square_side])

    for i, area in enumerate(areas):
        overlap_mat_mean = overlap_mats[area] / n_mats[area]
        sns.heatmap(overlap_mat_mean, ax=ax[i], cmap=area_cmap[area], annot=True,  linewidths=.8,
                    xticklabels=ticklabels, yticklabels=ticklabels)
    for axx in ax:
        axx.tick_params(axis=u'both', which=u'both',length=0)
        axx.set_xticklabels(ticklabels, rotation=40)

    plt.tight_layout()