import os
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import glob
from constants import *
import pickle
from plotting_style import *
import pandas as pd

"""
This script generates decode figures which combine results from differernt
sessions (as opposed to plotting individual sessions, which is done in
load_dec_single_sess.py)

You need to specify the settings name of the decode 'experiment', together
withe the name of the decode target and the areas for spikes and lfp. 
You can additionally specify the 
animal_id, if you want to combine only sessions which belong to a specific 
animal. Otherwise, all available sessions are used. 


PLOTS (further described below)
1. Plot decode time aggregate
2. Decoding over time multiple sessions
3. Decoding improvement over shuffled over time ()

"""


settings_name = 'june28phase'

# You can specify an individual session, otherwise we look for all sessions
# for which the decode was run.
animal_id = None #'2009'
session_id = None#'2018-08-22_14-05-50'
area_spikes = 'V1'
area_lfp = 'V1'
target_name = 'visualOriPostNorm'
#target_name = 'audioFreqPostNorm'
#target_name = 'audioFreqChangeNorm'

# plotting parameters
ci=95
plot_format = 'png'
dpi = 400
save_plots = True
markers_in_lineplot=True

# the decode inputs for the plots
inputs = [('rate', 'shuffled_rate'),
          ('rate+phase', 'rate+shuffled_phase'),
          ('rate', 'rate+phase')]

# --- SET UP PATHS ------------------------------------------------------------

plot_folder = os.path.join(DATA_FOLDER, 'plots', 'dec', settings_name)
results_folder = os.path.join(DATA_FOLDER, 'results', 'dec', settings_name)
result_files = glob.glob('{}/decode*'.format(results_folder))
pars_file = 'decode_settings_{}.pkl'.format(settings_name)



# to figure which sessions we have run the decode for, we load all the
# results (a bit hacky but takes no time). Then we subselect the ones we
# are interested in
sessions = pd.DataFrame(columns=['animal_id', 'session_id', 'target_name',
                                 'area_spikes', 'area_lfp'])

for file in result_files:
    results_full_path = os.path.join(results_folder, file)
    results = pickle.load(open(results_full_path, 'rb'))

    pars = results['pars']

    sessions.loc[sessions.shape[0], :] = [pars['animal_id'], pars['session_id'],
                                          pars['target_name'], pars['area_spikes'],
                                          pars['area_lfp']]


# subselect sessions:
sessions = sessions[sessions['target_name'] == target_name]
sessions = sessions[sessions['area_spikes'] == area_spikes]
sessions = sessions[sessions['area_lfp'] == area_lfp]

if animal_id is not None:
    sessions = sessions[sessions['animal_id'] == animal_id]


if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder, exist_ok=True)


# LOAD AND CONCATENATE DATA
dfs = []
for i, row in sessions.iterrows():
    animal_id = row['animal_id']
    session_id = row['session_id']

    results_file_name = 'decode_setting_{}_{}_{}_{}_{}_{}.pkl'.format(settings_name,
                                                                      area_spikes, area_lfp,
                                                                      animal_id,
                                                                      session_id, target_name)
    print(results_file_name)
    results_full_path = os.path.join(results_folder, results_file_name)
    results = pickle.load(open(results_full_path, 'rb'))
    df = results['decoding_scores']
    dfs.append(df)


df = pd.concat(dfs)
df['score'] = pd.to_numeric(df['score'])
df['animal_session'] = df['animal_id']+'_'+df['session_id']

if df['area_lfp'][0] is None:
    df = df.drop(['area_lfp', 'lfp_channel_id'], axis=1)



# --- PLOT DECODING OVER TIME AGGREGATE ---------------------------------------
"""
In this plot we simply aggregate the decode performance over time across
all sessions/animals for different decode inputs
"""

for input1, input2 in inputs:

    dfx = df[np.isin(df['input'], (input1, input2))]
    palette = [decoding_inputs_colormap[c] for c in (input1, input2)]

    f, ax = plt.subplots(1, 1, figsize=small_decoding_panel_size)
    sns.lineplot(x='time', y='score', hue='input', data=dfx, ax=ax, ci=ci,
                 err_kws={'linewidth': 0}, palette=palette, markers=False)

    ax.set_xlabel('Time [s]')
    ax.axvline(0, c='grey', alpha=0.8, ls='--')
    #ax.legend(bbox_to_anchor=(1.04, 1), frameon=False)
    ax.legend()
    sns.despine()
    plt.tight_layout()


# --- PLOT DECODING OVER TIME MULTIPLE SESSIONS -------------------------------
"""
In this plot we show decode per session (bars indicate confidence intervals
of the repetitions of the k-fold procedure). We can also add a shuffled decode, 
which is then aggregated across all sessions (to avoid having one shuffled performance
per session, which would be too much).
"""


for input1, input2 in inputs:

    dfx = df[df['input'] == input1]
    palette = [animals_colormap[an] for an in sessions['animal_id']]

    f, ax = plt.subplots(1, 1, figsize=small_decoding_panel_size)
    sns.lineplot(x='time', y='score', hue='animal_session', data=dfx, ax=ax, ci=ci,
                 err_kws={'linewidth': 0}, palette=palette, markers=False,
                 style='area_spikes')
    if input2 is not None:
        dfy = df[df['input'] == input2]
        sns.lineplot(x='time', y='score', hue='input', data=dfy, ax=ax, ci=ci,
                     err_kws={'linewidth': 0}, palette=[shuffle_grey])

    ax.set_xlabel('Time [s]')
    ax.set_ylabel('Accuracy in decode\n{}'.format(targets_labels_matthijs[target_name].lower()))
    ax.set_title('Input: {}\nShuffle: {}'.format(input1, input2))
    ax.axvline(0, c='grey', alpha=0.8, ls='--')
    ax.legend(bbox_to_anchor=(1.04, 1), frameon=False)
    sns.despine()
    plt.tight_layout()

    plot_name = 'dec_combined_{}_{}_{}_{}_{}.{}'.format(input1, settings_name,
                                                  area_spikes, area_lfp,
                                                  target_name,
                                                  plot_format)
    plt.savefig(os.path.join(plot_folder, plot_name), dpi=dpi, bbox_inches="tight")




# --- PLOT DECODING OVER TIME IMPROVEMENT ABOVE RESPECTIVE SHUFFLE ------------
"""
We first average across repetitions of the k-fold procedure. Then, for every 
animal/session, we compute the difference between decode with inputs[0] and inputs[1]
at every time point (e.g. difference between rate decode and shuffled rate decode).

We then have two versions of the figure, one in which we show individual session
lines (coloured by animal), and one in which we aggregate sessions by animal with 
confidence intervals. 
"""


#df = df.drop(['area_lfp', 'lfp_channel_id'], axis=1)

df2 = df.groupby([i for i in df.columns if ~np.isin(i, ['repeat', 'score'])]).mean().reset_index()


for input1, input2 in inputs:
    x = df2[df2['input'] == input1]
    y = df2[df2['input'] == input2]

    for col in ['animal_id', 'session_id', 'decoder', 'time_bin', 'time']:
        np.testing.assert_array_equal(x[col], y[col])

    dfdiff = x.copy()
    input_name = '{}_over_{}'.format(input1, input2)
    dfdiff['input'] = input_name
    dfdiff['score'] = x['score'].as_matrix() - y['score'].as_matrix()


    plot_name = 'dec_combined_diff_singlelines_{}_{}_{}_{}_{}_{}.{}'.format(input1, input2, settings_name,
                                                          area_spikes, area_lfp,
                                                          target_name, plot_format)
    palette = [animals_colormap[an] for an in sessions['animal_id']]
    dfx = dfdiff[dfdiff['input'] == input_name]
    f, ax = plt.subplots(1, 1, figsize=small_decoding_panel_size)
    sns.lineplot(x='time', y='score', hue='animal_session', data=dfx, ax=ax, ci=ci,
                 err_kws={'linewidth': 0}, palette=palette, style='area_spikes',
                 markers=False)
    ax.set_xlabel('Time [s]')
    ax.set_ylabel('Decoding {}\nimprovement of {}\nover {}'.format(
                  targets_labels_matthijs[target_name].lower(), input1, input2))
    ax.axvline(0, c='grey', alpha=0.8, ls='--')
    ax.axhline(0, c='grey', alpha=0.8, ls=':')
    ax.legend(bbox_to_anchor=(1.04, 1))
    sns.despine()
    plt.tight_layout()
    plt.savefig(os.path.join(plot_folder, plot_name), dpi=dpi, bbox_inches="tight")


    plot_name = 'dec_combined_diff_agg_{}_{}_{}_{}_{}_{}.{}'.format(input1, input2, settings_name,
                                                              area_spikes, area_lfp,
                                                          target_name, plot_format)
    palette = [animals_colormap[an] for an in np.unique(dfdiff['animal_id'])]
    dfx = dfdiff[dfdiff['input'] == input_name]
    f, ax = plt.subplots(1, 1, figsize=small_decoding_panel_size)
    sns.lineplot(x='time', y='score', hue='animal_id', data=dfx, ax=ax, ci=ci,
                 err_kws={'linewidth': 0}, palette=palette, style='area_spikes',
                 markers=markers_in_lineplot)
    ax.set_xlabel('Time [s]')
    ax.set_ylabel('Decoding {}\nimprovement of {}\nover {}'.format(
                  targets_labels_matthijs[target_name].lower(), input1, input2))
    ax.axvline(0, c='grey', alpha=0.8, ls='--')
    ax.axhline(0, c='grey', alpha=0.8, ls=':')
    ax.legend(bbox_to_anchor=(1.04, 1))
    sns.despine()
    plt.tight_layout()
    plt.savefig(os.path.join(plot_folder, plot_name), dpi=dpi, bbox_inches="tight")




