from constants import *
import subprocess
from session_info import select_sessions
import pickle

settings_name = 'May27b' # avoid '_' in the settings name
decoding_inputs_mode = 'rate_only'
only_correct = False
min_trials_per_class = 30
min_units = 15
area_lfp = 'nolfp'
binsize_in_ms = 200
slide_by_in_ms = 200
min_perc_correct = 40

sessions = select_sessions(min_trials_per_class=min_trials_per_class,
                           only_correct=only_correct,
                           min_perc_correct=min_perc_correct,
                           min_units=min_units)


# --- SAVE SESSION SELECTION --------------------------------------------------

output_folder = os.path.join(DATA_FOLDER, 'results', 'dec', settings_name)
output_full_path = os.path.join(output_folder, 'session_selection_{}.pkl'.format(settings_name))

session_sel_pars = {'min_trials_per_class' : min_trials_per_class,
                    'only_correct' : only_correct,
                    'min_units' : min_units,
                    'min_perc_correct' : min_perc_correct}

out = {'session_selection_pars' : session_sel_pars,
       'selected_sessions' : sessions}


if not os.path.isdir(output_folder):
    os.makedirs(output_folder, exist_ok=True)


pickle.dump(out, open(output_full_path, 'wb'))


# --- DECODE ------------------------------------------------------------------

for i, row in sessions.iterrows():
    animal_id = row['animal_id']
    session_id = row['session_id']
    target_name = row['target_name']
    only_correct = row['only_correct']
    area_spikes = row['area']

    if area_lfp == 'nolfp':
        pass
    elif area_lfp == 'same_as_spikes':
        area_lfp = area_spikes

    print(animal_id)
    print(session_id)
    print(target_name)

    args = '--settings_name {} --decoding_inputs_mode {} ' \
           '--animal_id {} --session_id {} ' \
           '--target_name {} ' \
           '--only_correct_trials {} '\
           '--area_spikes {} '\
           '--area_lfp {} '\
           '--binsize_in_ms {} '\
           '--slide_by_in_ms {}'.format(settings_name, decoding_inputs_mode,
                                  animal_id, session_id,
                                  target_name, only_correct,
                                  area_spikes, area_lfp, binsize_in_ms, slide_by_in_ms)
    print(args)
    subprocess.call('python ./compute_dec.py {}'.format(args), shell=True)