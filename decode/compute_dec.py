import numpy as np
import pandas as pd
from constants import *
from loadmat import *
from session import Session
from sklearn.model_selection import StratifiedKFold
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score, recall_score
import pickle
import quantities as pq
from utils import shuffle_array_rows_within_labels
from sklearn.ensemble import ExtraTreesClassifier
import distutils
from unit_scores import SingleUnitDiscriminationScores
from sklearn.preprocessing import LabelEncoder

try:
    __IPYTHON__

except NameError:
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('--settings_name', type=str)
    parser.add_argument('--decoding_inputs_mode', type=str)
    parser.add_argument('--animal_id', type=str)
    parser.add_argument('--session_id', type=str)
    parser.add_argument('--target_name', type=str)
    parser.add_argument('--only_correct_trials', type=distutils.util.strtobool, default=0)
    parser.add_argument('--area_spikes', type=str)
    parser.add_argument('--area_lfp', type=str)
    parser.add_argument('--binsize_in_ms', type=int)
    parser.add_argument('--slide_by_in_ms', type=int)

    ARGS = parser.parse_args()

    settings_name = ARGS.settings_name
    decoding_inputs_mode = ARGS.decoding_inputs_mode
    animal_id = ARGS.animal_id
    session_id = ARGS.session_id
    target_name = ARGS.target_name
    only_correct_trials = ARGS.only_correct_trials
    area_spikes = ARGS.area_spikes
    area_lfp = ARGS.area_lfp
    binsize_in_ms = ARGS.binsize_in_ms
    slide_by_in_ms = ARGS.slide_by_in_ms

else:
    settings_name = 'may10'
    decoding_inputs_mode = 'rate_only'
    animal_id = '2009'
    session_id = '2018-08-22_14-05-50'
    target_name = 'trialType'  # 'audioFreqPostNorm' 'visualOriPostNorm
    only_correct_trials = False
    area_spikes = 'V1'
    area_lfp = 'V1'
    binsize_in_ms = 100
    slide_by_in_ms = 100



# TIME PARAMETERS
sliding_window = True
time_before_stim_in_s = 0.4
time_after_stim_in_s = 0.8

# FILTERING PARAMETERS
transition_width = 25  # Hz
bandpass_attenuation = 60  # dB
low_freq = 3
mid_freq = 43
high_freq = 83
spacing_low = 20
spacing_high = 20

# DISCRETIZATION
discretize_phase = False
discretize_energy = False
n_phase_bins = 8
n_energy_bins = 8


if np.isin(target_name, ['visualOriPostNorm', 'visualOriChangeNorm']):
    trial_type = 'X'

elif np.isin(target_name, ['audioFreqPostNorm', 'audioFreqChangeNorm']):
    trial_type = 'Y'

elif np.isin(target_name, ['correctResponse', 'trialType']):
    trial_type = ['X', 'Y']


auditory_change=None
auditory_post = None
visual_change = None

# SELECT UNITS

# SELECT CHANNELS
layer_lfp = None
ch_in_the_middle_of_the_probe = True

# SELECT TARGET VARIABLE
join_small_large_stim = True

# DECODING PARAMETERS
decoder_name = 'random_forest'
n_estimators = 100
n_splits = 3
n_repeats = 5
n_shuffles = 20
score_name = 'accuracy'
shuffle_kfold=True

# SINGLE UNIT SCORES PARAMETERS
discrimination_score_names = ['feature_importance', 'auc']#['jackknife', 'auc', 'feature_importance']
n_repeats_scores = 3

"""
decode inputs can be simple (e.g. 'rate'), combined (e.g. 'rate+phase'), 
padded (e.g. 'rate+pad_phase') if you want to decode only with
if you want the dimensionality to be consistent with 'rate+phase'
"""


if decoding_inputs_mode == 'rate_only':
    decoding_inputs = ['rate']
    decoding_inputs_control = ['shuffled_rate']

elif decoding_inputs_mode == 'rate_phase_energy':
    decoding_inputs = ['rate', 'phase', 'energy', 'rate+phase']
    decoding_inputs_control = ['shuffled_rate', 'shuffled_phase', 'shuffled_energy',
                               'rate+shuffled_phase']


# figure out if we need to use the lfp
if any('phase' in st for st in decoding_inputs) or any('energy' in st for st in decoding_inputs):
    process_lfp = True
else:
    process_lfp    = False
    area_lfp       = 'nolfp'
    lfp_channel_id = 'nolfp'
    freq_bands     = 'nolfp'


if isinstance(discrimination_score_names, str):
    discrimination_score_names = [discrimination_score_names]


# --- SET UP PATHS ------------------------------------------------------------

output_file_name = 'decode_setting_{}_{}_{}_{}_{}_{}.pkl'.format(settings_name,
                                                            area_spikes, area_lfp,
                                                            animal_id,
                                                            session_id, target_name)

output_folder    = os.path.join(DATA_FOLDER, 'results', 'dec', settings_name)
output_full_path = os.path.join(output_folder, output_file_name)

# we also save a file with just the settings for ease of access later
pars_full_path   = os.path.join(output_folder, 'parameters_decode_setting_{}.pkl'.format(settings_name))

if not os.path.isdir(output_folder):
    os.makedirs(output_folder)


# --- PREPARE DATA ------------------------------------------------------------

session = Session(animal_id=animal_id, session_id=session_id)


session.load_data(load_lfp=process_lfp)

trial_numbers = session.select_trials(only_correct=only_correct_trials,
                                      trial_type=trial_type,
                                      auditory_change=auditory_change,
                                      auditory_post=auditory_post,
                                      visual_change=visual_change)

trial_times = session.get_aligned_times(trial_numbers,
                                        time_before_in_s=time_before_stim_in_s,
                                        time_after_in_s=time_after_stim_in_s)


binned_spikes, spike_bin_centers = session.bin_spikes_per_trial(binsize_in_ms, trial_times,
                                                                sliding_window=sliding_window,
                                                                slide_by_in_ms=slide_by_in_ms)

if process_lfp:
    session.quick_downsample_lfp(factor=2)

    freq_bands = session.make_frequency_bands(low_freq, mid_freq, high_freq, spacing_low, spacing_high)

    session.set_filter_parameters(freq_bands, transition_width, bandpass_attenuation)

    lfp_channel_id = session.get_random_channel_id(area=area_lfp,
                                                   layer=layer_lfp,
                                                   in_the_middle=ch_in_the_middle_of_the_probe)

    session.bandpass_filter(lfp_channel_id)

    interp_phase, interp_energy = session.interpolate_phase_and_energy_per_trial(spike_bin_centers)

    if discretize_phase:
        binned_phase = session.discretize_phase(interp_phase, n_bins=n_phase_bins)
    else:
        binned_phase = interp_phase

    if discretize_energy:
        binned_energy = session.discretize_energy(interp_energy, n_bins=n_energy_bins)
    else:
        binned_energy = interp_energy


selected_unit_ind = session.select_units(area=area_spikes)

selected_unit_id = session.get_cell_id(selected_unit_ind, shortened_id=False)

binned_spikes = [s[selected_unit_ind, :] for s in binned_spikes]

n_trials = len(trial_numbers)
n_time_bins_per_trial = spike_bin_centers[0].shape[0]
time_bin_times = (spike_bin_centers[0] - spike_bin_centers[0][0]).rescale(pq.s) \
                 + (binsize_in_ms*pq.ms).rescale(pq.s)/2 - time_before_stim_in_s*pq.s
time_bin_times = time_bin_times.__array__().round(5)

y = session.make_target(trial_numbers, target_name=target_name,
                        coarse=join_small_large_stim)

ll = LabelEncoder()
y = ll.fit_transform(y)



# --- SET UP UTILITIES --------------------------------------------------------


def get_scoring_function(score_name='accuracy'):
    if score_name != 'accuracy':
        raise NotImplementedError
    else:
        return accuracy_score


def get_kfold_score(X, y, decoder, n_splits, seed, shuffle, score_name,
                    average_fold_scores=True, verbose=0):

    kfold = StratifiedKFold(n_splits=n_splits, shuffle=shuffle,
                            random_state=seed)
    kfold_scores = []
    y_test_all, y_pred_all = [], []

    for fold, (training_ind, testing_ind) in enumerate(kfold.split(X, y)):

        X_train = X[training_ind, :]
        X_test  = X[testing_ind, :]
        y_train = y[training_ind]
        y_test  = y[testing_ind]

        decoder.fit(X_train, y_train)
        y_pred = decoder.predict(X_test)
        scoring_function = get_scoring_function(score_name=score_name)
        score = scoring_function(y_test, y_pred)
        kfold_scores.append(score)
        y_test_all.append(y_test)
        y_pred_all.append(y_pred)
        if verbose > 0:
            print('--- Fold {}/{} - {}: {}'.format(fold, n_splits, score_name,
                                                   score))

    y_test_all = np.hstack(y_test_all)
    y_pred_all = np.hstack(y_pred_all)

    if average_fold_scores:
        kfold_scores = np.mean(kfold_scores)

    return kfold_scores, y_test_all, y_pred_all



def get_decoding_features(decoding_input, spikes, phase=None, energy=None, seed=None):

    """
    Utility function to get the inputs for the decoder by passing a
    decoding_input string. For padded inputs, rate is always first.
    """

    spikes_pad = np.zeros_like(spikes)

    if phase is not None:
        phase_pad = np.zeros_like(phase)
    if energy is not None:
        energy_pad = np.zeros_like(energy)

    if decoding_input == 'rate':
        X = spikes

    elif decoding_input == 'phase':
        X = phase

    elif decoding_input == 'energy':
        X = energy

    elif decoding_input == 'rate+phase':
        X = np.hstack((spikes, phase))

    elif decoding_input == 'rate+pad_phase':
        X = np.hstack((spikes, phase_pad))

    elif decoding_input == 'phase+pad_rate':
        X = np.hstack((spikes_pad, phase))

    elif decoding_input == 'rate+energy':
        X = np.hstack((spikes, energy))

    elif decoding_input == 'rate+pad_energy':
        X = np.hstack((spikes, energy_pad))

    elif decoding_input == 'energy+pad_rate':
        X = np.hstack((spikes_pad, energy))

    # TODO do we want to seed the shuffling?
    elif decoding_input == 'shuffled_rate':
        shuffled_ind = np.random.permutation(np.arange(S.shape[0]))
        X = spikes[shuffled_ind, :]

    elif decoding_input == 'shuffled_phase':
        shuffled_ind = np.random.permutation(np.arange(S.shape[0]))
        X = phase[shuffled_ind, :]

    elif decoding_input == 'shuffled_energy':
        shuffled_ind = np.random.permutation(np.arange(S.shape[0]))
        X = energy[shuffled_ind, :]

    elif decoding_input == 'rate+shuffled_phase':
        phase_shuffled = shuffle_array_rows_within_labels(phase, y, seed=seed)
        X = np.hstack((spikes, phase_shuffled))

    else:
        raise NotImplementedError

    return X



# --- DECODING ----------------------------------------------------------------


df = pd.DataFrame(columns=['animal_id', 'session_id', 'input', 'decoder', 'time_bin', 'time',
                           'target', 'area_spikes', 'area_lfp', 'lfp_channel_id',
                           'repeat', 'score'])


rs = {inp : {'true' : np.zeros([n_repeats, n_trials]),
             'pred' : np.zeros([n_repeats, n_trials])}
      for inp in decoding_inputs}

for decoding_input in decoding_inputs_control:
    rs[decoding_input] = {'true' : np.zeros([n_shuffles, n_trials]),
                          'pred' : np.zeros([n_shuffles, n_trials])}


print('\n>>> Starting the decode ({} samples)\n'.format(y.shape[0]))

for time_bin in range(n_time_bins_per_trial):

    time = time_bin_times[time_bin]

    S = np.vstack([s[:, time_bin] for s in binned_spikes])

    if process_lfp:
        P = np.vstack([s[:, time_bin] for s in binned_phase[lfp_channel_id]])
        E = np.vstack([s[:, time_bin] for s in binned_energy[lfp_channel_id]])
    else:
        P = None
        E = None

    for decoding_input in decoding_inputs:

        print('\n\nTime bin {} of {} Decoding with input: '
              '{}'.format(time_bin, n_time_bins_per_trial, decoding_input))

        X = get_decoding_features(decoding_input, spikes=S, phase=P, energy=E)

        for repeat in range(n_repeats):

            print('- Repeat {}/{}'.format(repeat, n_repeats))

            if decoder_name == 'random_forest':
                decoder = RandomForestClassifier(n_estimators=n_estimators)
            else:
                raise NotImplementedError

            score, targ_true, targ_pred = get_kfold_score(X, y, decoder, n_splits=n_splits,
                                                seed=repeat, shuffle=shuffle_kfold,
                                                score_name=score_name)

            row = [animal_id, session_id, decoding_input, decoder_name,
                   time_bin, time,target_name, area_spikes, area_lfp,
                   lfp_channel_id, repeat, score]
            df.loc[df.shape[0], :] = row

            rs[decoding_input]['true'][repeat, :] = targ_true
            rs[decoding_input]['pred'][repeat, :] = targ_pred


    for decoding_input in decoding_inputs_control:

        print('\n\nTime bin {} of {} Decoding with input: '
              '{}'.format(time_bin, n_time_bins_per_trial, decoding_input))


        for repeat in range(n_shuffles):

            print('- Repeat {}/{} [SHUFFLES]'.format(repeat, n_shuffles))

            if decoder_name == 'random_forest':
                decoder = RandomForestClassifier(n_estimators=n_estimators)
            else:
                raise NotImplementedError

            X = get_decoding_features(decoding_input, spikes=S, phase=P, energy=E)

            score, targ_true, targ_pred = get_kfold_score(X, y, decoder, n_splits=n_splits,
                                         seed=repeat, shuffle=shuffle_kfold,
                                         score_name=score_name)

            row = [animal_id, session_id, decoding_input, decoder_name,
                   time_bin, time, target_name, area_spikes, area_lfp,
                   lfp_channel_id, repeat, score]
            df.loc[df.shape[0], :] = row

            rs[decoding_input]['true'][repeat, :] = targ_true
            rs[decoding_input]['pred'][repeat, :] = targ_pred

numeric_cols = ['score']

for col in numeric_cols:
    df[col] = pd.to_numeric(df[col])


# --- FEATURE IMPORTANCES -----------------------------------------------------


if process_lfp:
    freq_bands_str = ['{}-{}'.format(f[0], f[1]) for f in freq_bands]
    feature_names = np.hstack((selected_unit_id, freq_bands_str, freq_bands_str))
    feature_types = ['rate' for _ in range(len(selected_unit_id))] + \
                    ['phase' for _ in range(len(freq_bands_str))] + \
                    ['energy' for _ in range(len(freq_bands_str))]

else:
    feature_names = selected_unit_id
    feature_types = ['rate' for _ in range(len(selected_unit_id))]


unit_scores = {}

for discrimination_score_name in discrimination_score_names:

    sudf = pd.DataFrame(columns=['animal_id', 'session_id', 'area_spikes', 'area_lfp',
                                 'feature_type', 'feature_name', 'target_name',
                                 'time', 'repeat', 'score', 'population_score'])

    for time_bin in range(n_time_bins_per_trial):

        time = time_bin_times[time_bin]

        for repeat in range(n_repeats_scores):

            print('Computing feature importances - time bin {:03}/{:03}'
                  ' repeat {:02}/{:02}'.format(time_bin, n_time_bins_per_trial,
                                               repeat, n_repeats))
            S = np.vstack([s[:, time_bin] for s in binned_spikes])

            if process_lfp:
                P = np.vstack([s[:, time_bin] for s in binned_phase[lfp_channel_id]])
                E = np.vstack([s[:, time_bin] for s in binned_energy[lfp_channel_id]])
                X = np.hstack((S, P, E))

            else:
                X = S

            sud = SingleUnitDiscriminationScores(decoder=decoder_name,
                                                 n_estimators=n_estimators)

            if discrimination_score_name == 'jackknife':
                scores = sud.compute_jackknife_scores(X, y, n_splits=n_splits,
                                                      random_state=repeat,
                                                      return_population_score=False)

            elif discrimination_score_name == 'feature_importance':
                scores = sud.compute_importance_scores(X, y, n_estimators=n_estimators,
                                                       random_state=repeat,
                                                       return_population_score=False)

            elif discrimination_score_name == 'auc':
                scores = sud.compute_auc_scores(X, y)

            pop_score = 0

            for ftype, fname, fimp in zip(feature_types, feature_names, scores):
                sudf.loc[sudf.shape[0], :] = [animal_id, session_id, area_spikes,
                                              area_lfp, ftype,
                                              fname, target_name, time, repeat,
                                              fimp, pop_score]


    numeric_cols = ['score', 'population_score']

    for col in numeric_cols:
        sudf[col] = pd.to_numeric(sudf[col])

    # if we have run the actual decode, we assign the actual decode scores
    # to the feature scores data frame

    dfm = df.groupby(['animal_id', 'session_id', 'input', 'decoder', 'time_bin', 'time',
                'target', 'area_spikes', 'area_lfp', 'lfp_channel_id']).mean().reset_index()

    pop_dec = dfm.loc[dfm['input'] == 'rate', 'score'].as_matrix()
    pop_dec_shuf = dfm.loc[dfm['input'] == 'shuffled_rate', 'score'].as_matrix()
    pop_dec_above_shuf = pop_dec - pop_dec_shuf

    np.testing.assert_array_equal(dfm.loc[dfm['input'] == 'rate', 'time'],
                                  dfm.loc[dfm['input'] == 'shuffled_rate', 'time'])

    for i, time in enumerate(time_bin_times):

        sudf.loc[sudf['time'] == time, 'population_score'] = pop_dec_above_shuf[i]

    sudf['score_name'] = discrimination_score_name
    unit_scores[discrimination_score_name] = sudf


# --- COLLECT PARS ------------------------------------------------------------

pars = {'settings_name' : settings_name,
        'animal_id' : animal_id,
        'session_id' : session_id,
        'binsize_in_ms' : binsize_in_ms,
        'time_before_stim_in_s' : time_before_stim_in_s,
        'time_after_stim_in_s' : time_after_stim_in_s,
        'transition_width' : transition_width,
        'bandpass_attenuation' : bandpass_attenuation,
        'low_freq' : low_freq,
        'mid_freq' : mid_freq,
        'high_freq' : high_freq,
        'spacing_low' : spacing_low,
        'spacing_high' : spacing_high,
        'n_phase_bins' : n_phase_bins,
        'n_energy_bins' : n_energy_bins,
        'only_correct_trials' : only_correct_trials,
        'trial_type' : trial_type,
        'auditory_change' : auditory_change,
        'auditory_post' : auditory_post,
        'area_spikes' : area_spikes,
        'area_lfp' : area_lfp,
        'layer_lfp' : layer_lfp,
        'target_name' : target_name,
        'decoder_name' : decoder_name,
        'n_estimators' : n_estimators,
        'n_splits' : n_splits,
        'n_repeats' : n_repeats,
        'n_shuffles' : n_shuffles,
        'score_name' : score_name,
        'shuffle_kfold' : shuffle_kfold,
        'decoding_inputs' : decoding_inputs,
        'decoding_inputs_control' : decoding_inputs_control,
        # not strictly user defined
        'trial_numbers' : trial_numbers,
        'freq_bands' : freq_bands,
        'n_time_bins_per_trial': n_time_bins_per_trial}



out = {'pars'                 : pars,
       'decoding_scores'      : df,
       'decoding_predictions' : rs,
       'unit_scores'          : unit_scores}

print('Saving output to {}'.format(output_full_path))
pickle.dump(out, open(output_full_path, 'wb'))

print('Saving parameters to {}'.format(pars_full_path))
pickle.dump(pars, open(pars_full_path, 'wb'))
