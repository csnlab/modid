import numpy as np
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import accuracy_score, recall_score

def get_scoring_function(score_name='accuracy'):
    if score_name != 'accuracy':
        raise NotImplementedError
    else:
        return accuracy_score


def get_kfold_score(X, y, decoder, n_splits, seed, shuffle, score_name,
                    average_fold_scores=True, verbose=0):

    kfold = StratifiedKFold(n_splits=n_splits, shuffle=shuffle,
                            random_state=seed)
    kfold_scores = []
    y_test_all, y_pred_all = [], []

    for fold, (training_ind, testing_ind) in enumerate(kfold.split(X, y)):

        X_train = X[training_ind, :]
        X_test  = X[testing_ind, :]
        y_train = y[training_ind]
        y_test  = y[testing_ind]

        decoder.fit(X_train, y_train)
        y_pred = decoder.predict(X_test)
        scoring_function = get_scoring_function(score_name=score_name)
        score = scoring_function(y_test, y_pred)
        kfold_scores.append(score)
        y_test_all.append(y_test)
        y_pred_all.append(y_pred)
        if verbose > 0:
            print('--- Fold {}/{} - {}: {}'.format(fold, n_splits, score_name,
                                                   score))

    y_test_all = np.hstack(y_test_all)
    y_pred_all = np.hstack(y_pred_all)

    if average_fold_scores:
        kfold_scores = np.mean(kfold_scores)

    return kfold_scores, y_test_all, y_pred_all