import os
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import glob
from constants import *
import pickle
from plotting_style import *
import pandas as pd

"""
This script generates decode figures which combine results from differernt
sessions (as opposed to plotting individual sessions, which is done in
load_dec_single_sess.py)

You need to specify the settings name of the decode 'experiment', together
withe the name of the decode target and the areas for spikes and lfp. 
You can additionally specify the 
animal_id, if you want to combine only sessions which belong to a specific 
animal. Otherwise, all available sessions are used. 
"""



settings_name = 'oct3'

# plotting parameters
ci=95
plot_format = 'svg'
dpi = 400
save_plots = True
markers_in_lineplot=False

plot_folder = os.path.join(DATA_FOLDER, 'plots', 'dec', settings_name)
results_folder = os.path.join(DATA_FOLDER, 'results', 'dec', settings_name)
result_files = glob.glob('{}/decode*'.format(results_folder))
pars_file = 'decode_settings_{}.pkl'.format(settings_name)

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder, exist_ok=True)

# to figure which sessions we have run the decode for, we load all the
# results (a bit hacky but takes no time). Then we subselect the ones we
# are interested in
sessions = pd.DataFrame(columns=['animal_id', 'session_id', 'target_name',
                                 'area_spikes', 'area_lfp'])

dfs = []

for file in result_files:
    results_full_path = os.path.join(results_folder, file)
    results = pickle.load(open(results_full_path, 'rb'))

    try:
        df = results['decoding_scores']
        pars = results['pars']
        dfs.append(df)
    except KeyError:
        pass

df = pd.concat(dfs)
df = df.drop(['area_lfp', 'lfp_channel_id'], axis=1)

df['score'] = pd.to_numeric(df['score'])
df['animal_session'] = df['animal_id']+'_'+df['session_id']

# subselect sessions:


sessions = df.drop_duplicates(subset=['animal_id', 'session_id', 'target',
                                      'area_spikes'])
sessions = sessions.drop(['time', 'time_bin', 'input', 'decoder',
                          'score', 'animal_session', 'repeat'],
                         axis=1)


area = 'V1'
input1 = 'rate'
input2 = 'shuffled_rate'

target_names = ['visualOriPostChangeNorm', 'visualOriChangeNorm',
                'audioFreqPostChangeNorm', 'audioFreqChangeNorm',
                'correctResponse', 'trialType']

#target_names = ['audioFreqPostNorm', 'audioFreqChangeNorm']
#AREAS = ['V1']
#target_names = df['target'].unique()

palette = [layer_palette[layer] for layer in LAYERS]
n_plots = len(target_names)

f, axes = plt.subplots(2, 3, figsize=[3*small_square_side, 3*small_square_side/2],
                     sharex=True, sharey=True)
ax = axes.flatten(order='F')

for i, target in enumerate(target_names):

    dfx = df[(df['target'] == target) & (df['area_spikes'] == area)]
    print(target)
    print(dfx.head())

    if dfx.shape[0] > 0:

        df2 = dfx.groupby([i for i in dfx.columns if ~np.isin(i, ['repeat', 'score'])]).mean().reset_index()

        x = df2[df2['input'] == input1]
        y = df2[df2['input'] == input2]

        for col in ['animal_id', 'session_id', 'decoder', 'time_bin', 'time']:
            np.testing.assert_array_equal(x[col], y[col])

        dfdiff = x.copy()
        dfdiff['input'] = 'rate_over_shuffled'
        dfdiff['score'] = x['score'].values - y['score'].values

        dfx = dfdiff[dfdiff['input'] == 'rate_over_shuffled']
        sns.lineplot(x='time', y='score', data=dfx, ax=ax[i], ci=ci,
                     err_kws={'linewidth': 0}, markers=markers_in_lineplot,
                     style='layer_spikes',
                     style_order=LAYERS,
                     hue='layer_spikes',
                     hue_order=LAYERS, palette=palette)
        ax[i].set_xlabel('Time [s]')
        ax[i].set_ylabel('Decoding\nimprovement')
        ax[i].axvline(0, c='grey', alpha=0.8, ls='--')
        ax[i].axhline(0, c='grey', alpha=0.8, ls=':')
        #ax.legend(bbox_to_anchor=(1.04, 1))
        ax[i].set_title(targets_labels_matthijs[target])
        ax[i].get_legend().remove()

axes[0, -1].legend(bbox_to_anchor=(1.04, 0.5), loc='center left',
                   frameon=False, title='').texts[0].set_text("")
sns.despine()
plt.tight_layout(rect=(0, 0, 0.8, 1))

plot_name = 'dec_all_sess_all_areas_by_layer_{}.{}'.format(settings_name, plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi,
            bbox_inches="tight")
