import os
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import glob
from constants import *
import pickle
from plotting_style import *
import pandas as pd
from sklearn import preprocessing
from sklearn.metrics.pairwise import cosine_similarity
import matplotlib_venn

"""
Load and plot the unit scores of a decode experiment. 

PLOTS
1. For every target type, plot the time at which unit reach the highest score
2. Cosine similarity of unit scores vectors across targets
3. Distribution of preferred target per unit

"""

settings_name = 'sep26'
plot_settings_name = 'base3'

# analysis parameters
t0 = 0
t1 = 0.5
aggregate_scores = 'max' #'max' or 'mean' (there's a score per time point, so
# you can average over time or take the highest point )
score_name = 'auc'
use_debiased = True
p_val_thr = 0.01 #0.01  # can also set to bonferroni, in which case it's 0.05/N

# plotting parameters
ci=95
plot_format = 'png'
dpi = 400
save_plots = True
markers_in_lineplot=True

if use_debiased:
    score_col = 'score_debiased'
else:
    score_col = 'score'

# --- SET UP PATHS AND LOAD ---------------------------------------------------

plot_folder = os.path.join(DATA_FOLDER, 'plots', 'dec', settings_name, plot_settings_name)
results_folder = os.path.join(DATA_FOLDER, 'results', 'dec', settings_name)
result_files = glob.glob('{}/decode*'.format(results_folder))
pars_file = 'decode_settings_{}.pkl'.format(settings_name)

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder, exist_ok=True)


# to figure which sessions we have run the decode for, we load all the
# results (a bit hacky but takes no time). Then we subselect the ones we
# are interested in
sessions = pd.DataFrame(columns=['animal_id', 'session_id', 'target_name',
                                 'area_spikes', 'area_lfp'])

dfs = []

for file in result_files:
    results_full_path = os.path.join(results_folder, file)
    results = pickle.load(open(results_full_path, 'rb'))

    try:
        unit_scores = results['unit_scores']
        for score in unit_scores.keys():
            df = unit_scores[score]
            dfs.append(df)
    except KeyError:
        pass

df = pd.concat(dfs)
try:
    df = df.drop(['area_lfp'], axis=1)
except KeyError:
    pass


# we do it like this to have a precise order of the targets when we plot
# target_names = ['correctResponse', 'Lick', 'trialType', 'visualOriPostChangeNorm',
#                 'audioFreqPostChangeNorm', 'visualOriChangeNorm', 'audioFreqChangeNorm']

target_names = ['Lick', 'visualOriPostChangeNorm', 'visualOriChangeNorm',
                'audioFreqPostChangeNorm', 'audioFreqChangeNorm']
target_names = [t for t in target_names if np.isin(t, df['target_name'].unique())]

# every unit has a value of the score at every point in time: so we can
# aggregate by taking the average, or taking the peak discrimination

# if df.repeat.unique().shape[0] > 1:
#     raise ValueError('Watch out because you will average p-values')

# first average across repeats (per time point)
dfx = df.groupby(['animal_id', 'session_id', 'area_spikes',
                  'feature_name', 'target_name', 'score_name', 'time']).mean().reset_index()

# RESTRICT TO CERTAIN TIMES
dfx = dfx[(dfx['time'] >= t0) & (dfx['time'] <= t1)]

groupbys = ['animal_id', 'session_id', 'area_spikes',
            'feature_name', 'target_name', 'score_name']

if aggregate_scores == 'max':
    # pick only the time of maximum auc PER TARGET
    dfx = dfx.sort_values(score_col, ascending=False).drop_duplicates(groupbys)
elif aggregate_scores == 'mean':
    dfx = dfx.groupby(groupbys).mean().reset_index()
    raise ValueError('If you use mean, you also average the p-values')
else:
    raise ValueError
dfx = dfx.sort_values(['feature_name', 'score_name', 'target_name'])

unique_sessions = dfx.session_id.unique()


# 1. --- TIME OF PEAK OF AUC SCORE (PER AREA) ---------------------------------
# Time at which units reach their maximum discrimination for the different
# targets per area

dfs = dfx[(dfx['score_name'] == score_name)]

palette = [targets_palette_matthijs[k] for k in target_names]
labels = [targets_labels_matthijs[k].replace(' ', '\n') for k in target_names]
f, ax = plt.subplots(1, 1, figsize=small_decoding_panel_size)
sns.barplot(data=dfs, x='target_name', y='time', hue='area_spikes',
            order=target_names, palette=[area_palette[a] for a in AREAS],
            hue_order=AREAS, ax=ax)
ax.set_xticklabels(labels, rotation=45)
ax.set_xlabel('')
ax.set_ylabel('Time of peak\n{}'.format(unit_score_labels[score_name]))
ax.legend(bbox_to_anchor=(1.04,0.5), loc="center left", borderaxespad=0,
          frameon=False)
sns.despine()
plt.tight_layout(rect=(0, 0, 0.84, 1))

plot_name = 'unit_scores_time_peak_auc_{}_{}.{}'.format(
             settings_name, plot_settings_name, plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi, bbox_inches="tight")

# 2. --- COSINE SIMILARITY OF UNITS SCORES ACROSS TARGETS ---------------------


overlap_mat = np.zeros((len(target_names), len(target_names)))
n_mat = np.zeros_like(overlap_mat)

for area in AREAS:
    for sess in unique_sessions:

        dfs = dfx[(dfx['session_id'] == sess) & (dfx['area_spikes'] == area) &
                  (dfx['score_name'] == score_name)]

        for i, target1 in enumerate(target_names):
            for j, target2 in enumerate(target_names):

                df1 = dfs[(dfs['target_name'] == target1)].sort_values('feature_name')
                df2 = dfs[(dfs['target_name'] == target2)].sort_values('feature_name')

                if df1.shape[0] == df2.shape[0] and df1.shape[0] > 0:
                    np.testing.assert_array_equal(df1['feature_name'], df2['feature_name'])

                    un1 = df1[score_col].values.reshape(-1, 1)
                    un2 = df2[score_col].values.reshape(-1, 1)

                    # if score_name == 'auc':
                    #     un1 = (un1 - 0.5)
                    #     un2 = (un2 - 0.5)

                    #un1 = preprocessing.normalize(un1.reshape(-1, 1), norm='l2', axis=0)
                    #un2 = preprocessing.normalize(un2.reshape(-1, 1), norm='l2', axis=0)

                    overlap = cosine_similarity(un1.T, un2.T)[0]
                    #overlap = np.nansum(un1 * un2)
                    overlap_mat[i,j] += overlap
                    n_mat[i, j] += 1

    overlap_mat_mean = overlap_mat / n_mat

    labels = [targets_labels_matthijs[k].replace(' ', '\n') for k in target_names]
    f, ax = plt.subplots(1, 1)
    sns.heatmap(overlap_mat_mean, ax=ax, cmap=area_cmap[area], annot=False,  linewidths=.8)
    ax.set_xticklabels(labels, rotation=40)
    ax.set_yticklabels(labels, rotation=0)
    ax.tick_params(axis=u'both', which=u'both',length=0)
    plt.tight_layout()

    plot_name = 'unit_scores_cosine_similarity_{}_{}_{}.{}'.format(area,
        settings_name, plot_settings_name,  plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi, bbox_inches="tight")

# 3. --- NUMBER OF UNITS BY THEIR PREFERRED TARGET PER AREA -------------------
# for every unit, figure out for which target it has the highest score,
# and plot bars of the number of units with a given preferred target

as_percentage = False

labels = [targets_labels_matthijs[k].replace(' ', '\n') for k in target_names]
palette = [targets_palette_matthijs[k] for k in target_names]

# this is a slight variation of the grouping we do at the top for dfx, because
# when we pick the highest auc per unit we don't pick it per target but rather
# we pick one across targets
# TODO we are forgetting to subselect time here
dft = df.groupby(['animal_id', 'session_id', 'area_spikes',
                  'feature_name', 'target_name', 'score_name', 'time']).mean().reset_index()
groupbys = ['animal_id', 'session_id', 'area_spikes',
            'feature_name', 'score_name']
dft = dft.sort_values(score_col, ascending=False).drop_duplicates(groupbys)
dft = dft.sort_values(['feature_name', 'score_name', 'target_name'])
dft = dft[dft['score_name'] == score_name]


f, ax = plt.subplots(3, 1, figsize=[4, 5], sharex=True, sharey=False)

for i, area in enumerate(AREAS):
    counts = dft[dft['area_spikes'] == area]['target_name'].value_counts()

    if as_percentage:
        counts = 100 * counts / counts.sum()
    sns.barplot(counts.index, counts, ax=ax[i], color=area_palette[area],
                order=target_names)
    ax[i].set_xticklabels(labels, rotation=45)
    ax[i].set_xlabel('')
    ax[i].set_ylabel('')

if as_percentage:
    ax[1].set_ylabel('Percentage of units per preferred target')

else:
    ax[1].set_ylabel('# of units per preferred target')
sns.despine()
plt.tight_layout()


plot_name = 'unit_scores_units_by_pref_target_{}_{}.{}'.format(
             settings_name, plot_settings_name, plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi, bbox_inches="tight")

# 4. --- SCATTERPLOT OF AUDIO VS VISUAL AUC PER AREA --------------------------

plot_significance = True
significance_modality = 'visual'
tar1 = 'visualOriPostChangeNorm'
tar2 = 'audioFreqPostChangeNorm'
tar1 = 'visualOriChangeNorm'
tar2 = 'audioFreqChangeNorm'
tar1 = 'visualOriChangeNorm'
tar2 = 'audioFreqPostChangeNorm'
lim = [0.0, 0.3]

df_sel = dfx[dfx['score_name'] == score_name]

df_sel = df_sel[np.isin(df_sel['target_name'], [tar1, tar2])]
f1 = df_sel.loc[df_sel['target_name'] == tar1, 'feature_name']
f2 = df_sel.loc[df_sel['target_name'] == tar2, 'feature_name']
fboth = list(set(f1).intersection(set(f2)))
df_sel = df_sel[np.isin(df_sel['feature_name'], fboth)]

if p_val_thr == 'bonferroni':
    thr = 0.05 / (fboth.__len__())
else:
    thr = p_val_thr


f, ax = plt.subplots(1, 3, figsize=[10,4], sharex=True, sharey=True)
for i, area in enumerate(AREAS):

    dfs = df_sel[(df_sel['area_spikes'] == area)].sort_values(by='feature_name')

    x = dfs[dfs['target_name'] == tar1]
    y = dfs[dfs['target_name'] == tar2]

    for col in ['animal_id', 'session_id', 'feature_name']:
        np.testing.assert_array_equal(x[col], y[col])

    if plot_significance:
        if significance_modality == 'visual':
            sig_ind = (x['p_val'] < thr).values
            nonsig_ind = (x['p_val'] >= thr).values
        elif significance_modality == 'audio':
            sig_ind = (y['p_val'] < thr).values
            nonsig_ind = (y['p_val'] > thr).values

        x_sig = x.loc[sig_ind, score_col]
        y_sig = y.loc[sig_ind, score_col]
        ax[i].scatter(x_sig, y_sig, c=area_palette[area], s=45,
                      edgecolor='w', zorder=12)

        x_nonsig = x.loc[nonsig_ind, score_col]
        y_nonsig = y.loc[nonsig_ind, score_col]
        ax[i].scatter(x_nonsig, y_nonsig, c=sns.xkcd_rgb['light grey'], s=45,
                      edgecolor='w', zorder=12)

    ax[i].set_aspect('equal', adjustable='datalim')
    ax[i].grid(ls=':')

for axx in ax:
    axx.set_xlim(lim)
    axx.set_ylim(lim)
    axx.set_xticks(np.arange(lim[0], lim[1], 0.1))
    axx.set_yticks(np.arange(lim[0], lim[1], 0.1))
    axx.set_xlabel('AUC ({})'.format(targets_labels_matthijs[tar1]))

ax[0].set_ylabel('AUC ({})'.format(targets_labels_matthijs[tar2]))
sns.despine()
plt.tight_layout()


# 5. ---  --------------------------

target_pairs = [['visualOriPostChangeNorm', 'audioFreqPostChangeNorm'],
                ['visualOriChangeNorm', 'audioFreqChangeNorm'],
                ['visualOriChangeNorm', 'Lick'],
                ['audioFreqChangeNorm', 'Lick'],
                ['visualOriPostChangeNorm', 'Lick'],
                ['audioFreqPostChangeNorm', 'Lick'],
                ['visualOriChangeNorm', 'visualOriPostChangeNorm'],
                ['audioFreqChangeNorm', 'audioFreqPostChangeNorm']]

target_pair_names = ['Orientation and frequency',
                     'Orientation change and frequency change',
                     'Orientation change and lick',
                     'Frequency change and lick',
                     'Orientation and lick',
                     'Frequency and lick',
                     'Orientation change and identity',
                     'Frequency change and identity']

colors = [sns.xkcd_rgb['medium grey'], sns.xkcd_rgb['light grey'],
          sns.xkcd_rgb['purple grey']]

for area in AREAS:

    f, axes = plt.subplots(4, 2, figsize=[7, 14])

    for i, (title, pair) in enumerate(zip(target_pair_names, target_pairs)):

        ax = axes.flatten()[i]

        tar1, tar2 = pair

        df_sel = dfx[dfx['score_name'] == score_name]
        # Here I should restrict to units which are recorded in both conditions
        # because otherwise results may be skewed by just having more audio sessions
        df_sel = df_sel[np.isin(df_sel['target_name'], [tar1, tar2])]

        # here we make sure that we only include units for which auc was
        # computed for both targets
        f1 = df_sel.loc[df_sel['target_name'] == tar1, 'feature_name']
        f2 = df_sel.loc[df_sel['target_name'] == tar2, 'feature_name']

        fboth = list(set(f1).intersection(set(f2)))
        df_sel = df_sel[np.isin(df_sel['feature_name'], fboth)]

        if p_val_thr == 'bonferroni':
            thr = 0.05 / (fboth.__len__())
        else:
            thr = p_val_thr


        df1 = df_sel.loc[(df_sel['area_spikes'] == area) &
                      (df_sel['target_name'] == tar1)]

        df2 = df_sel.loc[(df_sel['area_spikes'] == area) &
                      (df_sel['target_name'] == tar2)]

        df1 = df1[df1['p_val'] < thr]
        df2 = df2[df2['p_val'] < thr]

            # sig_pos = set(df1['feature_name']).__len__()
            # sig_hdir = set(df2['feature_name']).__len__()
            # sigboth = set(df1['feature_name']).intersection(set(df2['feature_name'])).__len__()
        labels = [targets_labels_matthijs[k].replace(' ', '\n') for k in
                  pair]
        v = matplotlib_venn.venn2([set(df1['feature_name']), set(df2['feature_name'])],
                              set_labels=labels, ax=ax,
                              set_colors=colors)

    #f.suptitle(area)
    #plt.tight_layout(rect=[0.1, 0.1, 0.9, 0.9])

    plot_name = 'unit_scores_venn_all_{}_{}_{}.{}'.format(area,
                settings_name, plot_settings_name, plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi,
              bbox_inches="tight")




# target_pairs = [['visualOriPostChangeNorm', 'audioFreqPostChangeNorm'],
#                 ['visualOriChangeNorm', 'audioFreqChangeNorm'],
#                 ['visualOriChangeNorm', 'Lick'],
#                 ['audioFreqChangeNorm', 'Lick'],
#                 ['visualOriPostChangeNorm', 'Lick'],
#                 ['audioFreqPostChangeNorm', 'Lick'],
#                 ['visualOriChangeNorm', 'visualOriPostChangeNorm'],
#                 ['audioFreqChangeNorm', 'audioFreqPostChangeNorm']]
#
# target_pair_names = ['Orientation and frequency',
#                      'Orientation change and frequency change',
#                      'Orientation change and lick',
#                      'Frequency change and lick',
#                      'Orientation and lick',
#                      'Frequency and lick',
#                      'Orientation change and identity',
#                      'Frequency change and identity']
#
#
# for title, pair in zip(target_pair_names, target_pairs):
#
#     tar1, tar2 = pair
#
#     df_sel = dfx[dfx['score_name'] == score_name]
#     # Here I should restrict to units which are recorded in both conditions
#     # because otherwise results may be skewed by just having more audio sessions
#     df_sel = df_sel[np.isin(df_sel['target_name'], [tar1, tar2])]
#     f1 = df_sel.loc[df_sel['target_name'] == tar1, 'feature_name']
#     f2 = df_sel.loc[df_sel['target_name'] == tar2, 'feature_name']
#     fboth = list(set(f1).intersection(set(f2)))
#     df_sel = df_sel[np.isin(df_sel['feature_name'], fboth)]
#
#     if p_val_thr == 'bonferroni':
#         thr = 0.05 / (fboth.__len__())
#     else:
#         thr = p_val_thr
#
#     colors = [sns.xkcd_rgb['medium grey'], sns.xkcd_rgb['light grey'], sns.xkcd_rgb['purple grey']]
#
#
#     f, ax = plt.subplots(1, 3, figsize=[5, 2])
#     for i, area in enumerate(AREAS):
#
#         df1 = df_sel.loc[(df_sel['area_spikes'] == area) &
#                       (df_sel['target_name'] == tar1)]
#
#         df2 = df_sel.loc[(df_sel['area_spikes'] == area) &
#                       (df_sel['target_name'] == tar2)]
#
#         df1 = df1[df1['p_val'] < thr]
#         df2 = df2[df2['p_val'] < thr]
#
#         # sig_pos = set(df1['feature_name']).__len__()
#         # sig_hdir = set(df2['feature_name']).__len__()
#         # sigboth = set(df1['feature_name']).intersection(set(df2['feature_name'])).__len__()
#
#         matplotlib_venn.venn2([set(df1['feature_name']), set(df2['feature_name'])],
#                               set_labels=['', ''], ax=ax[i],
#                               set_colors=colors)
#         ax[i].set_title(AREAS[i])
#     f.suptitle(title)
#
#     plot_name = 'unit_scores_venn_{}_{}_{}.{}'.format(title,
#         settings_name, plot_settings_name, plot_format)
#     f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi,
#               bbox_inches="tight")
#
#


#
# for area in AREAS:
#     for title, pair in zip(target_pair_names, target_pairs):
#
#         tar1, tar2 = pair
#
#         df_sel = dfx[(dfx['score_name'] == score_name) & (dfx['area_spikes'] == area)]
#         # Here I should restrict to units which are recorded in both conditions
#         # because otherwise results may be skewed by just having more audio sessions
#         df_sel = df_sel[np.isin(df_sel['target_name'], [tar1, tar2])]
#         f1 = df_sel.loc[df_sel['target_name'] == tar1, 'feature_name']
#         f2 = df_sel.loc[df_sel['target_name'] == tar2, 'feature_name']
#         fboth = list(set(f1).intersection(set(f2)))
#         df_sel = df_sel[np.isin(df_sel['feature_name'], fboth)]
#
#         if p_val_thr == 'bonferroni':
#             thr = 0.05 / (fboth.__len__())
#         else:
#             thr = p_val_thr
#
#         colors = [sns.xkcd_rgb['medium grey'], sns.xkcd_rgb['light grey'], sns.xkcd_rgb['purple grey']]
#
#
#         f, ax = plt.subplots(1, 1, figsize=[4, 4])
#
#         df1 = df_sel.loc[(df_sel['target_name'] == tar1)]
#
#         df2 = df_sel.loc[(df_sel['target_name'] == tar2)]
#
#         df1 = df1[df1['p_val'] < thr]
#         df2 = df2[df2['p_val'] < thr]
#
#         # sig_pos = set(df1['feature_name']).__len__()
#         # sig_hdir = set(df2['feature_name']).__len__()
#         # sigboth = set(df1['feature_name']).intersection(set(df2['feature_name'])).__len__()
#         labels = [targets_labels_matthijs[k].replace(' ', '\n') for k in
#                   pair]
#         matplotlib_venn.venn2([set(df1['feature_name']), set(df2['feature_name'])],
#                               set_labels=labels, ax=ax,
#                               set_colors=colors)
#         #ax.set_title('{}\n{}'.format(area, title))
#         plt.tight_layout()
#         labels_save = [targets_labels_matthijs[k].replace(' ', '_') for k in
#                   pair]
#         plot_name = 'unit_scores_venn_{}_{}_{}_{}_{}.{}'.format(area, labels_save[0],
#                     labels_save[1], settings_name, plot_settings_name, plot_format)
#
#         f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi,
#                   bbox_inches="tight")
#
#
#
#
#
# target_triplets = [['Lick', 'visualOriPostChangeNorm', 'audioFreqPostChangeNorm'],
#                 ['Lick', 'visualOriChangeNorm', 'audioFreqChangeNorm'],
#                 ['visualOriChangeNorm', 'audioFreqChangeNorm', 'visualOriPostChangeNorm'],
#                 ['visualOriChangeNorm', 'audioFreqChangeNorm', 'audioFreqPostChangeNorm']]
#
# target_triplet_names = ['1', '2', '3', '4']
#
# for area in AREAS:
#     for title, targets in zip(target_triplet_names, target_triplets):
#
#         df_sel = dfx[(dfx['score_name'] == score_name) & (dfx['area_spikes'] == area)]
#         # Here I should restrict to units which are recorded in both conditions
#         # because otherwise results may be skewed by just having more audio sessions
#         df_sel = df_sel[np.isin(df_sel['target_name'], targets)]
#
#         # f1 = df_sel.loc[df_sel['target_name'] == tar1, 'feature_name']
#         # f2 = df_sel.loc[df_sel['target_name'] == tar2, 'feature_name']
#         #
#         # fboth = list(set(f1).intersection(set(f2)))
#         # df_sel = df_sel[np.isin(df_sel['feature_name'], fboth)]
#         #
#         # if p_val_thr == 'bonferroni':
#         #     thr = 0.05 / (fboth.__len__())
#         # else:
#         #     thr = p_val_thr
#
#         colors = [sns.xkcd_rgb['medium grey'], sns.xkcd_rgb['light grey'], sns.xkcd_rgb['purple grey']]
#
#
#
#         dfs = []
#         for target in targets:
#             df1 = df_sel.loc[(df_sel['target_name'] == target)]
#             df1 = df1[df1['p_val'] < thr]
#             dfs.append(set(df1['feature_name']))
#
#         # sig_pos = set(df1['feature_name']).__len__()
#         # sig_hdir = set(df2['feature_name']).__len__()
#         # sigboth = set(df1['feature_name']).intersection(set(df2['feature_name'])).__len__()
#
#         labels = [targets_labels_matthijs[k].replace(' ', '\n') for k in
#                   targets]
#         f, ax = plt.subplots(1, 1, figsize=[5, 5])
#
#         matplotlib_venn.venn3(dfs,
#                               set_labels=labels, ax=ax,
#                               set_colors=colors)
#         #ax.set_title('{}\n{}'.format(area, title))
#         plt.tight_layout(rect=[0.1, 0.1, 0.9, 0.9])
#         plot_name = 'unit_scores_venn3_{}_{}_{}_{}.{}'.format(area, title,
#             settings_name, plot_settings_name, plot_format)
#
#         f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi,
#                   bbox_inches="tight")