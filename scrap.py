


signal = {}

for trial_type in trial_types:
    for trial_folder in trials_in_test_folder:
        for layer in layers:

            for file in files_in_this_subfolder:
                
                # get id
                try:
                    signal[channel_id].append(loaded_file)
                except KeyError:
                    signal[channel_id] = [loaded_file]

statistics  = {}
for key in signal.keys():

    signal[key] = np.hstack(signal[key])
    statistics[key] = [np.mean(signal[key]), np.std(signal[key])]
