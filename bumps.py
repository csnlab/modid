import numpy as np
import pandas as pd
from constants import *
from loadmat import *
from session import Session
import quantities as pq
from sklearn.preprocessing import StandardScaler, MinMaxScaler
import tensortools as tt
from plotting_style import *
import matplotlib.pyplot as plt
from session_info import select_sessions
import itertools
import scipy.stats
from utils import convolve_chunk
from sklearn.metrics import roc_auc_score

settings_name = 'May21'

# if animal_id is specified, run for all sessions of that animal
# if area_spikes is not specified run for all available areas
animal_id = None
session_id = None
# area_spikes = 'all'  # if 'all' do one TCA for all units, otherwise area individually
min_units_per_area = 10

# animal_id = '2003'
# session_id = '2018-02-07_14-28-57'
# area_spikes = 'V1'

# TCA PARAMETERS
tca_rank = 10
fit_ensemble = False

# TIME PARAMETERS
align_event = 'stimChange'
sliding_window = False
time_before_stim_in_s = 0.0
time_after_stim_in_s = 0.4
binsize_in_ms = 200
slide_by_in_ms = -10


preprocess = 'z_score'


# PLOTTING PARAMETERS
plot_format = 'png'
dpi = 400
save_plots = True


# --- SET UP PATHS ------------------------------------------------------------

plot_folder = os.path.join(DATA_FOLDER, 'plots', 'bumps', settings_name)

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder, exist_ok=True)


# --- IDENTIFY SESSIONS -------------------------------------------------------

if session_id is None:
    sessions = select_sessions(min_units=0)


    sessions = sessions.drop_duplicates(subset=['animal_id', 'session_id'])

    if animal_id is not None:
        sessions = sessions[sessions['animal_id'] == animal_id]


else:
    sessions = pd.DataFrame(columns=['animal_id', 'session_id'],
                            data=[(animal_id, session_id)])


# --- RUN TCA -----------------------------------------------------------------

response = (0, 1)
modality = ['X', 'Y']
stimulus = (1, 2, 3, 4)
stimulus_change = [1, 2]


df = pd.DataFrame(columns=['animal_id', 'session_id', 'unit_id', 'area',
                           '1st_bump', '1st_bump_std', '2nd_bump', '2nd_bump_std'])

for i, row in sessions.iterrows():

    animal_id = row['animal_id']
    session_id = row['session_id']


    session = Session(animal_id=animal_id, session_id=session_id)
    session.load_data(load_lfp=False)


    trial_numbers = session.select_trials(response=response,
                                          trial_type=modality,
                                          visual_post_norm=stimulus,
                                          visual_change=stimulus_change)


    trial_times = session.get_aligned_times(trial_numbers,
                                            time_before_in_s=time_before_stim_in_s,
                                            time_after_in_s=time_after_stim_in_s,
                                            event=align_event)

    binned_spikes, spike_bin_centers = session.bin_spikes_per_trial(
                                            binsize_in_ms, trial_times,
                                            sliding_window=sliding_window,
                                            slide_by_in_ms=slide_by_in_ms)

    selected_unit_ind = session.select_units()
    selected_unit_id = session.get_cell_id(selected_unit_ind,
                                           shortened_id=False)
    unit_area = session.get_cell_area_from_cell_ind(selected_unit_ind)

    for ind, id, area in zip(selected_unit_ind, selected_unit_id, unit_area):

        first_bumps = [s[ind, 0] for s in binned_spikes]
        second_bumps = [s[ind, 1] for s in binned_spikes]

        first_bump = np.mean(first_bumps)
        second_bump = np.mean(second_bumps)

        first_bump_std = np.std(first_bumps)
        second_bump_std = np.std(second_bumps)

        row = [animal_id, session_id, id, area, first_bump, first_bump_std,
               second_bump, second_bump_std]

        df.loc[df.shape[0], :] = row


numeric_cols = ['1st_bump', '1st_bump_std', '2nd_bump', '2nd_bump_std']
for col in numeric_cols:
    df[col] = pd.to_numeric(df[col])

g = sns.pairplot(vars=["1st_bump", "2nd_bump"], data=df,
                  hue='area', hue_order=AREAS,
                  palette=[area_palette[a] for a in AREAS], kind='reg',
                  plot_kws=dict(scatter_kws=dict(edgecolor='w')))

labels = ['Spike count in the\nfirst {} ms'.format(binsize_in_ms),
          'Spike between\n{} and {} ms'.format(binsize_in_ms, 2*binsize_in_ms)]

for i in range(2):
    g.axes[i, 0].set_ylabel(labels[i])
    g.axes[-1, i].set_xlabel(labels[i])


plot_name = '1st_vs_2nd_bump_{}ms.{}'.format(binsize_in_ms, plot_format)

g.savefig(os.path.join(plot_folder, plot_name), dpi=dpi,bbox_inches="tight")

