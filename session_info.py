import numpy as np
import pandas as pd
from session import Session
from constants import *
import subprocess
import warnings


def make_trial_overview():

    dfs = []

    for animal_id in decoding_sessions.keys():
        for session_id in decoding_sessions[animal_id]:
            session = Session(animal_id=animal_id, session_id=session_id)
            session.load_data(load_spikes=False, load_lfp=False)
            df = session.get_session_overview_trials()
            print(df)
            dfs.append(df)

    df = pd.concat(dfs).reset_index().drop('index', axis=1)

    ind = df.loc[(df['animal_id'] == '2009') &
           (df['session_id'] == '2018-08-24_11-56-35') &
           (df['target_name'] == 'visualOriPostNorm')].index

    df = df.drop(ind)

    # just to make sure
    sel = df.loc[(df['animal_id'] == '2009') &
           (df['session_id'] == '2018-08-24_11-56-35') &
           (df['target_name'] == 'visualOriPostNorm')]
    assert sel.shape[0] == 0

    return df



def make_units_overview():

    dfs = []

    for animal_id in decoding_sessions.keys():
        for session_id in decoding_sessions[animal_id]:
            session = Session(animal_id=animal_id, session_id=session_id)
            # TODO we should be able to provide the unit layer without loading
            # the LFP
            session.load_data(load_spikes=True, load_lfp=False)
            df = session.get_session_overview_n_units()
            dfs.append(df)

    return pd.concat(dfs)



def make_units_and_channels_overview():
    dfs = []

    for animal_id in decoding_sessions.keys():
        for session_id in decoding_sessions[animal_id]:
            session = Session(animal_id=animal_id, session_id=session_id)
            session.load_data(load_spikes=True, load_lfp=False,
                              load_lfp_lazy=True)
            df = session.get_session_overview_units_and_channels()
            dfs.append(df)
    return pd.concat(dfs)




def select_sessions(stimulus_type=None, min_trials_per_class=None,
                    min_units=None,
                    per_layer=False, min_channels_per_layer=None,
                    exclude_NA_layer=True, min_perc_correct=None):

    """
    This function outputs a dataframe with all the sessions which
    respond to the given criteria. Usage is as follows:

    sessions = select_sessions(min_trials_per_stim=10)
    for i, row in sessions.iterrows():
        animal_id = row['animal_id']
        session_id = row['session_id']

    :param stimulus_type: Whether to restrict the output to a certain stimulus
    type.
    :param min_trials_per_class: To restrict to sessions which have, e.g. for visual
    trials, at least n trials with small orientations and n with large
    orientations. To ensure balance in stimulus categories for decode.
    # :param only_correct: (bool) Ths affects the parameter above: the number of
    # minimum trials per stimulus category can be computed for all trials,
    # or considering only correct trials.
    :param min_units: The minimum number of units required.
    :param per_layer: Determines whether the minimum number of units is to be
    specified per area or per layer
    :param min_channels_per_layer: The minimum number of channels required per area.
    :param exclude_NA_layer: To automatically drop from the output channels/units which
    are not assigned to a layer.
    :param min_perc_correct: Minimum performance (percentage of correct trials)
    in trials with large stimulus change. To make sure that the animals are
    performing the task in that modality.
    :return:
    """

    trial_df = make_trial_overview()

    # SELECT BASED ON TRIALS
    if min_trials_per_class is not None:
        trial_df = trial_df.loc[(trial_df.nt_0 >= min_trials_per_class) &
                                (trial_df.nt_1 >= min_trials_per_class)]

    if stimulus_type is not None:
        trial_df = trial_df[trial_df['stimulus_type'] == stimulus_type]

    if min_perc_correct is not None:
        trial_df = trial_df[trial_df['perc_corr'] >= min_perc_correct]


    # SELECT BASED ON NUMBER OF UNITS PER AREA
    if min_units is not None and not per_layer:
        units_df = make_units_overview()
        units_df = units_df[units_df['n_units'] >= min_units]
        sel_df = pd.merge(units_df, trial_df, on=['animal_id', 'session_id'])


    need_layer_info = min_channels_per_layer is not None or (min_units is not None and per_layer)
    # SELECT BASED ON NUMBER OF UNITS/CHANNELS PER AREA/LAYER
    if need_layer_info:
        layers_df = make_units_and_channels_overview()

        if min_units is not None and per_layer:
            layers_df = layers_df[layers_df['n_units'] >= min_units]

        if min_channels_per_layer is not None:
            layers_df = layers_df[layers_df['n_channels'] >= min_channels_per_layer]

        if exclude_NA_layer:
            layers_df = layers_df[layers_df['layer'] != 'NA']

        sel_df = pd.merge(layers_df, trial_df, on=['animal_id', 'session_id'])

    try:
        return sel_df
    except NameError:
        return trial_df


if __name__ == '__main__':

    min_channels_per_layer = None
    min_units = 0
    per_layer = False
    exclude_NA_layer = True

    sdf = select_sessions(min_channels_per_layer=min_channels_per_layer,
                          min_units=min_units, per_layer=per_layer,
                          exclude_NA_layer=exclude_NA_layer)



# if __name__ == '__main__':
#     # performance
#
#     only_correct = True
#     stimulus_type = None
#     min_trials_per_stim = 10
#     min_units = None
#     min_units_per_layer = 12
#     min_channels_per_layer = 10
#     min_perc_correct = 40
#     exclude_NA_layer = True
#
#     if min_units is not None and min_units_per_layer is not None:
#         raise ValueError('You can select either the minimum number of (total) units'
#                          'per area, or the minimum number of units per area/layer, '
#                          'not both!')
#
#     trial_df = make_trial_overview()
#
#     if min_trials_per_stim is not None:
#         trial_df = trial_df.loc[(trial_df.only_correct == only_correct) &
#                                     (trial_df.nt_0 >= min_trials_per_stim) &
#                                     (trial_df.nt_1 >= min_trials_per_stim)]
#
#     if stimulus_type is not None:
#         trial_df = trial_df[trial_df['stimulus_type'] == stimulus_type]
#
#     if min_perc_correct is not None:
#         trial_df = trial_df[trial_df['perc_corr'] >= min_perc_correct]
#
#
#
#     if min_units is not None:
#         units_df = make_units_overview()
#
#         units_df = units_df[units_df['n_units'] >= min_units]
#         sel_df = pd.merge(units_df, trial_df, on=['animal_id', 'session_id'])
#
#
#     if min_units_per_layer is not None or min_channels_per_layer is not None:
#         layers_df = make_units_and_channels_overview()
#         layers_df_copy = layers_df.copy()
#
#         if min_units_per_layer is not None:
#             layers_df = layers_df[layers_df['n_units'] >= min_units_per_layer]
#
#         if min_channels_per_layer is not None:
#             layers_df = layers_df[layers_df['n_channels'] >= min_channels_per_layer]
#
#         if exclude_NA_layer:
#             layers_df = layers_df[layers_df['layer'] != 'NA']
#
#         sel_df = pd.merge(layers_df, trial_df, on=['animal_id', 'session_id'])



