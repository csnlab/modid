import numpy as np
import pandas as pd
from constants import *
from loadmat import *
from session import Session
import scipy
from sklearn.model_selection import StratifiedKFold
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score, recall_score
import pickle
import quantities as pq
from utils import shuffle_array_rows_within_labels
from sklearn.ensemble import ExtraTreesClassifier
import distutils
from unit_scores import SingleUnitDiscriminationScores
from sklearn.preprocessing import StandardScaler, MinMaxScaler
import tensortools as tt
from plotting_style import *
import matplotlib.pyplot as plt


animal_id = '2003'
session_id = '2018-02-07_14-28-57'

only_correct_trials = False
area_spikes = 'V1'

# TIME PARAMETERS
sliding_window = False
time_before_stim_in_s = 0.4
time_after_stim_in_s = 0.8
binsize_in_ms = 1
slide_by_in_ms = -10

# SMOOTHING PARAMETERS
apply_smoothing = True
sampling_period = 10
kernel_width = 30
kernel_size = kernel_width * 8
kernel = scipy.signal.gaussian(kernel_size, kernel_width, sym=True)
kernel = 1000 * kernel / kernel.sum()

preprocess = 'z_score'

# SELECT TRIALS

#modalities = ['X', 'X', 'Y', 'Y']
# stimuli = [(1, 2), (3, 4), (1, 2), (3, 4)]
# stimulus_changes = [(1, 2), (1, 2), (1, 2), (1, 2)]

modalities = ['X', 'Y']


auditory_change=None
auditory_post = None
visual_change = None


# --- UTILITIES ---------------------------------------------------------------

def convolve_chunk(chunk, kernel, p):
    kernel_size = kernel.shape[0]
    rate = np.apply_along_axis(lambda m: np.convolve(m, kernel, mode='same'),
                               axis=1, arr=chunk)
    rate = rate[:, int(kernel_size / 2):-int(kernel_size / 2)]
    rate = rate[:, ::p]
    return rate



# --- PREPARE DATA ------------------------------------------------------------

session = Session(animal_id=animal_id, session_id=session_id)
session.load_data(load_lfp=False)

trial_numbers = session.select_trials(only_correct=only_correct_trials,
                                      trial_type=modalities)

trial_times = session.get_aligned_times(trial_numbers,
                                        time_before_in_s=time_before_stim_in_s,
                                        time_after_in_s=time_after_stim_in_s)

binned_spikes, spike_bin_centers = session.bin_spikes_per_trial(binsize_in_ms, trial_times,
                                                                sliding_window=sliding_window,
                                                                slide_by_in_ms=slide_by_in_ms)


selected_unit_ind = session.select_units(area=area_spikes)
selected_unit_id = session.get_cell_id(selected_unit_ind, shortened_id=False)

binned_spikes = [s[selected_unit_ind, :] for s in binned_spikes]

if apply_smoothing:
    binned_spikes = [convolve_chunk(arr, kernel, sampling_period) for
                     arr in binned_spikes]

n_time_bins_per_trial = spike_bin_centers[0].shape[0]
time_bin_times = (spike_bin_centers[0] - spike_bin_centers[0][0]).rescale(pq.s) \
                 + (binsize_in_ms*pq.ms).rescale(pq.s)/2 - time_before_stim_in_s*pq.s
time_bin_times = time_bin_times.__array__().round(5)
if apply_smoothing:
    time_bin_times = time_bin_times[int(kernel_size / 2):-int(kernel_size / 2)]
    time_bin_times = time_bin_times[::sampling_period]




modality_list, stimulus_list = [], []
stimulus_change_list, response_list = [], []
response_side_list = []

for num in trial_numbers:
    mod = session.get_type_of_trial(num)
    stim = session.get_stimulus_identity_of_trial(num)
    change = session.get_stimulus_change_of_trial(num)
    response = session.get_correct_response_of_trial(num)
    response_side = session.get_response_side_of_trial(num)
    modality_list.append(mod)
    stimulus_list.append(stim)
    stimulus_change_list.append(change)
    response_list.append(response)
    response_side_list.append(response_side)

response_side_list = [i if isinstance(i, str) else 'n' for i in response_side_list ]

# --- PREPARE TENSOR ----------------------------------------------------------

n_neurons = binned_spikes[0].shape[0]
n_all_trials = len(binned_spikes)
n_times = binned_spikes[0].shape[1]

X = np.hstack(binned_spikes)

if preprocess == 'min_max':
    mm = MinMaxScaler(feature_range=(0, 1))
    X = mm.fit_transform(X.T).T
elif preprocess == 'z_score':
    ss = StandardScaler(with_mean=True, with_std=True)
    X = ss.fit_transform(X.T).T

X = np.reshape(X, (n_neurons, n_times, n_all_trials), order='F')




# --- RUN TCA AND PLOT --------------------------------------------------------

mod_color = [modality_palette[k] for k in modality_list]
stim_color = [stimulus_palette[k] for k in stimulus_list]
stim_ch_color = [stimulus_change_palette[k] for k in stimulus_change_list]
resp_color = [response_palette[k] for k in response_list]
resp_side_color = [response_side_palette[k] for k in response_side_list]

titles = ['Modality', 'Stimulus identity', 'Stimulus change', 'Response',
          'Response side']
colors = [mod_color, stim_color, stim_ch_color, resp_color, resp_side_color]

R = 5
U = tt.ncp_bcd(X, rank=R, verbose=True)

#U = tt.cp_als(X, rank=R, verbose=True)

for color, title in zip(colors, titles):

    fig, axes, _ = tt.plot_factors(U.factors, plots=['bar', 'line', 'scatter'],
                                scatter_kw={'c' : color})
    ticks = axes[-1, 1].get_xticks()[:-1]
    axes[-1, 1].set_xticks(ticks)
    axes[-1, 1].set_xticklabels(np.round(time_bin_times[ticks.astype(int)], 2))
    axes[-1, 0].set_xlabel('Unit factors')
    axes[-1, 1].set_xlabel('Time factors')
    axes[-1, 2].set_xlabel('Trial factors')
    fig.suptitle(title)
    plt.tight_layout()

