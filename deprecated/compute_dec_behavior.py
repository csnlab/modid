import numpy as np
import pandas as pd
from constants import *
from loadmat import *
from session import Session
from sklearn.model_selection import StratifiedKFold
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score, recall_score
import pickle
import quantities as pq
from utils import shuffle_array_rows_within_labels
from sklearn.ensemble import ExtraTreesClassifier
import distutils
from unit_scores import SingleUnitDiscriminationScores
from sklearn.preprocessing import LabelEncoder
from session_info import select_sessions
import plotting_style
from decode.dec_utils import get_kfold_score, get_scoring_function

settings_name = 'sep26'
decoding_inputs_mode = 'standard'

# SESSION SELECTION
min_trials_per_class = 20
min_units = 2
min_perc_correct = 30

# SELECT AREAS
only_correct_trials = False
areas_spikes = None
select_area_lfp = 'same_as_spikes' #'nolfp' or 'same_as_spikes'

# TIME PARAMETERS
#binsize_in_ms = 100
#slide_by_in_ms = 300
#sliding_window = False
#time_before_stim_in_s = 0.4
#time_after_stim_in_s = 0.8

# FILTERING PARAMETERS
#lfp_downsample_factor = 5
#transition_width = 20  # Hz
#bandpass_attenuation = 60  # dB
#low_freq = 3
#mid_freq = 43
#high_freq = 83
#spacing_low = 10
#spacing_high = 10

# DISCRETIZATION
#discretize_phase = False
#discretize_energy = False
#n_phase_bins = 8
#n_energy_bins = 8

auditory_change=None
auditory_post = None
visual_change = None

# SELECT UNITS

# SELECT CHANNELS
#layer_lfp = None
#ch_in_the_middle_of_the_probe = True

trial_type = ['X', 'Y']
target_name = 'correctResponse'
#join_small_large_stim = True

# DECODING PARAMETERS
decoder_name = 'random_forest'
n_estimators = 200
n_splits = 5
n_repeats = 5
n_shuffles = 20
score_name = 'accuracy'
shuffle_kfold=True

# SINGLE UNIT SCORES PARAMETERS
discrimination_score_names = ['auc']#['jackknife', 'auc', 'feature_importance']
n_repeats_scores = 1
debias_auc_with_shuffle = True
n_auc_shuffles = 200

kfold_shuffle = True
n_repeats = 5

if decoding_inputs_mode == 'standard':
    decoding_inputs = ['V1_total_spikes']

process_lfp = False


if isinstance(discrimination_score_names, str):
    discrimination_score_names = [discrimination_score_names]


# --- SELECT SESSIONS ---------------------------------------------------------

sessions = select_sessions(min_trials_per_class=min_trials_per_class,
                           only_correct=only_correct_trials,
                           min_perc_correct=min_perc_correct,
                           min_units=min_units)

#sessions[np.isin(sessions['target_name'], ['audioFreqPostChangeNorm', 'visualOriPostChangeNorm'])].sort_values(['area', 'session_id'])

# if area_spikes is not None:
#     sessions = sessions[sessions['area'] == area_spikes]

sessions = sessions[np.isin(sessions['target_name'], target_name)]

session_ids = sessions['session_id'].unique()
animal_ids = [sessions.loc[sessions['session_id'] == s, 'animal_id'].unique()[0]
              for s in session_ids]

print('\n\n\n', sessions, '\n\n\n')

# --- LOOP OVER SESSIONS AND DECODE -------------------------------------------

dfs = []


for animal_id, session_id in zip(animal_ids, session_ids):

    df_sess = pd.DataFrame(columns=['session_id'] + decoding_inputs + [target_name])
    # # --- SET UP PATHS ------------------------------------------------------------
    #
    # output_file_name = 'decode_setting_{}_{}_{}_{}_{}_{}.pkl'.format(settings_name,
    #                                                             area_spikes, area_lfp,
    #                                                             animal_id,
    #                                                             session_id, target_name)

    # output_folder    = os.path.join(DATA_FOLDER, 'results', 'dec', settings_name)
    # output_full_path = os.path.join(output_folder, output_file_name)

    # we also save a file with just the settings for ease of access later
    # pars_full_path   = os.path.join(output_folder, 'parameters_decode_setting_{}.pkl'.format(settings_name))
    #
    # if not os.path.isdir(output_folder):
    #     os.makedirs(output_folder)


    # --- PREPARE DATA ------------------------------------------------------------

    session = Session(animal_id=animal_id, session_id=session_id)


    session.load_data(load_lfp=process_lfp)

    trial_numbers = session.select_trials(only_correct=only_correct_trials,
                                          trial_type=trial_type,
                                          auditory_change=auditory_change,
                                          auditory_post=auditory_post,
                                          visual_change=visual_change)

    # TARGET
    y = session.make_target(trial_numbers, target_name=target_name)
    ll = LabelEncoder()
    y = ll.fit_transform(y)
    df_sess[target_name] = y


    # Activity pre-stimulus
    time_before_in_s = 0.2
    time_after_in_s = - 0.009
    binsize_in_ms = (time_before_in_s + time_after_in_s) * 1000 - 1
    trial_times = session.get_aligned_times(trial_numbers,
                                            time_before_in_s=time_before_in_s,
                                            time_after_in_s=time_after_in_s)


    binned_spikes, spike_bin_centers = session.bin_spikes_per_trial(binsize_in_ms, trial_times,
                                                                    sliding_window=False)

    V1_unit_ind = session.select_units(area='V1')
    V1_binned_spikes = [s[V1_unit_ind, :] for s in binned_spikes]
    V1_total_spikes = [s.sum() for s in V1_binned_spikes]
    df_sess['V1_total_spikes'] = V1_total_spikes

    if process_lfp:
        session.quick_downsample_lfp(factor=lfp_downsample_factor)

        freq_bands = session.make_frequency_bands(low_freq, mid_freq, high_freq, spacing_low, spacing_high)

        session.set_filter_parameters(freq_bands, transition_width, bandpass_attenuation)

        lfp_channel_id = session.get_random_channel_id(area=area_lfp,
                                                       layer=layer_lfp,
                                                       in_the_middle=ch_in_the_middle_of_the_probe)

        session.bandpass_filter(lfp_channel_id)

        interp_phase, interp_energy = session.interpolate_phase_and_energy_per_trial(spike_bin_centers)

        if discretize_phase:
            binned_phase = session.discretize_phase(interp_phase, n_bins=n_phase_bins)
        else:
            binned_phase = interp_phase

        if discretize_energy:
            binned_energy = session.discretize_energy(interp_energy, n_bins=n_energy_bins)
        else:
            binned_energy = interp_energy


    df_sess['session_id'] = session_id
    dfs.append(df_sess)


df = pd.concat(dfs)

def performance_metric(y_test, y_pred):
    return accuracy_score(y_true=y_test, y_pred=y_pred)




scores_all_features = np.zeros(n_repeats)
scores_single_features = np.zeros([len(decoding_inputs), n_repeats])

for repeat in range(n_repeats):
    decoder = RandomForestClassifier(n_estimators=n_estimators)

    X = df[decoding_inputs].values
    y = df[target_name].values

    score, targ_true, targ_pred = get_kfold_score(X, y, decoder, n_splits=n_splits,
                                                  seed=repeat,
                                                  shuffle=shuffle_kfold,
                                                  score_name=score_name)
    scores_all_features[repeat] = score


    for i, feature in enumerate(decoding_inputs):
        decoding_inputs_minus_one = [d for d in decoding_inputs if d != feature]
        X = df[decoding_inputs_minus_one].values

        score, targ_true, targ_pred = get_kfold_score(X, y, decoder,
                                                      n_splits=n_splits,
                                                      seed=repeat,
                                                      shuffle=shuffle_kfold,
                                                      score_name=score_name)
        scores_single_features[i, repeat] = score


