import numpy as np
import matplotlib.pyplot as plt

a = np.array([1, 359, 1]) * np.pi / 180

times = [0, 2, 4]

new_times = [1, 3]

a_int = np.interp(new_times, times, a, period=2*np.pi)


f, ax = plt.subplots()
ax.plot(times, a)
ax.scatter(times, a)
ax.plot(new_times, a_int)
ax.scatter(new_times, a_int)



from scipy import interpolate




data = np.array([[0, 2, 4, 6, 8], [1, 179, 211, 359, 1]])


complement360 = np.rad2deg(np.unwrap(np.deg2rad(data[1])))
f = interpolate.interp1d(data[0], complement360, kind='linear', bounds_error=False,
                         fill_value=None)
a_int = f(np.arange(9))%360

f, ax = plt.subplots()
ax.plot(times, a)
ax.scatter(times, a)
ax.plot(new_times, a_int)
ax.scatter(new_times, a_int)