


import numpy as np
import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from constants import *
from loadmat import *
from session import Session
import quantities as pq
from sklearn.model_selection import StratifiedKFold
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score, recall_score
from matplotlib.ticker import MaxNLocator
import pickle
from utils import shuffle_array_rows_within_labels


animal_id = '2012'
session_id = '2018-08-14_14-30-15'

# TIME PARAMETERS
binsize_in_ms = 50
time_before_stim_in_s = 0.1
time_after_stim_in_s = 0.1

plot_format = 'png'
dpi = 400
plot_folder = os.path.join(DATA_FOLDER,  'plots', 'lfp_exploratory')

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder)

# -----------------------------------------------------------------------------


session = Session(animal_id=animal_id, session_id=session_id)
session.load_data(load_lfp=False)
times = session.get_aligned_times(trial_numbers=6,
                                  time_before_in_s=time_before_stim_in_s,
                                  time_after_in_s=time_after_stim_in_s)


#
# binned_spikes, spike_bin_centers = session.bin_spikes(binsize_in_ms=50, t_start_in_us=times[0][0],
#                    t_stop_in_us=times[0][1], sliding_window=False, slide_by_in_ms=50)
#
# binned_spikes_2, spike_bin_centers_2 = session.bin_spikes(binsize_in_ms=50, t_start_in_us=times[0][0],
#                    t_stop_in_us=times[0][1], sliding_window=True, slide_by_in_ms=50)
#
#
# np.testing.assert_array_equal(binned_spikes, binned_spikes_2)
# np.testing.assert_array_equal(spike_bin_centers, spike_bin_centers_2)


binned_spikes, spike_bin_centers = session.bin_spikes(binsize_in_ms=50, t_start_in_us=times[0][0],
                   t_stop_in_us=times[0][1], sliding_window=True, slide_by_in_ms=25)

time_bin_times = (spike_bin_centers - spike_bin_centers[0]).rescale(pq.s) \
                 + (binsize_in_ms*pq.ms).rescale(pq.s)/2 - time_before_stim_in_s*pq.s
time_bin_times = time_bin_times.__array__().round(5)


t_start_in_us=times[0][0]
t_stop_in_us=times[0][1]
slide_by_in_us = 50 * 1000
binsize_in_us = 100 * 1000

left_edges = np.arange(t_start_in_us, t_stop_in_us, slide_by_in_us)
print(left_edges)

bins = [(e, e + binsize_in_us) for e in left_edges]

bins = [b for b in bins if (b[0] + b[1]) / 2 <= (t_stop_in_us)]
bins = [b for b in bins if b[1] <= t_stop_in_us]

spike_bin_centers = [(b1 + b2) / 2 for b1, b2 in bins]

print(len(spike_bin_centers))
print(spike_bin_centers)
print(t_stop_in_us)