



def make_sessions_df(include_n_units=False, include_lfp_layer=False):

    df = pd.DataFrame(columns=['animal_id', 'session_id', 'area', 'trial_type',
                               'target_name', 'only_correct', 'nt',
                               'nt_0', 'nt_1', 'n_units', 'n_units_IG',
                               'n_units_SG', 'n_units_NA', 'n_chan_IG',
                               'n_chan_SG', 'n_chan_NA'])

    for animal_id in decoding_sessions.keys():
        for session_id in decoding_sessions[animal_id]:

            session = Session(animal_id=animal_id, session_id=session_id)

            load_spikes = False
            load_lfp    = False

            if include_n_units:
                load_spikes = True
            if include_lfp_layer:
                load_lfp = True

            session.load_data(load_spikes=load_spikes, load_lfp=load_lfp)

            if include_n_units:
                nV1 = (session.spike_data['area'] == 'V1').sum()
                nPPC = (session.spike_data['area'] == 'PPC').sum()
                nCG1 = (session.spike_data['area'] == 'CG1').sum()
            else:
                nV1 =  'none'
                nPPC = 'none'
                nCG1 = 'none'


            for area in ['V1', 'PPC', 'CG1']:
                for layer in ['IG', 'SG', 'NA']:
                    mask = np.logical_and(session.lfp_data['layer'] == layer,
                                          session.lfp_data['area'] == area)
                    ids = session.lfp_data['channel_ID'][mask]


            for trial_type in ['X', 'Y']:
                for only_correct in [True, False]:

                    if trial_type == 'X':
                        target_name = 'visualOriPostNorm'
                    if trial_type == 'Y':
                        target_name = 'audioFreqPostNorm'

                    tn = session.select_trials(only_correct=only_correct,
                                               trial_type=trial_type)

                    y_all = session.make_target(tn, target_name=target_name,
                                    coarse=False)

                    if not np.unique(y_all).shape[0] <= 4:
                        warnings.warn('Session {} {} is FUNKY'.format(animal_id, session_id))

                    y = session.make_target(tn, target_name=target_name,
                                    coarse=True)
                    nt = len(tn)
                    nt_0 = (y==0).sum()
                    nt_1 = (y==1).sum()



                    #assert nt_0 + nt_1 == nt
                    row = [animal_id, session_id, trial_type, target_name,
                           only_correct, nt, nt_0, nt_1, nV1, nPPC, nCG1]
                    df.loc[df.shape[0], :] = row


    ind = df.loc[(df['animal_id'] == '2009') &
           (df['session_id'] == '2018-08-24_11-56-35') &
           (df['target_name'] == 'visualOriPostNorm')].index

    df = df.drop(ind)

    # just to make sure
    sel = df.loc[(df['animal_id'] == '2009') &
           (df['session_id'] == '2018-08-24_11-56-35') &
           (df['target_name'] == 'visualOriPostNorm')]
    assert sel.shape[0] == 0

    return df



def get_sessions_for_decoding(min_trials_per_stim, only_correct=False,
                              include_n_units=True):

    df = make_sessions_df(include_n_units=include_n_units)
    sessions = df.loc[(df.only_correct == only_correct) &
                      (df.nt_0 >= min_trials_per_stim) &
                      (df.nt_1 >= min_trials_per_stim)]

    return sessions