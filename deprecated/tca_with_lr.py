import numpy as np
import pandas as pd
from constants import *
from loadmat import *
from session import Session
import scipy
from sklearn.model_selection import StratifiedKFold
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score, recall_score
import pickle
import quantities as pq
from utils import shuffle_array_rows_within_labels
from sklearn.ensemble import ExtraTreesClassifier
import distutils
from unit_scores import SingleUnitDiscriminationScores
from sklearn.preprocessing import StandardScaler, MinMaxScaler
import tensortools as tt
from plotting_style import *
import matplotlib.pyplot as plt
from session_info import select_sessions
import itertools
from matplotlib.lines import Line2D

settings_name = 'alignedtolick'

# if animal_id is specified, run for all sessions of that animal
# if area_spikes is not specified run for all available areas
animal_id = None
session_id = None
area_spikes = 'all'  # if 'all' do one TCA for all units, otherwise area individually
min_units_per_area = 20

# animal_id = '2003'
# session_id = '2018-02-08_15-13-56'#'2018-02-07_14-28-57'
# area_spikes = 'all'

# TCA PARAMETERS
tca_rank = 4
fit_ensemble = False


# TIME PARAMETERS
align_event = 'stimChange' #'firstlickTime' #'stimChange'
sliding_window = False
time_before_stim_in_s = 0.4
time_after_stim_in_s = 0.8
binsize_in_ms = 1
slide_by_in_ms = -10

# SMOOTHING PARAMETERS
apply_smoothing = True
sampling_period = 10
kernel_width = 30
kernel_size = kernel_width * 8
kernel = scipy.signal.gaussian(kernel_size, kernel_width, sym=True)
kernel = 1000 * kernel / kernel.sum()

preprocess = 'z_score'


# PLOTTING PARAMETERS
plot_format = 'png'
dpi = 400
save_plots = True
plot_folder = os.path.join(DATA_FOLDER, 'plots', 'TCA', settings_name, 'rank_{}'.format(tca_rank))


if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder, exist_ok=True)

# SELECT TRIALS

#modalities = ['X', 'X', 'Y', 'Y']
# stimuli = [(1, 2), (3, 4), (1, 2), (3, 4)]
# stimulus_changes = [(1, 2), (1, 2), (1, 2), (1, 2)]

responses = [0, 1]
modalities = ['X', 'Y']
response_side = ['L', 'R']
stimuli = [(1, 2), (3, 4)]
stimulus_changes = [1, 2]


trial_types = list(itertools.product(responses, modalities, response_side,
                                     stimuli, stimulus_changes))



# --- UTILITIES ---------------------------------------------------------------

def convolve_chunk(chunk, kernel, p):
    kernel_size = kernel.shape[0]
    rate = np.apply_along_axis(lambda m: np.convolve(m, kernel, mode='same'),
                               axis=1, arr=chunk)
    rate = rate[:, int(kernel_size / 2):-int(kernel_size / 2)]
    rate = rate[:, ::p]
    return rate


# --- PREPARE DATA ------------------------------------------------------------

if session_id is None:
    sessions = select_sessions(min_units=min_units_per_area)

    if area_spikes == 'all':
        sessions = sessions.drop_duplicates(subset=['animal_id', 'session_id'])
    else:
        sessions = sessions.drop_duplicates(subset=['animal_id', 'session_id',
                                                    'area'])

    if animal_id is not None:
        sessions = sessions[sessions['animal_id'] == animal_id]

    if area_spikes is not None and area_spikes != 'all':
        sessions = sessions[sessions['area'] == area_spikes]

else:
    sessions = pd.DataFrame(columns=['animal_id', 'session_id', 'area'],
                            data=[(animal_id, session_id, area_spikes)])


for i, row in sessions.iterrows():

    animal_id = row['animal_id']
    session_id = row['session_id']
    if not area_spikes == 'all':
        area_spikes = row['area']

    session = Session(animal_id=animal_id, session_id=session_id)
    session.load_data(load_lfp=False)


    binned_spikes_all = {}
    n_trials_all = {}
    trial_nums = []

    for response, modality, response_side, stimulus, change in trial_types:

        if modality == 'X':
            trial_numbers = session.select_trials(response=response,
                                                  trial_type=modality,
                                                  response_side=response_side,
                                                  visual_post_norm=stimulus,
                                                  visual_change=change)
        elif modality == 'Y':

            trial_numbers = session.select_trials(response=response,
                                                  trial_type=modality,
                                                  response_side=response_side,
                                                  audio_post_norm=stimulus,
                                                  auditory_change=change)

        trial_nums.append(trial_numbers)
        n_trials_all[(response, modality, response_side, stimulus, change)] = len(trial_numbers)

        if len(trial_numbers) > 0:
            trial_times = session.get_aligned_times(trial_numbers,
                                                    time_before_in_s=time_before_stim_in_s,
                                                    time_after_in_s=time_after_stim_in_s,
                                                    event=align_event)


            binned_spikes, spike_bin_centers = session.bin_spikes_per_trial(binsize_in_ms, trial_times,
                                                                            sliding_window=sliding_window,
                                                                            slide_by_in_ms=slide_by_in_ms)


            selected_unit_ind = session.select_units(area=area_spikes)
            selected_unit_id = session.get_cell_id(selected_unit_ind, shortened_id=False)

            unit_area = session.get_cell_area_from_cell_ind(selected_unit_ind)

            binned_spikes = [s[selected_unit_ind, :] for s in binned_spikes]

            if apply_smoothing:
                binned_spikes = [convolve_chunk(arr, kernel, sampling_period) for
                                 arr in binned_spikes]

            binned_spikes_all[(response, modality, response_side, stimulus, change)] = binned_spikes



    n_time_bins_per_trial = spike_bin_centers[0].shape[0]

    time_bin_times = (spike_bin_centers[0] - spike_bin_centers[0][0]).rescale(pq.s) \
                     + (binsize_in_ms*pq.ms).rescale(pq.s)/2 - time_before_stim_in_s*pq.s
    time_bin_times = time_bin_times.__array__().round(5)
    if apply_smoothing:
        time_bin_times = time_bin_times[int(kernel_size / 2):-int(kernel_size / 2)]
        time_bin_times = time_bin_times[::sampling_period]


    # --- PREPARE TENSOR ----------------------------------------------------------

    actual_trial_types = list(binned_spikes_all.keys()) # where we have nonzero n of trials
    n_neurons = binned_spikes_all[actual_trial_types[0]][0].shape[0]
    n_all_trials = np.sum([n_trials_all[k] for k in trial_types])
    n_times = binned_spikes_all[actual_trial_types[0]][0].shape[1]

    X = np.hstack([np.hstack(binned_spikes_all[t]) for t in binned_spikes_all.keys()])
    #[len(binned_spikes_all[t]) for t in trial_groups]

    if preprocess == 'min_max':
        mm = MinMaxScaler(feature_range=(0, 1))
        X = mm.fit_transform(X.T).T
    elif preprocess == 'z_score':
        ss = StandardScaler(with_mean=True, with_std=True)
        X = ss.fit_transform(X.T).T

    X = np.reshape(X, (n_neurons, n_times, n_all_trials), order='F')


    modality_list, stimulus_list = [], []
    stimulus_change_list, response_list = [], []
    response_side_list = []

    for response, modality, response_side, stimulus, change in trial_types:
        for n in range(n_trials_all[(response, modality, response_side, stimulus, change)]):
            modality_list.append(modality)
            response_side_list.append(response_side)
            stimulus_list.append(stimulus)
            stimulus_change_list.append(change)
            response_list.append(response)



    # --- RUN TCA -----------------------------------------------------------------


    U = tt.ncp_bcd(X, rank=tca_rank, verbose=True)

    if fit_ensemble:
        E = tt.Ensemble(nonneg=True, fit_method='ncp_bcd')
        E.fit(X, ranks=[2, 4, 6, 8, 10, 12, 14, 16, 18], replicates=10)

        f, ax = plt.subplots(1, 2, figsize=[6, 3])
        tt.plot_objective(E, ax=ax[0])
        tt.plot_similarity(E, ax=ax[1])
        sns.despine()
        plt.tight_layout()

    # --- PLOT TCA --------------------------------------------------------

    unit_area_color = [area_palette[k] for k in unit_area]
    mod_color = [modality_palette[k] for k in modality_list]
    side_color = [response_side_palette[k] for k in response_side_list]
    stim_color = [stimulus_palette[k] for k in stimulus_list]
    stim_ch_color = [stimulus_change_palette[k] for k in stimulus_change_list]
    resp_color = [response_palette[k] for k in response_list]

    colors = [resp_color, mod_color, side_color, stim_color, stim_ch_color]

    trial_inds = (np.hstack(trial_nums) - 1).astype(int)


    trial_inds = trial_inds.argsort()

    titles = ['Unit factors', 'Time factors', 'Trial factors\nResponse',
              'Trial factors\nModality', 'Trial factors\nResponse side',
              'Trial factors\nStimulus identity',
              'Trial factors\nStimulus change', 'Trial factors\nReal order']

    xlabels = ['Units', 'Time (s)', 'Trials', 'Trials', 'Trials', 'Trials',
               'Trials', 'Trials']


    trial_scatter_colors = [resp_color, mod_color, side_color, stim_color, stim_ch_color,
                            sns.xkcd_rgb['grey']]
    n_scatters = len(trial_scatter_colors)

    plots=['bar', 'line'] + ['scatter'] * n_scatters
    ndim = 2+n_scatters

    fig, axes = plt.subplots(U.factors.rank, ndim, figsize=[ndim*2.5, U.factors.rank*1.1])

    # main loop, plot each factor
    plot_obj = np.empty((U.factors.rank, ndim), dtype=object)
    for r in range(U.factors.rank):
        for i in range(len(plots)):
            # start plots at 1 instead of zero

            if i < 2:
                f = U.factors[i]
            else:
                f = U.factors[2]

            x = np.arange(1, f.shape[0]+1)

            # determine type of plot
            if plots[i] == 'bar':
                plot_obj[r, i] = axes[r, i].bar(x, f[:, r], color=unit_area_color)
                axes[r, i].set_xlim(0, f.shape[0]+1)
            elif plots[i] == 'scatter':
                if i == (ndim - 1):
                    y = f[trial_inds, r]
                else:
                    y = f[:, r]
                plot_obj[r, i] = axes[r, i].scatter(x, y, s=8, c=trial_scatter_colors[i-2])
                axes[r, i].set_xlim(0, f.shape[0])
            elif plots[i] == 'line':
                plot_obj[r, i] = axes[r, i].plot(time_bin_times, f[:, r], '-')
                #axes[r, i].set_xlim(0, f.shape[0])
            else:
                raise ValueError('invalid plot type')

            # format axes
            axes[r, i].locator_params(nbins=4)
            axes[r, i].spines['top'].set_visible(False)
            axes[r, i].spines['right'].set_visible(False)
            axes[r, i].xaxis.set_tick_params(direction='out')
            axes[r, i].yaxis.set_tick_params(direction='out')
            axes[r, i].yaxis.set_ticks_position('left')
            axes[r, i].xaxis.set_ticks_position('bottom')

            # remove xticks on all but bottom row
            if r != U.factors.rank-1:
                plt.setp(axes[r, i].get_xticklabels(), visible=False)

    for i in range(ndim):
        axes[0, i].set_title(titles[i])
        axes[-1, i].set_xlabel(xlabels[i])

    # link y-axes within columns
    for i in range(ndim):
        yl = [a.get_ylim() for a in axes[:, i]]
        y0, y1 = min([y[0] for y in yl]), max([y[1] for y in yl])
        [a.set_ylim((y0, y1)) for a in axes[:, i]]

    # format y-ticks
    for r in range(U.factors.rank):
        for i in range(ndim):
            # only two labels
            ymin, ymax = np.round(axes[r, i].get_ylim(), 2)
            axes[r, i].set_ylim((ymin, ymax))

            # remove decimals from labels
            if ymin.is_integer():
                ymin = int(ymin)
            if ymax.is_integer():
                ymax = int(ymax)
            # update plot
            axes[r, i].set_yticks([ymin, ymax])

    #box = axes[0, -1].get_position()

    palette_titles = ['Area', 'Modality', 'Stimulus']
    palettes = [area_palette, modality_palette,
                stimulus_palette]

    def get_markers_and_labels(palette):
        markers, labels = [], []
        for key in palette.keys():
            labels.append(key)
            markers.append(Line2D([0], [0], marker='o', color=palette[key], lw=0))
        return markers, labels

    for i, palette in enumerate(palettes):
        markers, labels = get_markers_and_labels(palette)

        axes[i, -1].legend(markers, labels, loc='center left',
                           title=palette_titles[i],
                           bbox_to_anchor=(1.04, 0.5), frameon=False)
        leg = axes[i, -1].get_legend()
        leg._legend_box.align = "left"

    for i in range(axes.shape[0]):
        for j in range(2, axes.shape[1]):
            axes[i, j].set_ylim(-0.55, 5)


    plt.tight_layout(rect=[0,0,0.9,1])



    plot_name = 'TCA_{}_{}_{}_rank_{}.{}'.format(animal_id, session_id,
                                                 area_spikes, tca_rank, plot_format)
    fig.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)
