import numpy as np
import pandas as pd
from constants import *
from loadmat import *
from session import Session
import scipy
from sklearn.model_selection import StratifiedKFold
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score, recall_score
import pickle
import quantities as pq
from utils import shuffle_array_rows_within_labels
from sklearn.ensemble import ExtraTreesClassifier
import distutils
from unit_scores import SingleUnitDiscriminationScores
from sklearn.preprocessing import StandardScaler, MinMaxScaler
import tensortools as tt
from plotting_style import *
import matplotlib.pyplot as plt
from session_info import select_sessions
import itertools
from matplotlib.lines import Line2D
import scipy.stats
from sklearn import preprocessing
from sklearn import model_selection
from sklearn.metrics import roc_auc_score

settings_name = 'May22'
tca_rank = 8


# FILTERING FACTORS PARAMETERS
# if a parameter is None, e.g. min_score=None, min_score is set as the
# lowest quantile, with the number of quantiles set by q
filter_factors = True
min_perc_nonzero_units = None
min_perc_nonzero_trials = None
min_variation = None
min_score = 0.6
q = 10


# TODO load from pars file
time_before_stim_in_s = 0.4
time_after_stim_in_s = 0.8

# PLOTTING PARAMETERS
plot_format = 'png'
dpi = 400
save_plots = True


# --- LOAD RESULTS ------------------------------------------------------------

results_folder = os.path.join(DATA_FOLDER, 'results', 'TCA', settings_name)
results_file_name = 'TCA_settings_{}_rank_{}.pkl'.format(settings_name, tca_rank)
rs = pickle.load(open(os.path.join(results_folder, results_file_name), 'rb'))


# --- SET UP PATHS ------------------------------------------------------------

plot_folder = os.path.join(DATA_FOLDER, 'plots', 'TCA_agg', settings_name,
                            'rank_{}'.format(tca_rank))

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder, exist_ok=True)


# --- IDENTIFY DRIFT COMPONENTS -----------------------------------------------

def is_bad_factor(U, rank, min_perc_nonzero_units=5,
                  min_perc_nonzero_trials=5, min_variation=2):
    unit_factors = U.factors[0][:, rank]
    time_factors = U.factors[1][:, rank]
    trial_factors = U.factors[2][:, rank]

    # number of nonzero units
    perc_nonzero_units = 100 * (unit_factors > 0).sum() / unit_factors.shape[0]

    perc_nonzero_trials = 100 * (trial_factors > 0).sum() / \
                          trial_factors.shape[0]

    max_variation = np.abs((time_factors - time_factors.mean())
                           / time_factors.std()).max()

    enough_units = perc_nonzero_units >= min_perc_nonzero_units
    enough_trials = perc_nonzero_trials >= min_perc_nonzero_trials
    enough_variation = max_variation >= min_variation

    if enough_units and enough_trials and enough_variation:
        is_bad = False
    else:
        is_bad = True
    return is_bad




def get_perc_nonzero_units(U, rank):
    unit_factors = U.factors[0][:, rank]
    perc_nonzero_units = 100 * (unit_factors > 0).sum() / unit_factors.shape[0]
    return perc_nonzero_units

def get_perc_nonzero_trials(U, rank):
    trial_factors = U.factors[2][:, rank]
    perc_nonzero_trials = 100 * (trial_factors > 0).sum() / trial_factors.shape[0]
    return perc_nonzero_trials

def get_max_variation(U, rank):
    time_factors = U.factors[1][:, rank]
    max_variation = np.abs((time_factors - time_factors.mean())
                           / time_factors.std()).max()
    return max_variation

# --- PREPARE DATAFRAME -------------------------------------------------------

df = pd.DataFrame(columns=['animal_id', 'session_id', 'area',
                           'factor', 'peak_time', 'kurtosis',
                           'perc_nonzero_units', 'perc_nonzero_trials',
                           'max_variation', 'nonzero_mean_factor',
                           'target', 'score', 'unit_factors'])


for (animal_id, session_id, area_spikes) in rs.keys():
    print(animal_id, session_id, area_spikes)


    U = rs[(animal_id, session_id, area_spikes)]['factors']
    response_list = rs[(animal_id, session_id, area_spikes)]['response_list']
    modality_list = rs[(animal_id, session_id, area_spikes)]['modality_list']
    stimulus_list = rs[(animal_id, session_id, area_spikes)]['stimulus_list']
    stimulus_change_list = rs[(animal_id, session_id, area_spikes)]['stimulus_change_list']
    time_bin_times = rs[(animal_id, session_id, area_spikes)]['time_bin_times']

    unit_factors = U.factors[0]
    time_factors = U.factors[1]
    trial_factors = U.factors[2]

    targets = [response_list, modality_list, stimulus_list, stimulus_change_list]
    target_names = ['resp', 'mod', 'stim', 'stim_ch']

    for k in range(tca_rank):

        perc_nonzero_units = get_perc_nonzero_units(U, k)
        perc_nonzero_trials = get_perc_nonzero_trials(U, k)
        max_variation = get_max_variation(U, k)

        peak_time = time_bin_times[time_factors[:, k].argmax()]
        X = trial_factors[:, k][:, np.newaxis]

        kurtosis = scipy.stats.kurtosis(unit_factors[:, k])

        x = unit_factors[:, k]
        nonzero_mean = x[x>0].mean()

        for target_name, y in zip(target_names, targets):
            score = roc_auc_score(y, X.flatten())

            ind = df.shape[0]
            df.loc[ind, :] = [animal_id, session_id, area_spikes,
                              k, peak_time, kurtosis, perc_nonzero_units,
                              perc_nonzero_trials, max_variation, nonzero_mean,
                                      target_name, score, None]
            df.loc[ind, 'unit_factors'] = unit_factors[:, k]


numeric_cols = ['score', 'peak_time', 'kurtosis', 'perc_nonzero_units']
for col in numeric_cols:
    df[col] = pd.to_numeric(df[col])

print('ASSUMING SCORE IS AUC - MAKING MONOTONIC')
df['score'] = [s if s>0.5 else 1-s for s in df['score']]


# --- FILTER OUT BAD FACTORS --------------------------------------------------
if filter_factors:

    if min_perc_nonzero_units is None:
        min_perc_nonzero_units = pd.qcut(df['perc_nonzero_units'], q).cat.categories[0].right

    if min_perc_nonzero_trials is None:
        min_perc_nonzero_trials = pd.qcut(df['perc_nonzero_trials'], q).cat.categories[0].right

    if min_variation is None:
        min_variation = pd.qcut(df['max_variation'], q).cat.categories[0].right

    if min_score is None:
        min_score = pd.qcut(df['score'], q).cat.categories[0].right

    groupbys = ['animal_id', 'session_id', 'area', 'factor']

    df = df.groupby(groupbys).filter(lambda x: x['perc_nonzero_units'].max() >= min_perc_nonzero_units)
    df = df.groupby(groupbys).filter(lambda x: x['perc_nonzero_trials'].max() >= min_perc_nonzero_trials)
    df = df.groupby(groupbys).filter(lambda x: x['max_variation'].max() >= min_variation)
    df = df.groupby(groupbys).filter(lambda x: x['score'].max() >= min_score)




# --- OVERLAP BY PREFERRED TARGET ---------------------------------------------
target_names = ['resp', 'mod', 'stim', 'stim_ch']

df_sel = df.loc[df.groupby(['animal_id', 'session_id', 'area', 'factor'])['score'].idxmax()]

overlap_mat = np.zeros((len(target_names), len(target_names)))
n_mat = np.zeros_like(overlap_mat)

for (animal_id, session_id, area_spikes) in rs.keys():

    dfx = df_sel[(df_sel['animal_id'] == animal_id) &
                 (df_sel['session_id'] == session_id) &
                 (df_sel['area'] == area_spikes)]

    for i in range(dfx.shape[0]):

        for j in range(dfx.shape[0]):
            un1 = dfx.iloc[i]['unit_factors']
            un2 = dfx.iloc[j]['unit_factors']

            tar1 = dfx.iloc[i]['target']
            tar2 = dfx.iloc[j]['target']

            un1 = preprocessing.normalize(un1.reshape(-1, 1), norm='l2', axis=0)
            un2 = preprocessing.normalize(un2.reshape(-1, 1), norm='l2', axis=0)

            overlap = np.nansum(un1 * un2)

            overlap_mat[target_names.index(tar1), target_names.index(tar2)] += overlap
            n_mat[target_names.index(tar1), target_names.index(tar2)] += 1

overlap_mat_mean = overlap_mat / n_mat

labels = [targets_labels[t].replace(" ","\n") for t in target_names]
f, ax = plt.subplots(1, 1)
sns.heatmap(overlap_mat_mean, ax=ax, cmap='Blues', annot=True,  linewidths=.8)
ax.set_xticklabels(labels, rotation=0)
ax.set_yticklabels(labels, rotation=0)
ax.tick_params(axis=u'both', which=u'both',length=0)
plt.tight_layout()


plot_name = 'TCA_factor_overlap_by_preferred_target_{}_{}.{}'.format(settings_name, tca_rank, plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi,bbox_inches="tight")



# --- OVERLAP BY PEAK TIME ----------------------------------------------------

df_sel = df.drop_duplicates(subset=['animal_id', 'session_id', 'area', 'factor'])

bins = np.arange(-time_before_stim_in_s, time_after_stim_in_s, 0.1).round(2)

df_sel['time_bin'] = pd.cut(df_sel['peak_time'], bins=bins).cat.codes

overlap_mat = np.zeros((len(bins)-1, len(bins)-1))
n_mat = np.zeros_like(overlap_mat)

for (animal_id, session_id, area_spikes) in rs.keys():

    dfx = df_sel[(df_sel['animal_id'] == animal_id) &
                 (df_sel['session_id'] == session_id) &
                 (df_sel['area'] == area_spikes)]

    for i in range(dfx.shape[0]):
        for j in range(dfx.shape[0]):
            un1 = dfx.iloc[i]['unit_factors']
            un2 = dfx.iloc[j]['unit_factors']

            un1 = preprocessing.normalize(un1.reshape(-1, 1), norm='l2', axis=0)
            un2 = preprocessing.normalize(un2.reshape(-1, 1), norm='l2', axis=0)


            tb1 = dfx.iloc[i]['time_bin']
            tb2 = dfx.iloc[j]['time_bin']

            overlap = np.nansum(un1 * un2)

            overlap_mat[tb1, tb2] += overlap
            n_mat[tb1, tb2] += 1

n_mat[n_mat==0] = 1
overlap_mat_mean = overlap_mat / n_mat

labels = ['{} to {}s'.format(bins[i], bins[i+1]) for i in range(len(bins)-1)]
f, ax = plt.subplots(1, 1)
sns.heatmap(overlap_mat_mean, ax=ax, cmap='Blues', annot=False, linewidths=.8,
            yticklabels=labels, xticklabels=labels)
ax.tick_params(axis=u'both', which=u'both',length=0)
plt.tight_layout()

plot_name = 'TCA_factor_overlap_by_peak_time_{}_{}.{}'.format(settings_name, tca_rank, plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi,bbox_inches="tight")



# --- PLOT UNIT FACTOR DISTRIBUTION SPLIT BY TIME -----------------------------


split_time = 0.3

df_sel = df.copy()

st1 = 'Peak before\n{} ms'.format(int(split_time*1000))
st2 = 'Peak after\n{} ms'.format(int(split_time*1000))

df_sel['time_split'] = [st1 if k < split_time else st2 for k in df_sel['peak_time']]
df_sel = df_sel.groupby(['animal_id', 'session_id', 'area',
                         'time_split', 'factor']).mean().reset_index()

f, ax = plt.subplots(1, 1, figsize=[small_square_side, small_square_side])

sns.barplot(data=df_sel, x='time_split', y='perc_nonzero_units', ax=ax,
            hue='area', hue_order=AREAS, palette=[area_palette[a] for a in AREAS])
ax.set_xlabel('')
ax.set_ylabel('Units with\nnonzero factor (%)')
ax.get_legend().remove()
sns.despine()
plt.tight_layout()


f, ax = plt.subplots(1, 1, figsize=[small_square_side, small_square_side])

sns.barplot(data=df_sel, x='time_split', y='kurtosis', ax=ax,
            hue='area', hue_order=AREAS, palette=[area_palette[a] for a in AREAS])
ax.set_xlabel('')
ax.set_ylabel('Kurtosis of unit factors')
ax.get_legend().remove()
sns.despine()
plt.tight_layout()


# --- PLOT UNIT FACTOR DISTRIBUTION VERSUS DISCRIMINATION ---------------------

df_sel = df.copy()
df_sel = df_sel[df_sel['target'] == 'mod']
df_sel = df_sel.drop(['target', 'score'], axis=1)

for target in ['mod', 'resp', 'stim', 'stim_ch']:
    df_alt = df[df['target'] == target]
    for col in ['session_id', 'animal_id', 'area']:
        np.testing.assert_array_equal(df_sel[col], df_alt[col])

for target in ['resp', 'mod', 'stim', 'stim_ch']:
    df_alt = df[df['target'] == target]
    df_sel['score_{}'.format(target)] = df_alt['score'].as_matrix()


labels = {'score_resp' : 'Discrimination score\n(response)',
          'score_mod'  : 'Discrimination score\n(modality)',
          'score_stim' : 'Discrimination score\n(stim. identity)',
          'score_stim_ch' : 'Discrimination score\n(stim. change)',
          'peak_time' : 'Peak time of\ntime factor',
          'kurtosis'  : 'Kurtosis of\nunit factor',
          'perc_nonzero_units' : '% of nonzero entries\nin unit factor'}


# DISCRIMINATION WITH PEAK TIME

vars = ['score_resp', 'score_mod', 'score_stim', 'score_stim_ch', 'peak_time']
g = sns.pairplot(df_sel, vars=vars, hue='area', hue_order=AREAS,
             palette=[area_palette[a] for a in AREAS], kind='reg',
             plot_kws=dict(scatter_kws=dict(edgecolor='w')))

for i in range(g.axes.shape[0]):
    g.axes[i, 0].set_ylabel(labels[vars[i]])
    g.axes[-1, i].set_xlabel(labels[vars[i]])

for i in range(g.axes.shape[0]-1):
    for j in range(g.axes.shape[0]-1):
        g.axes[i, j].set_xlim([0, 1])
        g.axes[i, j].set_ylim([0, 1])
for i in range(g.axes.shape[0]):
    g.axes[-1, i].set_ylim(-time_before_stim_in_s, time_after_stim_in_s)
    g.axes[i, -1].set_xlim(-time_before_stim_in_s, time_after_stim_in_s)
plt.tight_layout(rect=(0, 0, 0.94, 1))

plot_name = 'TCA_pairplot_discrimination_vs_peak_' \
            'time_settings_{}_rank_{}.{}'.format(settings_name, tca_rank, plot_format)
g.savefig(os.path.join(plot_folder, plot_name), dpi=dpi,bbox_inches="tight")



# UNIT FACTORS WITH PEAK TIME

vars = ['kurtosis', 'perc_nonzero_units', 'peak_time']
g = sns.pairplot(df_sel, vars=vars, hue='area', hue_order=AREAS,
             palette=[area_palette[a] for a in AREAS], kind='reg',
             plot_kws=dict(scatter_kws=dict(edgecolor='w')))
for i in range(g.axes.shape[0]):
    g.axes[i, 0].set_ylabel(labels[vars[i]])
    g.axes[-1, i].set_xlabel(labels[vars[i]])
plt.tight_layout(rect=(0, 0, 0.94, 1))

plot_name = 'TCA_pairplot_unit_factors_vs_peak_' \
            'time_settings_{}_rank_{}.{}'.format(settings_name, tca_rank, plot_format)
g.savefig(os.path.join(plot_folder, plot_name), dpi=dpi,bbox_inches="tight")



# EVERYTHING
if False:
    vars = ['score_resp', 'score_mod', 'score_stim', 'score_stim_ch',
            'peak_time', 'kurtosis', 'perc_nonzero_units']

    g = sns.pairplot(df_sel, vars=vars, hue='area', hue_order=AREAS,
                 palette=[area_palette[a] for a in AREAS])
    for i in range(g.axes.shape[0]):
        g.axes[i, 0].set_ylabel(labels[vars[i]])
        g.axes[-1, i].set_xlabel(labels[vars[i]])
    plt.tight_layout(rect=(0, 0, 0.94, 1))
    plot_name = 'TCA_pairploteverything' \
                '_settings_{}_rank_{}.{}'.format(settings_name, tca_rank, plot_format)
    g.savefig(os.path.join(plot_folder, plot_name), dpi=dpi,bbox_inches="tight")




# --- PLOT DISCRIMINATION SCORE SPLITTING COMPONENTS BY TIME ------------------

split_time = 0.22

f, ax = plt.subplots(1, 2, figsize=[2*small_square_side, small_square_side],
                     sharex=True, sharey=True)

df1 = df[df['peak_time'] < split_time]
df2 = df[df['peak_time'] >= split_time]

for i, dfx in enumerate([df1, df2]):
    sns.barplot(data=dfx, x='target', y='score', ax=ax[i],
                order=targets_palette.keys(), hue='area', hue_order=AREAS,
                palette=[area_palette[k] for k in AREAS])
    lab = [targets_labels[k._text].replace(" ","\n") for k in ax[i].get_xticklabels()]
    ax[i].set_xticklabels(lab, rotation=40)
    ax[i].set_xlabel('')
    ax[i].set_ylabel('')
    ax[i].get_legend().remove()
ax[1].legend(bbox_to_anchor=(1.04,0.5), loc="center left", borderaxespad=0,
             frameon=False)
ax[0].set_ylabel('Discrimination score')
ax[0].set_title('Components with peak\nbefore {} ms'.format(int(split_time*1000)))
ax[1].set_title('Components with peak\nafter {} ms'.format(int(split_time*1000)))
sns.despine()
plt.tight_layout(rect=[0,0,0.87,1])

plot_name = 'TCA_components_split_{}s_rank_{}.{}'.format(split_time, tca_rank,
                                             plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi,
            bbox_inches="tight")


target_names = df.target.unique()

df_sel = df.copy()
#do any preselction here

bw = 0.05
for target in target_names:
    g = sns.jointplot(x=[], y=[], height=small_square_side*1.5)

    for i, area in enumerate(AREAS):
        dfx = df_sel[(df_sel['target'] == target) & (df_sel['area'] == area)]
        g.ax_joint.scatter(dfx['peak_time'], dfx['score'],
                           color=area_palette[area], s=40, edgecolor='w')


    xlim = (-time_before_stim_in_s, time_after_stim_in_s)
    g.ax_joint.set_xlim(xlim)
    ylim = g.ax_joint.get_ylim()

    for i, area in enumerate(AREAS):
        dfx = df_sel[(df_sel['target'] == target) & (df_sel['area'] == area)]
        sns.kdeplot(ax=g.ax_marg_x, data=dfx['peak_time'], bw=bw,
                    color=area_palette[area])
        sns.kdeplot(ax=g.ax_marg_y, data=dfx['score'], bw=bw,
                    color=area_palette[area], vertical=True)
        g.ax_marg_x.get_legend().remove()
        g.ax_marg_y.get_legend().remove()
        g.ax_marg_x.set_xlim(xlim)
        g.ax_marg_y.set_ylim(ylim)

    g.set_axis_labels('Peak time', 'Discrimination score\n({})'.format(targets_labels[target]))
    g.ax_joint.axvline(0, c='grey', ls='--')
    g.ax_joint.axhline(0.5, c='grey', ls=':')
    plt.tight_layout()

    plot_name = 'TCA_components_peak_vs_discr_{}_rank_{}.{}'.format(target,
                                                             tca_rank,
                                                             plot_format)
    g.savefig(os.path.join(plot_folder, plot_name), dpi=dpi,
              bbox_inches="tight")



