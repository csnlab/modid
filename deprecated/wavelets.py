import numpy as np
from matplotlib import pyplot as plt
from session import Session
from mne import create_info, EpochsArray
from mne.baseline import rescale
from mne.time_frequency import (tfr_multitaper, tfr_stockwell, tfr_morlet,
                                tfr_array_morlet)

animal_id = '2012'
session_id = '2018-08-14_14-30-15'

# SELECT CHANNELS
area_lfp = 'V1'
lfp_channel_id = None
layer_lfp = None
ch_in_the_middle_of_the_probe = True
binsize_in_ms = 20
max_freq = 100

time_before_stim_in_s = 1.0
time_after_stim_in_s = 3.0

session = Session(animal_id=animal_id, session_id=session_id)
session.load_data()

lfp_channel_id = session.get_random_channel_id(area=area_lfp,
                                               layer=layer_lfp,
                                               in_the_middle=ch_in_the_middle_of_the_probe)
channel_ind = session.get_lfp_channel_index_from_channel_id(lfp_channel_id)


# VISUAL TRIALS
trial_numbers = session.select_trials(only_correct=False, trial_type='Y')
trial_times = session.get_aligned_times(trial_numbers,
                                     time_before_in_s=time_before_stim_in_s,
                                     time_after_in_s=time_after_stim_in_s)

signal = session.get_lfp_signal_from_channel_index(channel_ind)

data = []
for times in trial_times:
    trial_signal = signal[(session.lfp_times >= times[0]) & (session.lfp_times < times[1])]
    data.append(trial_signal)

data = np.vstack(data)
data = data[:, np.newaxis, :]
ch_names = [lfp_channel_id[-10:]]

info = create_info(ch_names=ch_names, sfreq=sfreq, ch_types=['grad'])
epochs = EpochsArray(data=data, info=info)

epochs.average().plot()

freqs = np.arange(1., 100., 3.)
vmin, vmax = -3., 3.  # Define our color limits.

fig, axs = plt.subplots(1, 3, figsize=(15, 5), sharey=True)
fmin, fmax = freqs[[0, -1]]
for width, ax in zip((0.2, .7, 3.0), axs):
    power = tfr_stockwell(epochs, fmin=fmin, fmax=fmax, width=width)
    power.plot([0], baseline=None, mode='percent', axes=ax, show=False,
               colorbar=False, tmin=0.5, tmax=3.5, fmin=40, fmax=70)
    ax.set_title('Sim: Using S transform, width = {:0.1f}'.format(width))
plt.tight_layout()