import os
from constants import *
from loadmat import loadmat
import pandas as pd
import numpy as np
from scipy.signal import firwin, remez, kaiser_atten, kaiser_beta
import scipy
import matplotlib.pyplot as plt
import elephant
import numpy as np
import scipy
from scipy import signal
import neo
import matplotlib.pyplot as plt
import quantities as pq
import mne
import seaborn as sns
from utils import *

animal = '2012'
session_id = '2018-08-14_14-30-15'

data_folder = os.path.join(DATA_FOLDER, animal, session_id)

session_data_path = os.path.join(data_folder, 'sessionData.mat')
trial_data_path = os.path.join(data_folder, 'trialData.mat')
lfp_data_path = os.path.join(data_folder, 'lfpData.mat')
spike_data_path = os.path.join(data_folder, 'spikeData.mat')

session_data = loadmat(session_data_path)['sessionData']
trial_data = loadmat(trial_data_path)['trialData']
lfp_data = loadmat(lfp_data_path)['lfpData']
spike_data = loadmat(spike_data_path)['spikeData']

trial_data_df = pd.DataFrame(columns=trial_data.keys())
for key in trial_data.keys():
    trial_data_df[key] = trial_data[key]



session_t_start = lfp_data['t_start'][0]*pq.us
session_t_stop = lfp_data['t_end'][0]*pq.us



# --- GET LFP DATA ------------------------------------------------------------
trialNum = 35

trial = trial_data_df[trial_data_df['trialNum'] == trialNum]

trial_t_start = trial['trialStart'].iloc[0]
trial_t_stop  = trial['trialEnd'].iloc[0]


sampling_rate = lfp_data['fs'][0] * pq.Hz
assert lfp_data['signal_units'][0] == 'V'
lfp = neo.AnalogSignal(lfp_data['signal'].T, units='V',
                 t_start=session_t_start,
                 t_stop=session_t_stop,
                 sampling_rate=sampling_rate)
lfp_trial = lfp.time_slice(trial_t_start*pq.us, trial_t_stop*pq.us)
lfp_channel = lfp_trial.__array__()[:, 65]


sampling_rate = lfp_data['fs'][0] *pq.Hz
delta_t = (1 / sampling_rate) * 1e6 * pq.us
lfp_times = session_t_start + np.arange(lfp_data['signal'].shape[1]) / sampling_rate

np.testing.assert_array_equal(lfp.times.__array__(), lfp_times)
print(lfp_times[-1])
print(lfp.times[-1])

# --- SPECTROGRAM -------------------------------------------------------------
M = 50
freqs, sp_times, Sx = signal.spectrogram(lfp_channel,
                                      fs=sampling_rate, window='hanning',
                                      nperseg=M, noverlap=M-1,
                                      detrend=False, mode='psd')

f, ax = plt.subplots(figsize=(4.8, 2.4))
ax.pcolormesh(sp_times, freqs, 10 * np.log10(Sx), cmap='viridis')
ax.set_ylabel('Frequency [Hz]')
ax.set_xlabel('Time [s]');
#ax.set_ylim([0, fmax])

# -----------------------------------------------------------------------------

lfp_channel_index = 2
sampling_rate = 500.0 #Hz
transition_width = 1 #Hz
bandpass_attenuation = 60 #dB

low = 4
mid = 28
high = 40
spacing_low = 4
spacing_high = 4

times = lfp.times
#times = time_bin_times.rescale(pq.s)
signal = lfp.__array__()[:, 1]

delta_t = (1 / sampling_rate) * 1e6 * pq.us

np.testing.assert_array_equal(times.__array__(), mytimes)

"""
session = Session(data)

freq_bands = session.make_frequency_bands(low, mid, high, spacing_low, spacing_high)

session.set_filter_parameters(freq_bands, transition_width, 
                              bandpass_attenuation)

session.bandpass_filter(channel_index)

freq_bands = session.freq_bands
lfp = session.filtered_lfp[channel_index]
phase = session.lfp_phase[channel_index][freq_band, :]
energy = session.lfp_energy[channel_index][freq_band, :]


session.bin_spikes(binsize_in_ms=10, interpolate_lfp=True, drop_old=True)

session.filter_trials()


"""



filtered_signal = np.zeros([len(freq_bands), signal.shape[0]])
phase = np.zeros([len(freq_bands), signal.shape[0]])
energy = np.zeros([len(freq_bands), signal.shape[0]])

for j, (lowcut, highcut) in enumerate(freq_bands):
    print(lowcut, highcut)

    y = bandpass_filter(signal, lowcut, highcut, sampling_rate,
                        bandpass_attenuation, transition_width)
    filtered_signal[j, :] = y

    analytic_signal = get_analytic_signal(y)

    p = get_phase(analytic_signal)
    phase[j, :] = p

    e = get_energy(analytic_signal)
    energy[j, :] = e




# --- BIN SPIKES --------------------------------------------------------------

#binsize_in_us = 200

time_stamps = spike_data['ts']

binsize_in_ms = 10
binsize_in_us = binsize_in_ms * 1000
# generate spike trains
spiketrains = []
for k in range(time_stamps.shape[0]):
    spike_times = time_stamps[k]
    train = neo.SpikeTrain(times=spike_times*pq.us, t_start=session_t_start,
                           t_stop=session_t_stop)
    spiketrains.append(train)


bs = elephant.conversion.BinnedSpikeTrain(spiketrains,
                                          binsize=binsize_in_us * pq.us,
                                          t_start=session_t_start,
                                          t_stop=session_t_stop)
print(np.sum([t.times.__len__() for t in spiketrains]))
print(bs.to_array().sum())

bin_centers = bs.bin_centers.rescale(pq.us)



# --- INTERPOLATE PHASE -------------------------------------------------------

p = phase[0, :]
e = energy[0, :]

def interpolate(signal, times, new_times):
    return np.interp(new_times, times, signal)


interp_phase = interpolate(p, times, bin_centers)
interp_energy = interpolate(e, times, bin_centers)

# BIN GENERIC
n_phase_bins = 4
n_energy_bins = 4
# for the phase, we subdivide the into equally sized bins
binned_phase = pd.cut(interp_phase, bins=n_phase_bins, include_lowest=True, right=False).codes
# for energy, we go to equipopulated bins following Kayser et al.
binned_energy = pd.qcut(interp_energy, q=n_energy_bins).codes



# --- SELECT TRIALS -----------------------------------------------------------

trial_type = 'Y'

auditory_change = 2
visual_change = None
only_correct = False

selected_trials = []
if trial_type is not None:
    sel_trials = trial_data_df.loc[np.isin(trial_data_df['trialType'],
                                             trial_type), 'trialNum'].tolist()
    selected_trials.append(sel_trials)

if only_correct:
    sel_trials = trial_data_df.loc[trial_data_df['correctResponse'] == 1,
                                     'trialNum'].tolist()
    selected_trials.append(sel_trials)

if visual_change is not None:
    sel_trials = trial_data_df.loc[trial_data_df['visualOriChangeNorm'] == visual_change,
                                     'trialNum'].tolist()
    selected_trials.append(sel_trials)


if auditory_change is not None:
    sel_trials = trial_data_df.loc[trial_data_df['audioFreqChangeNorm'] == auditory_change,
                                     'trialNum'].tolist()
    selected_trials.append(sel_trials)


if len(selected_trials) > 0:
    selected_final = list(set.intersection(*map(set, selected_trials)))
else:
    selected_final = trial_data_df['trialNum'].tolist()

selected_trial_data = trial_data_df[np.isin(trial_data_df['trialNum'], selected_final)]

# --- CHOP UP BY TRIAL --------------------------------------------------------




time_before_in_s = 1
time_after_in_s = 1

time_before_in_us = time_before_in_s * 1e6
time_after_in_us  = time_after_in_s * 1e6


stim_change_t = selected_trial_data['stimChange'].tolist()[:-1]

aligned_trial_times = [[s - time_before_in_us, s + time_after_in_us] for
                       s in stim_change_t]

bin_centers_arr = bin_centers.__array__()

phase_by_trial = []
for trial_t_start, trial_t_stop in aligned_trial_times[:-1]:

    trial_time = ((trial_t_stop - trial_t_start)*pq.us).rescale(pq.s)
    ind = np.logical_and(bin_centers_arr >= trial_t_start,
                         bin_centers_arr < trial_t_stop)

    phase_by_trial.append(binned_phase[ind])
    print('Trial duration {:02.2f}, samples {:3}'.format(trial_time, np.sum(ind)))


phase_by_trial = np.vstack(phase_by_trial)


extent = (-time_before_in_s, time_after_in_s,  phase_by_trial.shape[0]-0.5, -0.5)
f, ax = plt.subplots(1, 1)
ax.imshow(phase_by_trial, aspect='auto', extent=extent, origin='upper')
ax.set_ylabel('Trials')
ax.set_xlabel('Times [s]')
sns.despine(offset=8)
plt.tight_layout()



end=1000
f, ax = plt.subplots(1, 1)
ax.scatter(times[0:end], p[0:end], label='phase')
ax.scatter(times[0:end], binned_phase[0:end], label='binned_phase')
ax.scatter(times[0:end], binned_energy[0:end], label='binned_energy')
ax.legend()

f, ax = plt.subplots(1, 1)
ax.plot(binned_energy)


end=100
f, ax = plt.subplots()
ax.plot(times[0:end], x[0:end], label='Noisy signal')
ax.plot(times[0:end], y[0:end], label='Filtered signal ({}-{} Hz, transition'
                    ' {} Hz)'.format(lowcut, highcut, transition_width))
ax.plot(times[0:end], instantaneous_phase[0:end] / 20000, alpha=0.2, label='Instantaneous phase')

ax.set_xlabel('time (seconds)')
#ax.hlines([-a, a], 0, T, linestyles='--')
ax.grid(True)
ax.axis('tight')
ax.legend(loc='upper left')
ax.plot(times, y + 1)
