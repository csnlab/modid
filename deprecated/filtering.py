from scipy.signal import firwin, remez, kaiser_atten, kaiser_beta

# Several flavors of bandpass FIR filters.

def bandpass_firwin(ntaps, lowcut, highcut, fs, window='hamming'):
    nyq = 0.5 * fs
    taps = firwin(ntaps, [lowcut, highcut], nyq=nyq, pass_zero=False,
                  window=window, scale=False)
    return taps

def bandpass_kaiser(ntaps, lowcut, highcut, fs, width):
    nyq = 0.5 * fs
    atten = kaiser_atten(ntaps, width / nyq)
    beta = kaiser_beta(atten)
    taps = firwin(ntaps, [lowcut, highcut], nyq=nyq, pass_zero=False,
                  window=('kaiser', beta), scale=False)
    return taps


def bandpass_kaiser2(ntaps, lowcut, highcut, fs, attenuation):
    nyq = 0.5 * fs
    beta = kaiser_beta(attenuation)
    taps = firwin(ntaps, [lowcut, highcut], nyq=nyq, pass_zero=False,
                  window=('kaiser', beta), scale=False)
    return taps

def bandpass_remez(ntaps, lowcut, highcut, fs, width):
    delta = 0.5 * width
    edges = [0, lowcut - delta, lowcut + delta,
             highcut - delta, highcut + delta, 0.5*fs]
    taps = remez(ntaps, edges, [0, 1, 0], Hz=fs)
    return taps


import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import freqz

# Sample rate and desired cutoff frequencies (in Hz).
fs = 500.0
lowcut = 4
highcut = 8.0

ntaps = 1024
taps_hamming = bandpass_firwin(ntaps, lowcut, highcut, fs=fs)
taps_kaiser  = bandpass_kaiser2(ntaps, lowcut, highcut, fs=fs, attenuation=60)
taps_kaiser16 = bandpass_kaiser(ntaps, lowcut, highcut, fs=fs, width=1.6)
taps_kaiser10 = bandpass_kaiser(ntaps, lowcut, highcut, fs=fs, width=1.0)
remez_width = 1.0
taps_remez = bandpass_remez(ntaps, lowcut, highcut, fs=fs,
                            width=remez_width)

# Plot the frequency responses of the filters.
plt.figure(1, figsize=(12, 9))
plt.clf()

# First plot the desired ideal response as a green(ish) rectangle.
rect = plt.Rectangle((lowcut, 0), highcut - lowcut, 1.0,
                     facecolor="#60ff60", alpha=0.2)
plt.gca().add_patch(rect)

# Plot the frequency response of each filter.
w, h = freqz(taps_hamming, 1, worN=2000)
plt.plot((fs * 0.5 / np.pi) * w, abs(h), label="Hamming window")

w, h = freqz(taps_kaiser, 1, worN=2000)
plt.plot((fs * 0.5 / np.pi) * w, abs(h), label="Kaiser window")

w, h = freqz(taps_kaiser16, 1, worN=2000)
plt.plot((fs * 0.5 / np.pi) * w, abs(h), label="Kaiser window, width=1.6")

w, h = freqz(taps_kaiser10, 1, worN=2000)
plt.plot((fs * 0.5 / np.pi) * w, abs(h), label="Kaiser window, width=1.0")

w, h = freqz(taps_remez, 1, worN=2000)
plt.plot((fs * 0.5 / np.pi) * w, abs(h),
         label="Remez algorithm, width=%.1f" % remez_width)

plt.xlim(0, 8.0)
plt.ylim(0, 1.1)
plt.grid(True)
plt.legend()
plt.xlabel('Frequency (Hz)')
plt.ylabel('Gain')
plt.title('Frequency response of several FIR filters, %d taps' % ntaps)

plt.show()



# --- MAKE FILTER -------------------------------------------------------------
# TODO: why transition of 1 Hz doesn'times work
# TODO ripple of 0.001??
# transition width should by normalised to Nyquist!!! 0 to 1
sampling_rate = 500.0 #Hz
transition_width = 1 #Hz
bandpass_attenuation = 60 #dB
lowcut = 30.0
highcut = 32.0

nyquist_frequency = sampling_rate / 2
transition_width_normalized = transition_width / nyquist_frequency

M, beta = scipy.signal.kaiserord(bandpass_attenuation, transition_width_normalized)
# M is number of taps

#W = scipy.signal.kaiser(M, beta, sym=True)

b = firwin(M, [lowcut, highcut], nyq=nyquist_frequency, pass_zero=False,
              window=('kaiser', beta), scale=False)

frequencies, freq_response = scipy.signal.freqz(b, worN=2000)

f, ax = plt.subplots(1, 1)
#ax.plot((sampling_rate * 0.5 / np.pi) * frequencies, abs(freq_response), label="Kaiser window")

x = frequencies * sampling_rate / ( 2 * np.pi)
#x = frequencies * (np.pi) / sampling_rate
y = 20 * np.log10(np.abs(freq_response))
ax.plot(x, y)
#ax.set_xlim(0, 250)


times = lfp.times - lfp.times[0]
times = times.rescale(pq.s)
x = lfp.__array__()[:, 1]

y = scipy.signal.filtfilt(b, 1, x)

analytic_signal = scipy.signal.hilbert(y)
amplitude_envelope = np.abs(analytic_signal)
instantaneous_phase = np.angle(analytic_signal)

end=100