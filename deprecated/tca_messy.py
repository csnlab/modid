import numpy as np
import pandas as pd
from constants import *
from loadmat import *
from session import Session
import scipy
from sklearn.model_selection import StratifiedKFold
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score, recall_score
import pickle
import quantities as pq
from utils import shuffle_array_rows_within_labels
from sklearn.ensemble import ExtraTreesClassifier
import distutils
from unit_scores import SingleUnitDiscriminationScores
from sklearn.preprocessing import StandardScaler, MinMaxScaler
import tensortools as tt
from plotting_style import *
import matplotlib.pyplot as plt
from session_info import select_sessions
import itertools
import scipy.stats
from sklearn import preprocessing
from sklearn import model_selection
from sklearn.metrics import roc_auc_score

settings_name = 'May22'

# if animal_id is specified, run for all sessions of that animal
# if area_spikes is not specified run for all available areas
animal_id = None
session_id = None
area_spikes = None
#area_spikes = 'all'  # if 'all' do one TCA for all units, otherwise area individually
min_units_per_area = 20

# animal_id = '2003'
# session_id = '2018-02-07_14-28-57'
# area_spikes = 'V1'

# TCA PARAMETERS
tca_rank = 12
fit_ensemble = False


# TIME PARAMETERS
align_event = 'stimChange'
sliding_window = False
time_before_stim_in_s = 0.4
time_after_stim_in_s = 0.8
binsize_in_ms = 1
slide_by_in_ms = -10

# SMOOTHING PARAMETERS
apply_smoothing = True
sampling_period = 10
kernel_width = 30
kernel_size = kernel_width * 8
kernel = scipy.signal.gaussian(kernel_size, kernel_width, sym=True)
kernel = 1000 * kernel / kernel.sum()

preprocess = 'z_score'


# PLOTTING PARAMETERS
plot_format = 'png'
dpi = 400
save_plots = True
plot_folder = os.path.join(DATA_FOLDER, 'plots', 'TCA', settings_name, 'rank_{}'.format(tca_rank))


if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder, exist_ok=True)

# SELECT TRIALS

#modalities = ['X', 'X', 'Y', 'Y']
# stimuli = [(1, 2), (3, 4), (1, 2), (3, 4)]
# stimulus_changes = [(1, 2), (1, 2), (1, 2), (1, 2)]

responses = [0, 1]
modalities = ['X', 'Y']
stimuli = [(1, 2), (3, 4)]
stimulus_changes = [1, 2]


trial_types = list(itertools.product(responses, modalities, stimuli,
                                     stimulus_changes))



# --- UTILITIES ---------------------------------------------------------------

def convolve_chunk(chunk, kernel, p):
    kernel_size = kernel.shape[0]
    rate = np.apply_along_axis(lambda m: np.convolve(m, kernel, mode='same'),
                               axis=1, arr=chunk)
    rate = rate[:, int(kernel_size / 2):-int(kernel_size / 2)]
    rate = rate[:, ::p]
    return rate


# --- PREPARE DATA ------------------------------------------------------------

if session_id is None:
    sessions = select_sessions(min_units=min_units_per_area)

    if area_spikes == 'all':
        sessions = sessions.drop_duplicates(subset=['animal_id', 'session_id'])
    else:
        sessions = sessions.drop_duplicates(subset=['animal_id', 'session_id',
                                                    'area'])

    if animal_id is not None:
        sessions = sessions[sessions['animal_id'] == animal_id]

    if area_spikes is not None and area_spikes != 'all':
        sessions = sessions[sessions['area'] == area_spikes]

else:
    sessions = pd.DataFrame(columns=['animal_id', 'session_id', 'area'],
                            data=[(animal_id, session_id, area_spikes)])


import numpy as np
y = np.random.randint(0, 10, 100)

def is_drift_component(y, c=2):
    # m  = y.mean()
    # m1 = y[0:y.shape[0] // 2].mean()
    # m2 = y[y.shape[0] // 2:].mean()
    #
    # if m1 > c*m or m2 > c*m:
    #     drift = True
    # else:
    #     drift = False

    yz = np.abs((y-y.mean())/y.std()).max()

    return drift


for i, row in sessions.iterrows():

    animal_id = row['animal_id']
    session_id = row['session_id']
    if not area_spikes == 'all':
        area_spikes = row['area']

    session = Session(animal_id=animal_id, session_id=session_id)
    session.load_data(load_lfp=False)


    binned_spikes_all = {}
    n_trials_all = {}
    trial_nums = []

    for response, modality, stimulus, change in trial_types:

        if modality == 'X':
            trial_numbers = session.select_trials(response=response,
                                                  trial_type=modality,
                                                  visual_post_norm=stimulus,
                                                  visual_change=change)
        elif modality == 'Y':

            trial_numbers = session.select_trials(response=response,
                                                  trial_type=modality,
                                                  audio_post_norm=stimulus,
                                                  auditory_change=change)

        trial_nums.append(trial_numbers)
        n_trials_all[(response, modality, stimulus, change)] = len(trial_numbers)

        if len(trial_numbers) > 0:
            trial_times = session.get_aligned_times(trial_numbers,
                                                    time_before_in_s=time_before_stim_in_s,
                                                    time_after_in_s=time_after_stim_in_s,
                                                    event=align_event)


            binned_spikes, spike_bin_centers = session.bin_spikes_per_trial(binsize_in_ms, trial_times,
                                                                            sliding_window=sliding_window,
                                                                            slide_by_in_ms=slide_by_in_ms)


            selected_unit_ind = session.select_units(area=area_spikes)
            selected_unit_id = session.get_cell_id(selected_unit_ind, shortened_id=False)

            unit_area = session.get_cell_area_from_cell_ind(selected_unit_ind)

            binned_spikes = [s[selected_unit_ind, :] for s in binned_spikes]

            if apply_smoothing:
                binned_spikes = [convolve_chunk(arr, kernel, sampling_period) for
                                 arr in binned_spikes]

            binned_spikes_all[(response, modality, stimulus, change)] = binned_spikes



        n_time_bins_per_trial = spike_bin_centers[0].shape[0]

        time_bin_times = (spike_bin_centers[0] - spike_bin_centers[0][0]).rescale(pq.s) \
                         + (binsize_in_ms*pq.ms).rescale(pq.s)/2 - time_before_stim_in_s*pq.s
        time_bin_times = time_bin_times.__array__().round(5)
        if apply_smoothing:
            time_bin_times = time_bin_times[int(kernel_size / 2):-int(kernel_size / 2)]
            time_bin_times = time_bin_times[::sampling_period]


    # --- PREPARE TENSOR ----------------------------------------------------------

    n_neurons = binned_spikes_all[trial_types[0]][0].shape[0]
    n_all_trials = np.sum([n_trials_all[k] for k in trial_types])
    n_times = binned_spikes_all[trial_types[0]][0].shape[1]

    X = np.hstack([np.hstack(binned_spikes_all[t]) for t in binned_spikes_all.keys()])
    #[len(binned_spikes_all[t]) for t in trial_groups]

    if preprocess == 'min_max':
        mm = MinMaxScaler(feature_range=(0, 1))
        X = mm.fit_transform(X.T).T
    elif preprocess == 'z_score':
        ss = StandardScaler(with_mean=True, with_std=True)
        X = ss.fit_transform(X.T).T

    X = np.reshape(X, (n_neurons, n_times, n_all_trials), order='F')


    modality_list, stimulus_list = [], []
    stimulus_change_list, response_list = [], []

    for response, modality, stimulus, change in trial_types:
        for n in range(n_trials_all[(response, modality, stimulus, change)]):
            modality_list.append(modality)
            stimulus_list.append(stimulus)
            stimulus_change_list.append(change)
            response_list.append(response)



    # --- RUN TCA -----------------------------------------------------------------


    U = tt.ncp_bcd(X, rank=tca_rank, verbose=True)

    if fit_ensemble:
        E = tt.Ensemble(nonneg=True, fit_method='ncp_bcd')
        E.fit(X, ranks=[2, 4, 6, 8, 10, 12, 14, 16, 18], replicates=10)

        f, ax = plt.subplots(1, 2, figsize=[6, 3])
        tt.plot_objective(E, ax=ax[0])
        tt.plot_similarity(E, ax=ax[1])
        sns.despine()
        plt.tight_layout()


    unit_factors = U.factors[0]
    n_nonzero_units = []

    for k in range(unit_factors.shape[1]):
        f = unit_factors[:, k]
        n_nonzero_units.append((f > 0).sum())

    unit_factors = preprocessing.normalize(unit_factors, norm='l2', axis=0)
    unit_factor_overlap = np.zeros([unit_factors.shape[1], unit_factors.shape[1]])
    for j in range(unit_factors.shape[1]):
        for k in range(unit_factors.shape[1]):
            d = np.nansum(unit_factors[:, j]*unit_factors[:, k])
            unit_factor_overlap[j, k] = d

    fig, ax = plt.subplots(1, 1)
    sns.heatmap(unit_factor_overlap, ax=ax, cmap="Blues")
    ax.set_xlabel('Unit factors')
    ax.set_ylabel('Unit factors')
    ax.set_title('Unit factor overlap')
    plot_name = 'TCA_{}_{}_{}_rank_{}_factors_overlap.{}'.format(animal_id, session_id,
                                                 area_spikes, tca_rank, plot_format)
    fig.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)


    # --- compute discrimination score ----------------------------------------

    trial_factors = U.factors[2]
    stimulus_list_simple = [0 if i == (1, 2) else 1 for i in stimulus_list]
    targets = [response_list, modality_list, stimulus_list_simple,
               stimulus_change_list]
    target_names = ['resp', 'mod', 'stim', 'stim_ch', '']


    df = pd.DataFrame(columns=['factor', 'target', 'score'])

    for name, target in zip(target_names, targets):

        for k in range(trial_factors.shape[1]):
            X = trial_factors[:, k][:, np.newaxis]
            y = target

            estimator = ''

            #score = model_selection.cross_val_score(estimator, X, y)
            score = roc_auc_score(y, X.flatten())

            df.loc[df.shape[0], :] = [k, name, score]



    # --- PLOT TCA ------------------------------------------------------------

    unit_area_color = [area_palette[k] for k in unit_area]
    mod_color = [modality_palette[k] for k in modality_list]
    stim_color = [stimulus_palette[k] for k in stimulus_list]
    stim_ch_color = [stimulus_change_palette[k] for k in stimulus_change_list]
    resp_color = [response_palette[k] for k in response_list]

    titles = ['Response', 'Modality', 'Stimulus identity', 'Stimulus change']
    colors = [resp_color, mod_color, stim_color, stim_ch_color]

    trial_inds = (np.hstack(trial_nums) - 1).astype(int)
    trial_inds = trial_inds.argsort()

    titles = ['Unit factors', 'Time factors', 'Trial factors\nResponse',
              'Trial factors\nModality', 'Trial factors\nStimulus identity',
              'Trial factors\nStimulus change', 'Trial factors\nReal order',
              'Distribution of unit\nweights', 'Discrimination']

    xlabels = ['Units', 'Time (s)', 'Trials', 'Trials', 'Trials', 'Trials',
               'Trials', 'Unit weights', '']


    trial_scatter_colors = [resp_color, mod_color, stim_color, stim_ch_color,
                            sns.xkcd_rgb['grey']]
    n_scatters = len(trial_scatter_colors)

    factor_plots= ['bar', 'line'] + ['scatter'] * n_scatters
    ndim = 2+n_scatters+2

    fig, axes = plt.subplots(U.factors.rank, ndim,
                             figsize=[ndim*2.5, U.factors.rank*1.5],
                             sharex='col')


    # main loop, plot each factor
    plot_obj = np.empty((U.factors.rank, ndim), dtype=object)
    for r in range(U.factors.rank):
        for i in range(len(factor_plots)):
            # start plots at 1 instead of zero

            if i < 2:
                f = U.factors[i]
            else:
                f = U.factors[2]

            x = np.arange(1, f.shape[0]+1)

            # determine type of plot
            if factor_plots[i] == 'bar':
                plot_obj[r, i] = axes[r, i].bar(x, f[:, r], color=unit_area_color)
                axes[r, i].set_xlim(0, f.shape[0]+1)
            elif factor_plots[i] == 'scatter':
                if i == (ndim - 3):
                    y = f[trial_inds, r]
                    drift = is_drift_component(y, 2)
                    if drift:
                        c='red'
                    else:
                        c=trial_scatter_colors[i-2]
                else:
                    y = f[:, r]
                    c=trial_scatter_colors[i-2]
                plot_obj[r, i] = axes[r, i].scatter(x, y, s=8, c=c)
                axes[r, i].set_xlim(0, f.shape[0])

            elif factor_plots[i] == 'line':
                plot_obj[r, i] = axes[r, i].plot(time_bin_times, f[:, r], '-')
                #axes[r, i].set_xlim(0, f.shape[0])
            else:
                raise ValueError('invalid plot type')

            # format axes
            axes[r, i].locator_params(nbins=4)
            axes[r, i].spines['top'].set_visible(False)
            axes[r, i].spines['right'].set_visible(False)
            axes[r, i].xaxis.set_tick_params(direction='out')
            axes[r, i].yaxis.set_tick_params(direction='out')
            axes[r, i].yaxis.set_ticks_position('left')
            axes[r, i].xaxis.set_ticks_position('bottom')

            # remove xticks on all but bottom row
            if r != U.factors.rank-1:
                plt.setp(axes[r, i].get_xticklabels(), visible=False)

    all_factors = U.factors[0].flatten()
    bins = np.linspace(all_factors.min(), all_factors.max(), 20)

    for r in range(U.factors.rank):
        sns.distplot(U.factors[0][:, r],ax=axes[r, len(factor_plots)],
                     bins=bins)
        axes[r, len(factor_plots)].set_xlim([-0.1, axes[r, len(factor_plots)].get_xlim()[1]])


    for i in range(ndim):
        axes[0, i].set_title(titles[i])
        axes[-1, i].set_xlabel(xlabels[i])


    for r in range(U.factors.rank):
        xx = df[df.factor == r]
        sns.barplot(data=xx, x='target', y='score', ax=axes[r, -1],
                    order=targets_palette.keys(),
                    palette=[targets_palette[k] for k in targets_palette.keys()])
        axes[r, -1].set_xticklabels([])
        axes[r, -1].set_ylabel('')
        axes[r, -1].set_xlabel('')
    axes[r, -1].set_xlabel('')
    #axes[-1, -1].set_xticklabels(axes[-1, -1].get_xticklabels(), rotation=40)


    # link y-axes within columns
    for i in range(ndim):
        yl = [a.get_ylim() for a in axes[:, i]]
        y0, y1 = min([y[0] for y in yl]), max([y[1] for y in yl])
        [a.set_ylim((y0, y1)) for a in axes[:, i]]

    # format y-ticks
    for r in range(U.factors.rank):
        for i in range(ndim):
            # only two labels
            ymin, ymax = np.round(axes[r, i].get_ylim(), 2)
            axes[r, i].set_ylim((ymin, ymax))

            # remove decimals from labels
            if ymin.is_integer():
                ymin = int(ymin)
            if ymax.is_integer():
                ymax = int(ymax)
            # update plot
            axes[r, i].set_yticks([ymin, ymax])

    #box = axes[0, -1].get_position()

    axis_for_labels = [2, 3, 4, 5, 8]
    palette_titles =  ['Response', 'Modality', 'Stimulus identity', 'Stimulus change',
                       'Target']
    legend_palettes = [response_palette, modality_palette,
                       stimulus_palette, stimulus_change_palette, targets_palette]

    legend_labels = [response_labels, modality_labels, stimulus_labels,
                     stimulus_change_labels, targets_labels]



    # fig, axes = plt.subplots(U.factors.rank, ndim,
    #                          figsize=[ndim*2.5, U.factors.rank*1.1],
    #                          sharex='col')

    for i, (axis_ind, pal, lab) in enumerate(zip(axis_for_labels,
                                                 legend_palettes, legend_labels)):
        print(axis_ind)
        markers, labels = get_markers_and_labels(pal, lab)

        axes[-1, axis_ind].legend(markers, labels, loc='upper center',
                           title=palette_titles[i],
                           bbox_to_anchor=(0.5, -1), frameon=False,
                           ncol=1)
        #leg = axes[-1, axis_ind].get_legend()
        #leg._legend_box.align = "left"

    sns.despine()
    plt.tight_layout(rect=[0,0.27,1,1])



    plot_name = 'TCA_{}_{}_{}_rank_{}.{}'.format(animal_id, session_id,
                                                 area_spikes, tca_rank, plot_format)
    fig.savefig(os.path.join(plot_folder, plot_name), dpi=dpi,
                bbox_inches="tight")
