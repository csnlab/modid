import numpy as np
import pandas as pd
from constants import *
from loadmat import *
from session import Session
import quantities as pq
from sklearn.preprocessing import StandardScaler, MinMaxScaler
import tensortools as tt
from plotting_style import *
import matplotlib.pyplot as plt
from session_info import select_sessions
import itertools
import scipy.stats
from utils import convolve_chunk
from sklearn.metrics import roc_auc_score

settings_name = 'May21'

# if animal_id is specified, run for all sessions of that animal
# if area_spikes is not specified run for all available areas
animal_id = None
session_id = None
area_spikes = None
# area_spikes = 'all'  # if 'all' do one TCA for all units, otherwise area individually
min_units_per_area = 10

# animal_id = '2003'
# session_id = '2018-02-07_14-28-57'
# area_spikes = 'V1'

# TCA PARAMETERS
tca_rank = 10
fit_ensemble = False

# TIME PARAMETERS
align_event = 'stimChange'
sliding_window = False
time_before_stim_in_s = 0.4
time_after_stim_in_s = 0.8
binsize_in_ms = 1
slide_by_in_ms = -10

# SMOOTHING PARAMETERS
apply_smoothing = True
sampling_period = 10
kernel_width = 30
kernel_size = kernel_width * 8
kernel = scipy.signal.gaussian(kernel_size, kernel_width, sym=True)
kernel = 1000 * kernel / kernel.sum()


preprocess = 'z_score'


# TODO collect pars and save as well

# --- SET UP PATHS ------------------------------------------------------------

results_folder = os.path.join(DATA_FOLDER, 'results', 'TCA_agg', settings_name)
results_file_name = 'TCA_settings_{}_rank_{}.pkl'.format(settings_name, tca_rank)


if not os.path.isdir(results_folder):
    os.makedirs(results_folder, exist_ok=True)


# --- IDENTIFY SESSIONS -------------------------------------------------------

if session_id is None:
    sessions = select_sessions(min_units=min_units_per_area)

    if area_spikes == 'all':
        sessions = sessions.drop_duplicates(subset=['animal_id', 'session_id'])
    else:
        sessions = sessions.drop_duplicates(subset=['animal_id', 'session_id',
                                                    'area'])

    if animal_id is not None:
        sessions = sessions[sessions['animal_id'] == animal_id]

    if area_spikes is not None and area_spikes != 'all':
        sessions = sessions[sessions['area'] == area_spikes]

else:
    sessions = pd.DataFrame(columns=['animal_id', 'session_id', 'area'],
                            data=[(animal_id, session_id, area_spikes)])


# --- RUN TCA -----------------------------------------------------------------

responses = [0, 1]
modalities = ['X', 'Y']
stimuli = [(1, 2), (3, 4)]
stimulus_changes = [1, 2]

trial_types = list(itertools.product(responses, modalities, stimuli,
                                     stimulus_changes))

df = pd.DataFrame(columns=['animal_id', 'session_id', 'area',
                           'factor', 'peak_time', 'kurtosis',
                           'perc_nonzero_units', 'nonzero_mean_factor',
                           'target', 'score'])

for i, row in sessions.iterrows():

    animal_id = row['animal_id']
    session_id = row['session_id']
    if not area_spikes == 'all':
        area_spikes = row['area']

    session = Session(animal_id=animal_id, session_id=session_id)
    session.load_data(load_lfp=False)

    binned_spikes_all = {}
    n_trials_all = {}
    trial_nums = []

    for response, modality, stimulus, change in trial_types:

        if modality == 'X':
            trial_numbers = session.select_trials(response=response,
                                                  trial_type=modality,
                                                  visual_post_norm=stimulus,
                                                  visual_change=change)
        elif modality == 'Y':

            trial_numbers = session.select_trials(response=response,
                                                  trial_type=modality,
                                                  audio_post_norm=stimulus,
                                                  auditory_change=change)

        trial_nums.append(trial_numbers)
        n_trials_all[(response, modality, stimulus, change)] = len(
            trial_numbers)

        if len(trial_numbers) > 0:
            trial_times = session.get_aligned_times(trial_numbers,
                                                    time_before_in_s=time_before_stim_in_s,
                                                    time_after_in_s=time_after_stim_in_s,
                                                    event=align_event)

            binned_spikes, spike_bin_centers = session.bin_spikes_per_trial(
                binsize_in_ms, trial_times,
                sliding_window=sliding_window,
                slide_by_in_ms=slide_by_in_ms)

            selected_unit_ind = session.select_units(area=area_spikes)
            selected_unit_id = session.get_cell_id(selected_unit_ind,
                                                   shortened_id=False)

            unit_area = session.get_cell_area_from_cell_ind(selected_unit_ind)

            binned_spikes = [s[selected_unit_ind, :] for s in binned_spikes]

            if apply_smoothing:
                binned_spikes = [convolve_chunk(arr, kernel, sampling_period)
                                 for
                                 arr in binned_spikes]

            binned_spikes_all[
                (response, modality, stimulus, change)] = binned_spikes

        n_time_bins_per_trial = spike_bin_centers[0].shape[0]

        time_bin_times = (spike_bin_centers[0] - spike_bin_centers[0][0]).rescale(pq.s) \
                         + (binsize_in_ms * pq.ms).rescale(pq.s) / 2 - time_before_stim_in_s * pq.s
        time_bin_times = time_bin_times.__array__().round(5)
        if apply_smoothing:
            time_bin_times = time_bin_times[int(kernel_size / 2):-int(kernel_size / 2)]
            time_bin_times = time_bin_times[::sampling_period]

    # --- PREPARE TENSOR ----------------------------------------------------------

    n_neurons = binned_spikes_all[trial_types[0]][0].shape[0]
    n_all_trials = np.sum([n_trials_all[k] for k in trial_types])
    n_times = binned_spikes_all[trial_types[0]][0].shape[1]

    X = np.hstack([np.hstack(binned_spikes_all[t]) for t in binned_spikes_all.keys()])
    # [len(binned_spikes_all[t]) for t in trial_groups]

    if preprocess == 'min_max':
        mm = MinMaxScaler(feature_range=(0, 1))
        X = mm.fit_transform(X.T).T
    elif preprocess == 'z_score':
        ss = StandardScaler(with_mean=True, with_std=True)
        X = ss.fit_transform(X.T).T

    X = np.reshape(X, (n_neurons, n_times, n_all_trials), order='F')

    modality_list, stimulus_list = [], []
    stimulus_change_list, response_list = [], []

    for response, modality, stimulus, change in trial_types:
        for n in range(n_trials_all[(response, modality, stimulus, change)]):
            modality_list.append(modality)
            stimulus_list.append(stimulus)
            stimulus_change_list.append(change)
            response_list.append(response)

    # --- RUN TCA -----------------------------------------------------------------

    U = tt.ncp_bcd(X, rank=tca_rank, verbose=True)

    if fit_ensemble:
        E = tt.Ensemble(nonneg=True, fit_method='ncp_bcd')
        E.fit(X, ranks=[2, 4, 6, 8, 10, 12, 14, 16, 18], replicates=10)

        f, ax = plt.subplots(1, 2, figsize=[6, 3])
        tt.plot_objective(E, ax=ax[0])
        tt.plot_similarity(E, ax=ax[1])
        sns.despine()
        plt.tight_layout()

    # --- compute discrimination score ----------------------------------------
    unit_factors = U.factors[0]
    time_factors = U.factors[1]
    trial_factors = U.factors[2]
    stimulus_list_simple = [0 if i == (1, 2) else 1 for i in stimulus_list]
    targets = [response_list, modality_list, stimulus_list_simple,
               stimulus_change_list]
    target_names = ['resp', 'mod', 'stim', 'stim_ch']

    for k in range(tca_rank):

        peak_time = time_bin_times[time_factors[:, k].argmax()]
        X = trial_factors[:, k][:, np.newaxis]

        kurtosis = scipy.stats.kurtosis(unit_factors[:, k])
        nonzero  = 100*(unit_factors[:, k] > 0).sum() / unit_factors[:, k].shape[0]

        x = unit_factors[:, k]
        nonzero_mean = x[x>0].mean()

        for target_name, y in zip(target_names, targets):
            score = roc_auc_score(y, X.flatten())


            df.loc[df.shape[0], :] = [animal_id, session_id, area_spikes,
                                      k, peak_time, kurtosis, nonzero, nonzero_mean,
                                      target_name, score]



numeric_cols = ['score', 'peak_time', 'kurtosis', 'perc_nonzero_units']
for col in numeric_cols:
    df[col] = pd.to_numeric(df[col])

#df.to_pickle(os.path.join(results_folder, results_file_name))