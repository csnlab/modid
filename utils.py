import os
from constants import *
import numpy as np
import scipy
from scipy.signal import firwin, remez, kaiser_atten, kaiser_beta
import matplotlib.pyplot as plt
import elephant
import numpy as np
import scipy
from scipy import signal
import neo
import matplotlib.pyplot as plt
import quantities as pq
import seaborn as sns
import pandas as pd


def to_long_form(time, X):
    """
    Array of shape n_repetitions, n_time_points
    """
    df = pd.DataFrame(columns=['time', 'values'])
    df['time'] = np.repeat(time, X.shape[0])
    df['values'] = X.T.flatten()
    return df

def shuffle_array_rows_within_labels(x, labels, seed=None):

    assert x.shape[0] == labels.shape[0]

    if x.ndim == 1:
        x = x[:, np.newaxis]

    df = pd.DataFrame({'labels': labels, 'data': x.mean(axis=1)})
    group_means = df.groupby('labels').mean().sort_values(by='labels').values
    group_medians = df.groupby('labels').median().sort_values(by='labels').values
    group_counts = df.groupby('labels').count().sort_values(by='labels').values

    bins = np.unique(labels)

    shuf_inds = np.zeros(x.shape[0], dtype=int)
    for bin in bins:
        ind = np.where(labels == bin)[0]
        if seed is not None:
            shuf_ind = np.random.RandomState(seed=seed).permutation(ind)
        else:
            shuf_ind = np.random.permutation(ind)
        shuf_inds[ind] = shuf_ind

    shuf_inds = np.hstack(shuf_inds)

    x_shuf = x[shuf_inds, :]

    df = pd.DataFrame({'labels': labels, 'data': x_shuf.mean(axis=1)})
    group_means_shuf = df.groupby('labels').mean().sort_values(by='labels').values
    group_medians_shuf = df.groupby('labels').median().sort_values(by='labels').values
    group_counts_shuf = df.groupby('labels').count().sort_values(by='labels').values

    # check that we have maintained the group means, medians, and counts,
    # so that we know we have not moved data points across labels
    np.testing.assert_array_almost_equal(group_means, group_means_shuf, decimal=14)
    np.testing.assert_array_almost_equal(group_medians, group_medians_shuf, decimal=14)
    np.testing.assert_array_equal(group_counts, group_counts_shuf)

    if x_shuf.shape[1] == 1:
        x_shuf = x_shuf[:, 0]

    return x_shuf




def plot_filtered_lfp(times, signal, filtered_signal, instantaneous_phase,
                      lowcut, highcut, transition_width,
                      nss=0, nse=1000):
    f, ax = plt.subplots()
    ax.plot(times[nss:nse], signal[nss:nse], label='Noisy signal')
    ax.plot(times[nss:nse], filtered_signal[nss:nse],
            label='Filtered signal ({}-{} Hz, transition'
                        ' {} Hz)'.format(lowcut, highcut, transition_width))
    ax.plot(times[nss:nse], instantaneous_phase[nss:nse] / 20000, alpha=0.2,
            label='Instantaneous phase')
    ax.set_xlabel('time (seconds)')
    #ax.hlines([-a, a], 0, T, linestyles='--')
    ax.grid(True)
    ax.axis('tight')
    ax.legend(loc='upper left')
    ax.plot(times, y + 1)




def convolve_chunk(chunk, kernel, p):
    kernel_size = kernel.shape[0]
    rate = np.apply_along_axis(lambda m: np.convolve(m, kernel, mode='same'),
                               axis=1, arr=chunk)
    rate = rate[:, int(kernel_size / 2):-int(kernel_size / 2)]
    rate = rate[:, ::p]
    return rate


def is_bad_factor(U, rank, min_perc_nonzero_units=5,
                  min_perc_nonzero_trials=5, min_variation=2):
    unit_factors = U.factors[0][:, rank]
    time_factors = U.factors[1][:, rank]
    trial_factors = U.factors[2][:, rank]

    # number of nonzero units
    perc_nonzero_units = 100 * (unit_factors > 0).sum() / unit_factors.shape[0]

    perc_nonzero_trials = 100 * (trial_factors > 0).sum() / trial_factors.shape[0]

    max_variation = np.abs((time_factors - time_factors.mean())
                           / time_factors.std()).max()

    enough_units = perc_nonzero_units >= min_perc_nonzero_units
    enough_trials = perc_nonzero_trials >= min_perc_nonzero_trials
    enough_variation = max_variation >= min_variation

    if enough_units and enough_trials and enough_variation:
        is_bad = False
    else:
        is_bad = True
    return is_bad

def get_factor_quality_metrics(U, rank):
    unit_factors = U.factors[0][:, rank]
    time_factors = U.factors[1][:, rank]
    trial_factors = U.factors[2][:, rank]

    # number of nonzero units
    perc_nonzero_units = 100 * (unit_factors > 0).sum() / unit_factors.shape[0]
    perc_nonzero_trials = 100 * (trial_factors > 0).sum() / trial_factors.shape[0]

    max_variation = np.abs((time_factors - time_factors.mean())
                           / time_factors.std()).max()

    return perc_nonzero_units.round(2), perc_nonzero_trials.round(2), max_variation.round(2)


def make_trial_df(session, trial_nums):
    trial_df = pd.DataFrame(columns=['num', 'mod', 'stim', 'stim_ch',
                                     'rew', 'lick', 'side', 'nlicks'])

    for num in trial_nums:
        row = session.trial_data[session.trial_data['trialNum'] == num]
        mod = row['trialType'].values[0]
        lick = 0 if row['noResponse'].values[0] == 1 else 1

        try:
            lickside = row['lickSide'].values[0][0]
        except IndexError:
            lickside = 'n'

        nlicks = len(row['lickSide'].values[0])

        if mod == 'P':
            stim = 0
            stim_ch = 0
            reward = 0

        else:
            reward = row['correctResponse'].values[0]
            if mod == 'X':
                stim = row['visualOriPostChangeNorm'].values[0]
                stim_ch = row['visualOriChangeNorm'].values[0]
            elif mod == 'Y':
                stim = row['audioFreqPostChangeNorm'].values[0]
                stim_ch = row['audioFreqChangeNorm'].values[0]

            if np.isin(stim, (2, 3)):
                stim = 2
            else:
                stim = 3

        trial_df.loc[trial_df.shape[0]] = [num, mod, stim, stim_ch,
                                           reward, lick, lickside, nlicks]

    trial_df['nlicksb'] = pd.cut(trial_df['nlicks'],
                                 bins=[0, 10, 20, 30, 100], include_lowest=True).cat.codes
    return trial_df



if __name__ == '__main__':

    labels = np.array([1, 1, 2, 1, 1, 2, 2, 1, 2, 2, 1])
    x = np.array([1, 2, 300, 4, 5, 100, 200, 3, 400, 500, 6])
    shuffle_array_rows_within_labels(x, labels)

    labels = np.array([1, 1, 2, 1, 1, 2, 2, 1, 2, 2, 1])
    x = np.array([[1, 2, 300, 4, 5, 100, 200, 3, 400, 500, 6],
                  [1, 2, 300, 4, 5, 100, 200, 3, 400, 500, 6]]).T
    shuffle_array_rows_within_labels(x, labels)
