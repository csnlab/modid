import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from session_info import *
from constants import *
import itertools
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
import pickle

# --- PARAMETERS --------------------------------------------------------------

dataset_name = 'dec2_poststim_nods'

area = 'V1'

min_visual_performance = 25

min_depth = 250
max_depth = 1150
n_depth_bins = 4
min_channel_per_range = 3

downsample = False
downsample_factor = 2

trial_subset_names = ['visual_detection', 'auditory_detection',
                      'no_visual_detection', 'no_auditory_detection']
trial_modalities = ['X', 'Y', 'X', 'Y']
trial_responses = [1, 1, 0, 0]
stimulus_changes = [2, 2, 2, 2]

# trial_subset_names = ['visual_detection', 'visual_no_detection']
# trial_modalities = ['X', 'X']
# trial_responses = [1, 0]
# visual_change = [2, 2]

time_before_stim_in_s = 1
time_after_stim_in_s = 0
allowed_experiments = ['visual_prestim', 'auditory_prestim']
#allowed_experiments = ['poststim_3labels']

train_size = 0.8
test_val_split = 0.5


# --- SET UP DEPTH VARIABLES --------------------------------------------------
trial_groups = list(zip(trial_subset_names, trial_modalities, trial_responses, stimulus_changes))
depth_edges = np.linspace(min_depth, max_depth, n_depth_bins+1)
depth_ranges = [(depth_edges[i], depth_edges[i+1]) for i in range(depth_edges.shape[0]-1)]
depth_range_names = ['depth_range_{}'.format(i) for i in range(1, len(depth_ranges)+1)]
#depth_range_names = ['depth_range_{}_{}'.format(int(x), int(y)) for x, y in depth_ranges]

# --- MAKE PATHS --------------------------------------------------------------

dataset_folder = os.path.join(DATA_FOLDER, 'deeplfp_datasets', dataset_name)
files_folder = os.path.join(dataset_folder, 'files')

os.makedirs(dataset_folder, exist_ok=True)
os.makedirs(files_folder, exist_ok=True)


# --- SELECT SESSIONS ---------------------------------------------------------

sessions = make_trial_overview()
sessions['perc_corr'] = pd.to_numeric(sessions['perc_corr'])
visual_perf = sessions[sessions['trial_type'] == 'X'].groupby(['animal_id',
                       'session_id', 'trial_type']).mean().reset_index()
sessions = visual_perf[visual_perf['perc_corr'] >= min_visual_performance]


animal_ids = []
session_ids = []

# Select only sessions for which there at least min_channel_per_range
# channels in every depth range
for animal_id, session_id in zip(sessions['animal_id'], sessions['session_id']):
        session = Session(animal_id=animal_id, session_id=session_id)
        session.load_data(load_spikes=False, load_lfp=False,
                          load_lfp_lazy=True)
        channel_ids = session.select_channels(area=area)

        if len(channel_ids) > 0:
            channel_depths = session.get_channel_depth(channel_ids)
            n_channels_per_range = []
            for depth_range in depth_ranges:
                mask = np.logical_and(channel_depths>=depth_range[0],
                                      channel_depths<depth_range[1])
                n_channels_per_range.append(mask.sum())
            if np.all(np.array(n_channels_per_range) >= min_channel_per_range):
                animal_ids.append(animal_id)
                session_ids.append(session_id)

# --- PLOT DEPTHS -------------------------------------------------------------

channel_ys = []
channel_la = []

for animal_id, session_id in zip(animal_ids, session_ids):

    session = Session(animal_id=animal_id, session_id=session_id)
    session.load_data(load_spikes=False, load_lfp=False,
                      load_lfp_lazy=True)
    channel_ys.append(session.lfp_data['ChannelY'])
    channel_la.append(session.lfp_data['Layer'])


f, ax = plt.subplots(1, 1, figsize=[5, 4])

for i, (y, layer) in enumerate(zip(channel_ys, channel_la)):

    y_sg = y[layer == 'SG']
    y_g  = y[layer == 'G']
    y_ig = y[layer == 'IG']

    x_sg = np.repeat(i, len(y_sg)).astype(float)
    x_sg[::2] = i + 0.1
    x_ig = np.repeat(i, len(y_ig)).astype(float)
    x_ig[::2] = i + 0.1
    x_g = np.repeat(i, len(y_g)).astype(float)
    x_g[::2] = i + 0.1

    ax.scatter(x_ig, y_ig, c=sns.xkcd_rgb['cerulean'], label='IG', s=10)
    ax.scatter(x_sg, y_sg, c=sns.xkcd_rgb['bluegreen'], label='SG', s=10)
    ax.scatter(x_g, y_g, c=sns.xkcd_rgb['greenish'], label='SG', s=10)

sns.despine(bottom=True)
for depth in depth_edges:
    ax.axhline(depth, ls='--', c='grey')
ax.set_ylim([0, 1500])
ax.set_ylabel('Channel depth')
ax.set_xticks([])
ax.set_xlabel('Probes')
plt.tight_layout()
plt.gca().invert_yaxis()


plot_path = os.path.join(dataset_folder, 'probe_depth.png')

f.savefig(plot_path, dpi=500)

# --- MAKE DATA ---------------------------------------------------------------

filesdf = pd.DataFrame(columns=['set', 'session_id', 'animal_id', 'channel_id',
                                'depth_range_name', 'depth_range',
                                'trial_number', 'trial_subset_name', 'trial_type',
                                'response', 'stim_change', 'file_name'])

for animal_id, session_id in zip(animal_ids, session_ids):

    session_id_compact = session_id.replace('-', '').replace('_', '')
    session = Session(animal_id=animal_id, session_id=session_id)
    session.load_data(load_spikes=False, load_lfp=True)

    if downsample:
        session.quick_downsample_lfp(factor=downsample_factor)

    for trial_subset_name, trial_type, response, stim_change in trial_groups:
        print(trial_subset_name, trial_type, response, stim_change)

        if trial_type == 'X':
            all_trial_numbers = session.select_trials(response=response,
                                                      trial_type=trial_type,
                                                      visual_change=stim_change)
        elif trial_type == 'Y':
            all_trial_numbers = session.select_trials(response=response,
                                                      trial_type=trial_type,
                                                      auditory_change=stim_change)

        elif trial_type == ['X', 'Y']:
            all_trial_numbers = session.select_trials(response=response,
                                                      trial_type=trial_type,
                                                      visual_change=[1, stim_change[0]],
                                                      auditory_change=[1, stim_change[1]])

        trial_numbers = np.array(all_trial_numbers)
        print(len(trial_numbers))

        # tests
        type_test = np.array([session.get_type_of_trial(n) for n in all_trial_numbers])
        assert np.all(np.isin(type_test, trial_type))

        resp_test = np.array([session.get_correct_response_of_trial(n) for n in all_trial_numbers])
        assert np.all(np.isin(resp_test, response))

        type_test = np.array([session.get_stimulus_change_of_trial(n) for n in all_trial_numbers])
        assert np.all(np.isin(type_test, stim_change))


        train_trials, test_val_trials = train_test_split(all_trial_numbers, train_size=train_size)

        test_trials, val_trials = train_test_split(test_val_trials, train_size=test_val_split)

        trial_split = [('train', train_trials),
                       ('test', test_trials),
                       ('val', val_trials)]

        assert set(train_trials).intersection(set(test_val_trials)).__len__() == 0
        assert set(test_trials).intersection(set(val_trials)).__len__() == 0

        # loop over train test val subsets
        for train_test_val, trial_numbers in trial_split:

            if len(trial_numbers) > 0:
                trial_times = session.get_aligned_times(trial_numbers,
                                                        time_before_in_s=time_before_stim_in_s,
                                                        time_after_in_s=time_after_stim_in_s)

                for depth_range_name, depth_range in zip(depth_range_names, depth_ranges):

                    channel_ids = session.select_channels(area=area, depth_range=depth_range)
                    assert np.all(session.get_channel_depth(channel_ids) >= depth_range[0])
                    assert np.all(session.get_channel_depth(channel_ids) < depth_range[1])

                    slices = session.slice_lfp_by_time(channel_ids, trial_times)

                    assert slices[0].shape[0] == len(channel_ids)

                    for trial_number, slice in zip(trial_numbers, slices):
                        for i, channel_id in enumerate(channel_ids):

                            file_name = 'trial_{}_{}_{}_ch_{}.npy'.format(animal_id, session_id_compact, trial_number,
                                                                channel_id)
                            file_location = os.path.join('files', file_name)
                            output_file = os.path.join(files_folder, file_name)
                            np.save(output_file, slice[i, :])

                            unique_trial_id = '{}_{}'.format(session_id_compact, trial_number)

                            row = [train_test_val, session_id, animal_id, channel_id,
                                   depth_range_name, depth_range, unique_trial_id, trial_subset_name,
                                   trial_type, response, stim_change, file_location]
                            filesdf.loc[filesdf.shape[0]] = row


# --- STATISTICS OF THE DATASET -----------------------------------------------
print('Percentage per split')

filesdf['trial_type'] = [i if isinstance(i, str)  else ''.join(i) for i in
                         filesdf['trial_type']]

for col in ['animal_id', 'trial_subset_name', 'trial_type', 'depth_range_name']:
    group1 = filesdf.groupby(['set', col]).agg({col:'count'})
    group2 = filesdf.groupby(['set']).agg({col:'count'})
    perc_col_per_set = group1.div(group2, level='set') * 100
    print(perc_col_per_set)


# make sure that the same channels appear across the splits
ids = filesdf.groupby('set')['channel_id'].unique()
np.testing.assert_array_equal(ids['test'], ids['train'])
np.testing.assert_array_equal(ids['train'], ids['val'])

# --- COLLECT AND SAVE PARAMETERS ---------------------------------------------

pars = {'dataset_name' : dataset_name,
        'area' : area,
        'min_visual_performance' : min_visual_performance,
        'min_depth' : min_depth,
        'max_depth' : max_depth,
        'n_depth_bins' : n_depth_bins,
        'min_channel_per_range' : min_channel_per_range,
        'downsample' : downsample,
        'downsample_factor' : downsample_factor,
        'trial_subset_names' : trial_subset_names,
        'trial_modalities' : trial_modalities,
        'trial_responses' : trial_responses,
        'stimulus_change' : stimulus_changes,
        'trial_groups' : trial_groups,
        'time_before_stim_in_s' : time_before_stim_in_s,
        'time_after_stim_in_s' : time_after_stim_in_s,
        'train_size' : train_size,
        'test_val_split' : test_val_split,
        'depth_edges' : depth_edges,
        'depth_ranges' : depth_ranges,
        'files_generated' : filesdf,
        'allowed_experiments' : allowed_experiments}


pickle.dump(pars, open(os.path.join(dataset_folder, 'pars.pkl'), 'wb'))
