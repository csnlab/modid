import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from session_info import *
from constants import *
import itertools
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split

dataset_name = 'test_nov1'

area = 'V1'
layers = ['SG', 'IG']

trial_subset_names = ['visual_detection', 'auditory_detection', 'no_detection']
trial_modalites = ['X', 'Y', ['X', 'Y']]
trial_responses = [1, 1, 0]
trial_groups = list(zip(trial_subset_names, trial_modalites, trial_responses))

time_before_stim_in_s = 0.2
time_after_stim_in_s = 0.4

train_size = 0.8
test_size = 0.5

animal_ids = ['2003', '2003', '2009']
session_ids = ['2018-02-08_15-13-56', '2018-02-09_14-30-19', '2018-08-22_14-05-50']

# --- MAKE PATHS --------------------------------------------------------------

dataset_folder = os.path.join(DATA_FOLDER, 'deeplfp_datasets', dataset_name)

for train_test_val in ['train', 'test', 'val']:
    for trial_subset_name in trial_subset_names:
        dir = os.path.join(dataset_folder, train_test_val, trial_subset_name)
        if not os.path.isdir(dir):
            os.makedirs(dir)


# --- MAKE DATA ---------------------------------------------------------------

for animal_id, session_id in zip(animal_ids, session_ids):

    session_id_compact = session_id.replace('-', '').replace('_', '')
    session = Session(animal_id=animal_id, session_id=session_id)
    session.load_data(load_spikes=True, load_lfp=True,
                      load_lfp_lazy=True)

    session.quick_downsample_lfp(factor=2)

    for trial_subset_name, trial_type, response in trial_groups:

        all_trial_numbers = session.select_trials(response=response,
                                              trial_type=trial_type)
        trial_numbers = np.array(all_trial_numbers)
        print(len(trial_numbers))

        # tests
        type_test = np.array([session.get_type_of_trial(n) for n in all_trial_numbers])
        assert np.all(np.isin(type_test, trial_type))

        resp_test = np.array([session.get_correct_response_of_trial(n) for n in all_trial_numbers])
        assert np.all(np.isin(resp_test, response))

        train_trials, test_val_trials = train_test_split(all_trial_numbers, train_size=train_size)

        test_trials, val_trials = train_test_split(test_val_trials, train_size=test_size)

        trial_split = [('train', train_trials),
                       ('test', test_trials),
                       ('val', val_trials)]

        for train_test_val, trial_numbers in trial_split:

            trial_times = session.get_aligned_times(trial_numbers,
                                                    time_before_in_s=time_before_stim_in_s,
                                                    time_after_in_s=time_after_stim_in_s)

            for layer in layers:

                channel_ids = session.select_channels(area=area, layer=layer)
                assert np.unique(session.get_channel_layer(channel_ids))[0] == layer
                slices = session.slice_lfp_by_time(channel_ids, trial_times)

                for trial_number, slice in zip(trial_numbers, slices):

                    output_dir = os.path.join(dataset_folder, train_test_val,
                                              trial_subset_name,
                                              'trial_{}{}{}'.format(animal_id,
                                              session_id_compact, trial_number), layer)

                    if not os.path.isdir(output_dir):
                        os.makedirs(output_dir)

                    for i, channel_id in enumerate(channel_ids):
                        output_file = os.path.join(output_dir, 'ch_{}'.format(channel_id))
                        np.save(output_file, slice[i, :])




