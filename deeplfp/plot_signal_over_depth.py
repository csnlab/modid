import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from session_info import *
from constants import *
import itertools
from sklearn.preprocessing import StandardScaler
import collections

session = Session(animal_id='2012', session_id='2018-08-14_14-30-15')
session.load_data(load_spikes=True, load_lfp=True,
                  load_lfp_lazy=False)

session.quick_downsample_lfp(factor=2)



channel_ids = session.select_channels(area='V1', layer='SG')
sorted_channel_ids = session.sort_lfp_channel_ids_by_depth(channel_ids=channel_ids)

session.get_channel_depth(sorted_channel_ids)
session.get_channel_x(sorted_channel_ids)


if not collections.Counter(channel_ids) == collections.Counter(sorted_channel_ids):
    raise ValueError

signal = session.get_lfp_signal_from_channel_id(sorted_channel_ids[::2])

signal = session.get_lfp_signal_from_channel_id(channel_ids)


f, ax = plt.subplots(20, 1, figsize=[8, 8], sharey=True, sharex=True)

for i in range(20):

    ax[i].plot(signal[i, 30000:40000])

sns.despine()
plt.tight_layout()


# TODO everything should follow the order of the input ids you give,
# TODO now it does not

"""
adapt get_lfp_channel_index_from_channel_id so it works for a list of ids
then in the functions like get_lfp_channel_layer etc always use that to
index the numpy arrays
"""