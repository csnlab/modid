import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from session_info import *
from constants import *

df = make_units_and_channels_overview()



dfx = df[(df['area'] == 'V1') & (df['layer'] != 'NA')]

df_sg = dfx[(dfx['layer'] == 'SG') & (dfx['n_channels'] > 2)]

df_ig = dfx[(dfx['layer'] == 'IG') & (dfx['n_channels'] > 2)]

df_sel = pd.merge(df_sg, df_ig, on=['animal_id', 'session_id', 'area'])


channel_ys = []
channel_la = []

for animal_id, session_id in zip(df_sel['animal_id'], df_sel['session_id']):

    session = Session(animal_id=animal_id, session_id=session_id)
    session.load_data(load_spikes=True, load_lfp=False,
                      load_lfp_lazy=True)
    channel_ys.append(session.lfp_data['ChannelY'])
    channel_la.append(session.lfp_data['Layer'])


f, ax = plt.subplots(1, 1, figsize=[5, 4])

for i, (y, layer) in enumerate(zip(channel_ys, channel_la)):

    y_sg = y[layer == 'SG']
    y_ig = y[layer == 'IG']

    x_sg = np.repeat(i, len(y_sg)).astype(float)
    x_sg[::2] = i + 0.1
    x_ig = np.repeat(i, len(y_ig)).astype(float)
    x_ig[::2] = i + 0.1
    ax.scatter(x_ig, y_ig, c='g', label='IG', s=10)
    ax.scatter(x_sg, y_sg, c='b', label='SG', s=10)
sns.despine(bottom=True)
ax.axhline(585-200, ls='--', c='grey')
ax.axhline(585-400, ls='--', c='grey')
ax.axhline(585, ls='--', c='grey')
#ax.axhline(585+100, ls='--', c='grey')
ax.axhline(585+100*2, ls='--', c='grey')
#ax.axhline(585+100*3, ls='--', c='grey')
ax.axhline(585+100*4, ls='--', c='grey')
ax.set_ylabel('Channel depth')
ax.set_xticks([])
ax.set_xlabel('Probes')
plt.tight_layout()



files_1 = ['a', 'b', 'c']
files_2 = ['1', '2', '3']
files_3 = ['x', 'y', 'z']
files = [files_1, files_2, files_3]

import itertools
list(itertools.product(*files))


