import numpy as np
import matplotlib.pyplot as plt

from affinewarp.datasets import piecewise_warped_data
binned, true_model = piecewise_warped_data(
    n_knots=1,
    n_trials=75,
    n_timepoints=150,
    n_neurons=100,
    clip_y_knots=False,
    template_scale=1.0,
    template_drop=.9,
    template_smoothness=2,
    noise_type="poisson",
    seed=1234)


from affinewarp import ShiftWarping, PiecewiseWarping

models = [
    ShiftWarping(smoothness_reg_scale=20.0),
    PiecewiseWarping(n_knots=0, warp_reg_scale=1e-6, smoothness_reg_scale=20.0),
    PiecewiseWarping(n_knots=1, warp_reg_scale=1e-6, smoothness_reg_scale=20.0),
    PiecewiseWarping(n_knots=2, warp_reg_scale=1e-6, smoothness_reg_scale=20.0),
]

for m in models:
    m.fit(binned, iterations=50, warp_iterations=200)


for m, label in zip(models, ('shift', 'linear', 'pwise-1', 'pwise-2')):
    plt.plot(m.loss_hist, label=label)
plt.legend()
plt.xlabel('iterations')
plt.ylabel('loss')

from affinewarp import SpikeData

fig, axes = plt.subplots(5, 6, sharex=True, sharey=True, figsize=(10, 5))

for n, axr in enumerate(axes) :
    raw_spikes = SpikeData(*np.where(binned), 0,
                           binned.shape[1]).select_neurons(n)

    true_align = true_model.transform(raw_spikes)
    est_align = [m.transform(raw_spikes) for m in models]

    axr[0].scatter(raw_spikes.spiketimes, raw_spikes.trials, lw=0, color='k',
                   s=1)
    axr[-1].scatter(true_align.spiketimes, true_align.trials, lw=0, color='k',
                    s=1)

    axr[1].scatter(est_align[0].spiketimes, est_align[0].trials, lw=0,
                   color='k', s=1)
    axr[2].scatter(est_align[1].spiketimes, est_align[1].trials, lw=0,
                   color='k', s=1)
    axr[3].scatter(est_align[2].spiketimes, est_align[2].trials, lw=0,
                   color='k', s=1)
    axr[4].scatter(est_align[3].spiketimes, est_align[3].trials, lw=0,
                   color='k', s=1)

axes[0, 0].set_title("raw data")
axes[0, -1].set_title("aligned by\nground truth")

axes[0, 1].set_title("shift-only")
axes[0, 2].set_title("linear")
axes[0, 3].set_title("piecewise-1")
axes[0, 4].set_title("piecewise-2")

for ax in axes.ravel() :
    ax.axis('off')
    ax.set_ylim([0, 100])
    ax.set_xlim([0, binned.shape[1]])

plt.tight_layout()
plt.subplots_adjust(wspace=.3, hspace=.3)