import numpy as np
import pandas as pd
from session import Session
import matplotlib.pyplot as plt
import quantities as pq
from affinewarp import ShiftWarping, PiecewiseWarping
from affinewarp import SpikeData
from affinewarp.visualization import rasters
import seaborn as sns

animal_id = '2012' #'2009
session_id = '2018-08-13_14-31-39'

binsize_in_ms = 10
time_before_in_s = 0.4
time_after_in_s = 0.8
extra_time = 0.2 #useful for when you shift

# --- LOAD SESSION ------------------------------------------------------------
session = Session(animal_id=animal_id, session_id=session_id)
session.load_data(load_lfp=False, load_spikes=True)

selected_unit_ind = session.select_units(area='PPC')

trial_numbers = session.select_trials(only_correct=True,
                                      trial_type=['X', 'Y'])

trials_inds, spiketimes, neuron_inds = session.prepare_data_for_affinewarp_spikedata(trial_numbers,
                                            selected_unit_ind, time_before_in_s=time_before_in_s,
                                            time_after_in_s=time_after_in_s)

spikedata = SpikeData(trials_inds, spiketimes, neuron_inds, -time_before_in_s,
                      time_after_in_s)

n_bins = 100
binned = spikedata.bin_spikes(n_bins)
assert binned.shape[0] == len(trial_numbers)
assert binned.shape[2] == len(selected_unit_ind)


# --- SET UP SPIKE DATA INSTANCE FOR LICKS ------------------------------------


# GET STIMULUS CHANGE TIMES
total_time = time_after_in_s + time_before_in_s
trial_inds_stim_change = np.arange(len(trial_numbers))
stim_ch_times = np.repeat(0, len(trial_numbers))
fractional_stim_ch_times = (stim_ch_times + time_before_in_s) / total_time

# GET LICK TIMES
sel_trial_ind = np.isin(session.trial_data['trialNum'], trial_numbers)
selected_trial_data = session.trial_data.loc[sel_trial_ind, :]
stim_ch_time_trial = selected_trial_data['stimChange'].values
lick_times = selected_trial_data['lickTime'].values

rel_lick_time = [lick_times[i] - stim_ch_time_trial[i]
                 for i in range(stim_ch_time_trial.shape[0])]
rel_lick_time_in_s = [l / 1e6 for l in rel_lick_time]

n_licks = []
for licks in rel_lick_time_in_s:
    if isinstance(licks, np.ndarray):
        n_licks.append(licks.shape[0])
    elif isinstance(licks, float):
        n_licks.append(1)
    else:
        raise ValueError
assert len(n_licks) == len(trial_numbers)
trial_inds_licks = [np.repeat(t, n) for t , n in zip(np.arange(len(trial_numbers)), n_licks)]

rel_lick_time_in_s = np.hstack(rel_lick_time_in_s)
trial_inds_licks = np.hstack(trial_inds_licks)

fractional_lick_times = (rel_lick_time_in_s + time_before_in_s) / total_time



# GET RESPONSE LICK
sel_trial_ind = np.isin(session.trial_data['trialNum'], trial_numbers)
selected_trial_data = session.trial_data.loc[sel_trial_ind, :]
stim_ch_time_trial = selected_trial_data['stimChange'].values
lick_times = selected_trial_data['lickTime'].values

rel_lick_time = [lick_times[i] - stim_ch_time_trial[i]
                 for i in range(stim_ch_time_trial.shape[0])]
rel_lick_time_in_s = [l / 1e6 for l in rel_lick_time]

resp_lick_time_in_s = []
for times in rel_lick_time_in_s:
    resp_times = times[times>0.1]
    if len(resp_times)>0:
        resp_lick_time_in_s.append(resp_times[0])
    else:
        resp_lick_time_in_s.append(np.nan)
resp_lick_time_in_s = np.array(resp_lick_time_in_s)

# --- FIT MODEL ---------------------------------------------------------------

#model = ShiftWarping(smoothness_reg_scale=20.0)

model = PiecewiseWarping(n_knots=2, warp_reg_scale=1e-6, smoothness_reg_scale=20.0)

model.fit(binned, iterations=50, warp_iterations=200)


fig, axes = rasters(spikedata, subplots=(5, 5), s=2);
for ax in axes.ravel()[:len(selected_unit_ind)]:
    ax.scatter(stim_ch_times, trial_inds_stim_change, c='r', s=1, alpha=.5)
    ax.scatter(resp_lick_time_in_s, trial_inds_stim_change,
               c=sns.xkcd_rgb['lighter green'], s=1, alpha=.5)



# warp spikes
warped_spikes = model.transform(spikedata)
# warp events
fractional_stim_ch_times = (stim_ch_times + time_before_in_s) / total_time
fractional_lick_times = (resp_lick_time_in_s + time_before_in_s) / total_time
warped_stim_ch = model.event_transform(trial_inds_stim_change, fractional_stim_ch_times)
warped_lick = model.event_transform(trial_inds_stim_change, fractional_lick_times)
warped_stim_ch = total_time * warped_stim_ch - time_before_in_s
warped_lick = total_time * warped_lick - time_before_in_s


fig, axes = rasters(warped_spikes, subplots=(5, 5), s=2);
for ax in axes.ravel()[:len(selected_unit_ind)]:
    ax.scatter(warped_stim_ch, trial_inds_stim_change, c='r', s=1, alpha=.5)
    ax.scatter(warped_lick, trial_inds_stim_change,
               c=sns.xkcd_rgb['lighter green'], s=1, alpha=.5)

# # --- PLOT OBJECTIVE ----------------------------------------------------------
#
# f, ax = plt.subplots(1, 1)
# ax.plot(model.loss_hist)
# ax.legend()
# ax.set_xlabel('iterations')
# ax.set_ylabel('loss')
#
# # --- PLOT RASTERS ------------------------------------------------------------
#
# neuron_inds = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
#
# fig, axes = plt.subplots(len(neuron_inds), 3, sharex=True, sharey=True, figsize=(10, 5))
#
# for i, n in enumerate(neuron_inds) :
#
#     raw_spikes = spikedata.select_neurons(n)
#     #true_align = true_model.transform(raw_spikes)
#     est_align = model.transform(raw_spikes)
#
#     warped_stim_ch = model.event_transform(trial_inds_events, fractional_stim_ch_times)
#     warped_stim_ch = total_time * warped_stim_ch - time_before_in_s
#
#
#     #warped_lick = model.event_transform(trial_inds_licks, fractional_lick_times)
#
#     warped_lick = np.asarray(fractional_lick_times) - model.fractional_shifts[trial_inds_licks]
#     warped_lick = total_time * warped_lick - time_before_in_s
#
#     axes[i, 0].scatter(raw_spikes.spiketimes, raw_spikes.trials, lw=0,
#                        color='k', s=4)
#     axes[i, 0].scatter(stim_ch_times, trial_inds_events, c='r', s=2,
#                        alpha=.5)
#     axes[i, 0].scatter(rel_lick_time_in_s, trial_inds_licks, c='g', s=4,
#                alpha=.5)
#
#
#
#     shift_sort = model.fractional_shifts.argsort()
#
#     sorted_raw_spikes = raw_spikes.reorder_trials(shift_sort)
#
#     axes[i, 1].scatter(sorted_raw_spikes.spiketimes, sorted_raw_spikes.trials, lw=0,
#                        color='k', s=4)
#     axes[i, 1].scatter(warped_stim_ch[shift_sort], trial_inds_events, c='r', s=2,
#                        alpha=.5)
#
#
#
#     axes[i, 2].scatter(est_align.spiketimes, est_align.trials, lw=0,
#                    color='k', s=4)
#
#     axes[i, 2].scatter(warped_stim_ch, trial_inds_events, c='r', s=2,
#                        alpha=.5)
#
#     axes[i, 2].scatter(warped_lick, trial_inds_licks, c='g', s=4,
#                alpha=.5)
#
# for ax in axes.flatten():
#     ax.set_xlim(-time_before_in_s, time_after_in_s)


# COMPARE DIFFERENT MODELS
# models = [
#     ShiftWarping(smoothness_reg_scale=20.0),
#     PiecewiseWarping(n_knots=0, warp_reg_scale=1e-6, smoothness_reg_scale=20.0),
#     PiecewiseWarping(n_knots=1, warp_reg_scale=1e-6, smoothness_reg_scale=20.0),
#     PiecewiseWarping(n_knots=2, warp_reg_scale=1e-6, smoothness_reg_scale=20.0),
# ]
#
# for m in models :
#     m.fit(binned, iterations=50, warp_iterations=200)
#
# for m, label in zip(models, ('shift', 'linear', 'pwise-1', 'pwise-2')) :
#     plt.plot(m.loss_hist, label=label)
# plt.legend()
# plt.xlabel('iterations')
# plt.ylabel('loss')
#
#
# trials_ind, spiketimes_ind, neurons_ind = np.where(binned)
# spiketimes = time_bin_times[spiketimes_ind]
# spikedata = SpikeData(trials_ind, spiketimes, neurons_ind, -time_before_in_s,
#                       time_after_in_s)
#
#
# trial_inds_events = np.arange(len(trial_numbers))
# lick_times = session.get_lick_time_of_trials(trial_numbers)
# #fractional_times_lick =
# # warp the onset of reward for each trial
#
# stim_ch_times = np.repeat(0, len(trial_numbers))
# fractional_stim_ch_times = (stim_ch_times + time_before_in_s) / (time_after_in_s + time_before_in_s)
#
# neuron_inds = [0, 1, 5, 7, 8, 10]
#
# fig, axes = plt.subplots(len(neuron_inds), 5, sharex=True, sharey=True, figsize=(10, 5))
#
# for i, n in enumerate(neuron_inds) :
#
#     raw_spikes = spikedata.select_neurons(n)
#
#     #true_align = true_model.transform(raw_spikes)
#     est_align = [m.transform(raw_spikes) for m in models]
#
#     axes[i, 0].scatter(raw_spikes.spiketimes, raw_spikes.trials, lw=0,
#                        color='k', s=4)
#
#     axes[i, 1].scatter(est_align[0].spiketimes, est_align[0].trials, lw=0,
#                    color='k', s=1)
#
#
#     axes[i, 2].scatter(est_align[1].spiketimes, est_align[1].trials, lw=0,
#                    color='k', s=1)
#     axes[i, 3].scatter(est_align[2].spiketimes, est_align[2].trials, lw=0,
#                    color='k', s=1)
#     axes[i, 4].scatter(est_align[3].spiketimes, est_align[3].trials, lw=0,
#                    color='k', s=1)
#
# axes[0, 0].set_title("raw data")
# axes[0, 1].set_title("shift-only")
# axes[0, 2].set_title("linear")
# axes[0, 3].set_title("piecewise-1")
# axes[0, 4].set_title("piecewise-2")
# plt.tight_layout()
# plt.subplots_adjust(wspace=.3, hspace=.3)