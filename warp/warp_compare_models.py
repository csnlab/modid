import os
import numpy as np
import pandas as pd
from session import Session
import matplotlib.pyplot as plt
import quantities as pq
from affinewarp import ShiftWarping, PiecewiseWarping
from affinewarp import SpikeData
from affinewarp.visualization import rasters
import seaborn as sns
from session_info import select_sessions
from constants import *
from plotting_style import *


settings_name = 'june27'
# animal_id = '2012'
# session_id = '2018-08-14_14-30-15'
# area_spikes = 'V1'
animal_id = None
session_id = None
area_spikes = None

# TIME PARS
time_before_in_s = 0.4
time_after_in_s = 0.8
extra_time = 0.2 #useful for when you shift

only_correct = True
trial_type = ['X', 'Y']

# WARPING PARAMETERS
warp_model = 'piecewise_linear' #shift_only, piecewise_linear
n_knots = 1 # only applied if warp_model == piecewise_linear
n_bins = 100

# PLOTTING PARAMETERS
plot_format = 'png'
dpi = 400
save_plots = False
plot_folder = os.path.join(DATA_FOLDER, 'plots', 'warp_compare_models', settings_name)

# --- WARP MODELS -------------------------------------------------------------

models = [ShiftWarping(smoothness_reg_scale=20.0, warp_reg_scale=0),
          PiecewiseWarping(n_knots=0, warp_reg_scale=1e-3,
                           smoothness_reg_scale=20.0),
          PiecewiseWarping(n_knots=1, warp_reg_scale=1e-3,
                           smoothness_reg_scale=20.0),
          PiecewiseWarping(n_knots=2, warp_reg_scale=1e-3,
                           smoothness_reg_scale=20.0)]




if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder, exist_ok=True)

if session_id is None:
    sessions = select_sessions(min_units=10)

    if area_spikes == 'all':
        sessions = sessions.drop_duplicates(subset=['animal_id', 'session_id'])
    else:
        sessions = sessions.drop_duplicates(subset=['animal_id', 'session_id',
                                                    'area'])

    if animal_id is not None:
        sessions = sessions[sessions['animal_id'] == animal_id]

    if area_spikes is not None and area_spikes != 'all':
        sessions = sessions[sessions['area'] == area_spikes]

else:
    sessions = pd.DataFrame(columns=['animal_id', 'session_id', 'area'],
                            data=[(animal_id, session_id, area_spikes)])


for i, row in sessions.iterrows():

    animal_id = row['animal_id']
    session_id = row['session_id']
    area_spikes = row['area']

    # --- LOAD SESSION ------------------------------------------------------------
    session = Session(animal_id=animal_id, session_id=session_id)
    session.load_data(load_lfp=False, load_spikes=True)

    selected_unit_ind = session.select_units(area=area_spikes)

    trial_numbers = session.select_trials(only_correct=only_correct,
                                          trial_type=trial_type)

    trials_inds, spiketimes, neuron_inds = session.prepare_data_for_affinewarp_spikedata(trial_numbers,
                                                selected_unit_ind, time_before_in_s=time_before_in_s+extra_time,
                                                time_after_in_s=time_after_in_s+extra_time)

    spikedata = SpikeData(trials_inds, spiketimes, neuron_inds, -time_before_in_s,
                          time_after_in_s)

    binned = spikedata.bin_spikes(n_bins)
    assert binned.shape[0] == len(trial_numbers)
    assert binned.shape[2] == len(selected_unit_ind)

    # --- GET TIME AND FRACTION TIME OF EVENTS (STIM. CHANGE, RESPONSE LICK) ------

    # GET STIMULUS CHANGE TIMES
    total_time = time_after_in_s + time_before_in_s
    trial_inds_events = np.arange(len(trial_numbers))
    stim_ch_times = np.repeat(0, len(trial_numbers))

    # GET RESPONSE LICK
    sel_trial_ind = np.isin(session.trial_data['trialNum'], trial_numbers)
    selected_trial_data = session.trial_data.loc[sel_trial_ind, :]
    stim_ch_time_trial = selected_trial_data['stimChange'].values
    lick_times = selected_trial_data['lickTime'].values

    rel_lick_time = [lick_times[i] - stim_ch_time_trial[i]
                     for i in range(stim_ch_time_trial.shape[0])]
    rel_lick_time_in_s = [l / 1e6 for l in rel_lick_time]

    resp_lick_time_in_s = []
    for times in rel_lick_time_in_s:
        # licks in the first 100ms are not considered a correct response
        resp_times = times[times>0.1]
        if len(resp_times)>0:
            resp_lick_time_in_s.append(resp_times[0])
        else:
            resp_lick_time_in_s.append(np.nan)
    resp_lick_time_in_s = np.array(resp_lick_time_in_s)

    fractional_stim_ch_times = (stim_ch_times + time_before_in_s) / total_time
    fractional_lick_times = (resp_lick_time_in_s + time_before_in_s) / total_time

    # --- FIT MODEL ---------------------------------------------------------------

    for m in models :
        m.fit(binned, iterations=50, warp_iterations=200)

    neuron_inds = [0, 1, 5, 7, 8, 10]

    sel_trial_ind = np.isin(session.trial_data['trialNum'], trial_numbers)
    trial_modality = session.trial_data.loc[sel_trial_ind, 'trialType']
    stim_ch_colors = [modality_palette[x] for x in trial_modality]

    fig, axes = plt.subplots(len(neuron_inds), len(models)+1, sharex=True, sharey=True, figsize=(10, 5))

    for i, n in enumerate(neuron_inds) :

        raw_spikes = spikedata.select_neurons(n)

        #true_align = true_model.transform(raw_spikes)
        est_align = [m.transform(raw_spikes) for m in models]

        axes[i, 0].scatter(raw_spikes.spiketimes, raw_spikes.trials, lw=0,
                           color='k', s=1)

        axes[i, 1].scatter(est_align[0].spiketimes, est_align[0].trials, lw=0,
                       color='k', s=1)


        axes[i, 2].scatter(est_align[1].spiketimes, est_align[1].trials, lw=0,
                       color='k', s=1)
        axes[i, 3].scatter(est_align[2].spiketimes, est_align[2].trials, lw=0,
                       color='k', s=1)
        axes[i, 4].scatter(est_align[3].spiketimes, est_align[3].trials, lw=0,
                       color='k', s=1)

    for kk, model in enumerate(models):
        warped_stim_ch = model.event_transform(trial_inds_events, fractional_stim_ch_times)
        warped_lick = model.event_transform(trial_inds_events, fractional_lick_times)
        warped_stim_ch = total_time * warped_stim_ch - time_before_in_s
        warped_lick = total_time * warped_lick - time_before_in_s

        for ax in axes[:, kk+1]:
            ax.scatter(warped_stim_ch, trial_inds_events, c=stim_ch_colors,
                       s=1, alpha=.5)
            ax.scatter(warped_lick, trial_inds_events,
                       c=sns.xkcd_rgb['light blue'], s=1, alpha=.5)


    axes[0, 0].set_title("raw data")
    axes[0, 1].set_title("shift-only")
    axes[0, 2].set_title("linear")
    axes[0, 3].set_title("piecewise-1")
    axes[0, 4].set_title("piecewise-2")
    plt.subplots_adjust(wspace=.3, hspace=.3)
    for ax in axes.flatten():
        ax.set_xlim(-time_before_in_s, time_after_in_s)
    plt.tight_layout()

    plot_name = 'warp_compare_models_{}_{}_{}.{}'.format(animal_id, session_id,
                                                  area_spikes, plot_format)
    fig.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)

