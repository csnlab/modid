import pickle
import os
import quantities as pq
from sklearn.preprocessing import LabelEncoder
from MLencoding import MLencoding
import itertools
import numpy as np
import scipy.ndimage
import matplotlib.pyplot as plt
import seaborn as sns

n = 10000

# --- BUMPS ---
x = np.zeros(n)
for i in range(100, n, 500):
    x[i:i + 50] = 1

y = scipy.ndimage.gaussian_filter1d(x, sigma=50)
x = x[:, np.newaxis]

# --- ANALOG WITH SHIFTED RESPONSE ---

x = np.random.normal(0, scale=1, size=n)
x = scipy.ndimage.gaussian_filter1d(x, sigma=10)

f, ax = plt.subplots(1, 1)
ax.plot(x)

y = np.roll(scipy.ndimage.gaussian_filter1d(x, sigma=50), shift=200)

f, ax = plt.subplots(1, 1)
ax.plot(x)
ax.plot(y)


# --- TRAIN TEST SPLIT --------------------------------------------------------
x = x[:, np.newaxis]
x_train = x[0:8000, :]
y_train = y[0:8000]
x_test  = x[8000:, :]
y_test  = y[8000:]

encoding_model = 'random_forest'
encoder_params = {'n_estimators' : 50}

encoder = MLencoding(tunemodel=encoding_model, cov_history=True, window=1,
                     n_filters=5, max_time=20)

# encoder = MLencoding(tunemodel=encoding_model, cov_history=False, window=1,
#                      n_filters=5, max_time=500)
encoder.set_params(encoder_params)
encoder.fit(x_train, y_train)


y_pred = encoder.predict(x_test)

f, ax = plt.subplots(1, 2, figsize=[8, 4])
ax[0].plot(x_train, label='predictor')
ax[0].plot(y_train, label='variable')
ax[1].plot(x_test, label='predictor')
ax[1].plot(y_pred, label='predicted values')
ax[1].plot(y_test, label='true values')
ax[0].set_title('train')
ax[0].legend(loc='upper left')
ax[1].set_title('test')
ax[1].legend(loc='upper left')
sns.despine()
plt.tight_layout()