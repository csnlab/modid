import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from constants import *
from loadmat import *
from session import Session
from sklearn.model_selection import StratifiedKFold
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score, recall_score
from matplotlib.ticker import MaxNLocator
import pickle
from utils import shuffle_array_rows_within_labels

setting_name = 'basic'
animal_id = '2009'
session_id = '2018-08-22_14-05-50'

# SELECT CHANNELS
area_lfp = 'V1'
layer_lfp = None
ch_in_the_middle_of_the_probe = True

# FILTERING PARAMETERS
transition_width = 1  # Hz
bandpass_attenuation = 60  # dB
low_freq = 4
mid_freq = 28
high_freq = 28 + 7 * 8
spacing_low = 4
spacing_high = 8
n_phase_bins = 4
n_energy_bins = 4


output_name = 'filtered_lfp_session_{}_{}_{}.pkl'.format(animal_id, session_id,
                                                         setting_name)
output_dir = os.path.join(DATA_FOLDER, 'filtered_sessions', setting_name)
output_path = os.path.join(output_dir, output_name)

if not os.path.isdir(output_dir):
    os.makedirs(output_dir)
# -----------------------------------------------------------------------------

session = Session(animal_id=animal_id, session_id=session_id)
session.load_data()
session.quick_downsample_lfp(factor=2)

freq_bands = session.make_frequency_bands(low_freq, mid_freq, high_freq, spacing_low, spacing_high)

session.set_filter_parameters(freq_bands, transition_width, bandpass_attenuation)

lfp_channel_id = session.get_random_channel_id(area=area_lfp,
                                               layer=layer_lfp,
                                               in_the_middle=ch_in_the_middle_of_the_probe)

session.bandpass_filter(lfp_channel_id)



pickle.dump(session, open(output_path, 'wb'))



