import numpy as np
import pandas as pd
from constants import *
from loadmat import *
from session import Session
import quantities as pq
from sklearn.preprocessing import StandardScaler, MinMaxScaler
import tensortools as tt
from plotting_style import *
import matplotlib.pyplot as plt
from session_info import select_sessions
import itertools
import scipy.stats
from utils import convolve_chunk
from utils import make_trial_df
from sklearn.metrics import roc_auc_score
import pickle

settings_name = 'mmm_nov11'

# if animal_id is specified, run for all sessions of that animal
# if area_spikes is not specified run for all available areas
animal_id = None#'2012' #None
session_id = None#'2018-08-14_14-30-15' #None
#area_spikes = 'PPC'
#area_spikes = 'all'  # if 'all' do one TCA for all units, if None for areas individually
area_spikes = None

# animal_id = '2003'
# session_id = '2018-02-07_14-28-57'
# area_spikes = 'V1'

# TCA PARAMETERS
tca_rank = 15
fit_ensemble = False
include_probe_trials = True
ensemble_ranks = [2, 4, 6, 8, 10, 12, 14, 16]
ensemble_replicates = 20

min_units_per_area = np.max(ensemble_ranks)+4

# TIME PARAMETERS
align_event = 'stimChange'
sliding_window = False
time_before_stim_in_s = 0.4
time_after_stim_in_s = 0.8
binsize_in_ms = 1
slide_by_in_ms = -10

# SMOOTHING PARAMETERS
apply_smoothing = True
sampling_period = 10
kernel_width = 30
kernel_size = kernel_width * 8
kernel = scipy.signal.gaussian(kernel_size, kernel_width, sym=True)
kernel = 1000 * kernel / kernel.sum()

preprocess = 'z_score'

# --- SET UP PATHS ------------------------------------------------------------

results_folder = os.path.join(DATA_FOLDER, 'results', 'TCA', settings_name)
results_file_name = 'TCA_settings_{}_rank_{}.pkl'.format(settings_name, tca_rank)
pars_file_name = 'TCA_settings_{}_rank_{}_pars.pkl'.format(settings_name, tca_rank)


if not os.path.isdir(results_folder):
    os.makedirs(results_folder, exist_ok=True)


# --- IDENTIFY SESSIONS -------------------------------------------------------

if session_id is None:
    sessions = select_sessions(min_units=min_units_per_area)

    if area_spikes == 'all':
        sessions = sessions.drop_duplicates(subset=['animal_id', 'session_id'])
    else:
        sessions = sessions.drop_duplicates(subset=['animal_id', 'session_id',
                                                    'area'])

    if animal_id is not None:
        sessions = sessions[sessions['animal_id'] == animal_id]

    if area_spikes is not None and area_spikes != 'all':
        sessions = sessions[sessions['area'] == area_spikes]

else:
    sessions = pd.DataFrame(columns=['animal_id', 'session_id', 'area'],
                            data=[(animal_id, session_id, area_spikes)])


# --- RUN TCA -----------------------------------------------------------------

responses = [0, 1]
modalities = ['X', 'Y']
stimuli = [(1, 2), (3, 4)]
stimulus_changes = [2, 3]

trial_types = list(itertools.product(responses, modalities, stimuli,
                                     stimulus_changes))

if include_probe_trials:
    probe_trials = [(0, 'P', 0, 0), (1, 'P', 0, 0)]
    trial_types = probe_trials + trial_types

rs = {}



for i, row in sessions.iterrows():

    animal_id = row['animal_id']
    session_id = row['session_id']
    if not area_spikes == 'all':
        area_spikes = row['area']

    session = Session(animal_id=animal_id, session_id=session_id)
    session.load_data(load_lfp=False)

    selected_unit_ind = session.select_units(area=area_spikes)
    selected_unit_id = session.get_cell_id(selected_unit_ind,
                                           shortened_id=False)

    unit_area = session.get_cell_area(selected_unit_id)
    unit_depth = session.get_cell_depth(selected_unit_id)
    unit_layer = session.get_cell_layer(selected_unit_id)

    binned_spikes_all = {}
    n_trials_all = {}
    trial_nums = []

    for response, modality, stimulus, change in trial_types:

        if modality == 'X':
            trial_numbers = session.select_trials(response=response,
                                                  trial_type=modality,
                                                  visual_post_norm=stimulus,
                                                  visual_change=change)
        elif modality == 'Y':
            trial_numbers = session.select_trials(response=response,
                                                  trial_type=modality,
                                                  audio_post_norm=stimulus,
                                                  auditory_change=change)

        elif modality == 'P':
            trial_numbers = session.select_trials(response=response,
                                                  trial_type=modality,
                                                  audio_post_norm=stimulus,
                                                  auditory_change=change,
                                                  visual_post_norm=stimulus,
                                                  visual_change=change)

        trial_nums.append(trial_numbers)
        n_trials_all[(response, modality, stimulus, change)] = len(trial_numbers)

        if len(trial_numbers) > 0:
            trial_times = session.get_aligned_times(trial_numbers,
                                                    time_before_in_s=time_before_stim_in_s,
                                                    time_after_in_s=time_after_stim_in_s,
                                                    event=align_event)

            binned_spikes, spike_bin_centers = session.bin_spikes_per_trial(
                binsize_in_ms, trial_times,
                sliding_window=sliding_window,
                slide_by_in_ms=slide_by_in_ms)

            binned_spikes = [s[selected_unit_ind, :] for s in binned_spikes]

            if apply_smoothing:
                binned_spikes = [convolve_chunk(arr, kernel, sampling_period)
                                 for
                                 arr in binned_spikes]

            binned_spikes_all[
                (response, modality, stimulus, change)] = binned_spikes


            n_time_bins_per_trial = spike_bin_centers[0].shape[0]
            time_bin_times = (spike_bin_centers[0] - spike_bin_centers[0][0]).rescale(pq.s) \
                             + (binsize_in_ms * pq.ms).rescale(pq.s) / 2 - time_before_stim_in_s * pq.s
            time_bin_times = time_bin_times.__array__().round(5)
            if apply_smoothing:
                time_bin_times = time_bin_times[int(kernel_size / 2):-int(kernel_size / 2)]
                time_bin_times = time_bin_times[::sampling_period]

    # --- PREPARE TENSOR ----------------------------------------------------------

    n_neurons = binned_spikes_all[trial_types[2]][0].shape[0]
    n_all_trials = np.sum([n_trials_all[k] for k in trial_types])
    n_times = binned_spikes_all[trial_types[2]][0].shape[1]

    X = np.hstack([np.hstack(binned_spikes_all[t]) for t in binned_spikes_all.keys()])
    # [len(binned_spikes_all[t]) for t in trial_groups]
    if preprocess == 'min_max':
        mm = MinMaxScaler(feature_range=(0, 1))
        X = mm.fit_transform(X.T).T
    elif preprocess == 'z_score':
        ss = StandardScaler(with_mean=True, with_std=True)
        X = ss.fit_transform(X.T).T

    X = np.reshape(X, (n_neurons, n_times, n_all_trials), order='F')

    print(X.shape)

    trial_nums = np.hstack(trial_nums)
    trial_df = make_trial_df(session, trial_nums)

    # --- RUN TCA -----------------------------------------------------------------

    if fit_ensemble:
        E = tt.Ensemble(nonneg=True, fit_method='ncp_bcd')
        E.fit(X, ranks=ensemble_ranks, replicates=ensemble_replicates)
        # f, ax = plt.subplots(1, 2, figsize=[6, 3])
        # tt.plot_objective(E, ax=ax[0])
        # tt.plot_similarity(E, ax=ax[1])
        # sns.despine()
        # plt.tight_layout()
    else:
        E = None


    U = tt.ncp_bcd(X, rank=tca_rank, verbose=True)


    trial_inds = (np.hstack(trial_nums) - 1).astype(int)
    trial_inds = trial_inds.argsort()

    out = {'factors' : U,
           'ensemble' : E,
           'trial_df' : trial_df,
           'trial_inds' : trial_inds,
           'time_bin_times' : time_bin_times,
           'selected_unit_id' : selected_unit_id,
           'unit_depth' : unit_depth,
           'unit_layer' : unit_layer}


    rs[(animal_id, session_id, area_spikes)] = out


# --- GATHER PARS ---

pars = {'settings_name' : settings_name,
        'animal_id' : animal_id,
        'session_id' : session_id,
        'area_spikes' : area_spikes,
        'min_units_per_area' : min_units_per_area,
        'tca_rank' : tca_rank,
        'fit_ensemble' : fit_ensemble,
        'ensemble_ranks' : ensemble_ranks,
        'ensemble_replicates' : ensemble_replicates,
        'include_probe_trials' : include_probe_trials,
        'align_event' : align_event,
        'sliding_window' : sliding_window,
        'time_before_stim_in_s' : time_before_stim_in_s,
        'time_after_stim_in_s' : time_after_stim_in_s,
        'binsize_in_ms' : binsize_in_ms,
        'slide_by_in_ms' : slide_by_in_ms,
        'apply_smoothing' : apply_smoothing,
        'sampling_period' : sampling_period,
        'kernel_width' : kernel_width,
        'kernel_size' : kernel_size,
        'preprocess' : preprocess}



print('Saving parameters to {}'.format(os.path.join(results_folder, pars_file_name)))
pickle.dump(pars, open(os.path.join(results_folder, pars_file_name), 'wb'))
print('Saving TCA results to {}'.format(os.path.join(results_folder, results_file_name)))
pickle.dump(rs, open(os.path.join(results_folder, results_file_name), 'wb'))