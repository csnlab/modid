import numpy as np
from sklearn.metrics import make_scorer, balanced_accuracy_score, roc_auc_score
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import cross_val_score


def compute_factor_discrimination_score(X, y, score_name):
    """
    Computes how well the trial factors X of a given factor discriminate
    between trial features in y

    X is of shape n_samples by 1
    y is of shape n_samples

    """

    assert np.unique(y).shape[0] == 2

    if score_name == 'rf_cross_val':
        estimator = RandomForestClassifier(n_estimators=50)
        scorer = make_scorer(balanced_accuracy_score)
        score = cross_val_score(estimator, X, y, cv=3, scoring=scorer).mean()

    elif score_name == 'auc':
        score = roc_auc_score(y, X.flatten())
        if score < 0.5:
            score = 1 - score
    return score



def is_bad_factor(U, rank, min_perc_nonzero_units=5,
                  min_perc_nonzero_trials=5, min_variation=2):
    unit_factors = U.factors[0][:, rank]
    time_factors = U.factors[1][:, rank]
    trial_factors = U.factors[2][:, rank]

    # number of nonzero units
    perc_nonzero_units = 100 * (unit_factors > 0).sum() / unit_factors.shape[0]

    perc_nonzero_trials = 100 * (trial_factors > 0).sum() / \
                          trial_factors.shape[0]

    max_variation = np.abs((time_factors - time_factors.mean())
                           / time_factors.std()).max()

    enough_units = perc_nonzero_units >= min_perc_nonzero_units
    enough_trials = perc_nonzero_trials >= min_perc_nonzero_trials
    enough_variation = max_variation >= min_variation

    if enough_units and enough_trials and enough_variation:
        is_bad = False
    else:
        is_bad = True
    return is_bad


def get_perc_nonzero_units(U, rank):
    unit_factors = U.factors[0][:, rank]
    perc_nonzero_units = 100 * (unit_factors > 0).sum() / unit_factors.shape[0]
    return perc_nonzero_units

def get_perc_nonzero_trials(U, rank):
    trial_factors = U.factors[2][:, rank]
    perc_nonzero_trials = 100 * (trial_factors > 0).sum() / trial_factors.shape[0]
    return perc_nonzero_trials

def get_max_variation(U, rank):
    time_factors = U.factors[1][:, rank]
    max_variation = np.abs((time_factors - time_factors.mean())
                           / time_factors.std()).max()
    return max_variation

def get_mean_of_nonzero_unit_factors(U, rank):
    unit_factors = U.factors[0][:, rank]
    nonzero_mean = unit_factors[unit_factors > 0].mean()
    return nonzero_mean
