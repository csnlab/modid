from constants import *
import pickle
from plotting_style import *
import matplotlib.pyplot as plt
import scipy.stats
from sklearn import preprocessing
from sklearn import model_selection
from sklearn.preprocessing import LabelEncoder
from tca.tca_utils import *
from sklearn.metrics.pairwise import cosine_similarity
import tensortools as tt

settings_name = 'june24b'
tca_rank = 10

# PLOTTING PARAMETERS
plot_format = 'png'
dpi = 400
save_plots = True


# --- LOAD RESULTS ------------------------------------------------------------

results_folder = os.path.join(DATA_FOLDER, 'results', 'TCA', settings_name)
results_file_name = 'TCA_settings_{}_rank_{}.pkl'.format(settings_name, tca_rank)
pars_file_name = 'TCA_settings_{}_rank_{}_pars.pkl'.format(settings_name, tca_rank)

rs = pickle.load(open(os.path.join(results_folder, results_file_name), 'rb'))
#pars = pickle.load(open(os.path.join(results_folder, pars_file_name), 'rb'))

sessions = list(rs.keys())
# --- SET UP PATHS ------------------------------------------------------------

plot_folder = os.path.join(DATA_FOLDER, 'plots', 'TCA_agg', settings_name,
                            'rank_{}'.format(tca_rank))

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder, exist_ok=True)


# -----------------------------------------------------------------------------
f, ax = plt.subplots(1, 2, figsize=[6, 3])

for session in sessions:

    area = session[2]
    E = rs[session]['ensemble']
    scatter_kw = {'s':0, 'c' : area_palette[area]}
    line_kw = {'c' : area_palette[area]}
    tt.plot_objective(E, ax=ax[0], scatter_kw=scatter_kw,
                      line_kw=line_kw)
    tt.plot_similarity(E, ax=ax[1], scatter_kw=scatter_kw,
                       line_kw=line_kw)
sns.despine()
plt.tight_layout()