import numpy as np
import pandas as pd
from constants import *
from loadmat import *
from session import Session
from utils import *
from sklearn.ensemble import RandomForestClassifier
import quantities as pq
from sklearn.preprocessing import StandardScaler, MinMaxScaler
import tensortools as tt
from plotting_style import *
import matplotlib.pyplot as plt
from session_info import select_sessions
import itertools
import scipy.stats
from sklearn import preprocessing
from sklearn import model_selection
from sklearn.metrics import roc_auc_score
from sklearn.preprocessing import LabelEncoder
from tca.tca_utils import compute_factor_discrimination_score

settings_name = 'mmm_nov11'

# if animal_id is specified, run for all sessions of that animal
# if area_spikes is not specified run for all available areas
animal_id = None
session_id = None
area_spikes = None
#area_spikes = 'all'  # if 'all' do one TCA for all units, otherwise area individually

animal_id = '2003'
session_id = '2018-02-08_15-13-56'
area_spikes = 'V1'

plot_selected = True

#2010  2018-08-14_12-24-28
# 2003  2018-02-07_14-28-57

min_units_per_area = 20

# TCA PARAMETERS
tca_rank = 8
fit_ensemble = False

# TIME PARAMETERS
align_event = 'stimChange'
sliding_window = False
time_before_stim_in_s = 0.4
time_after_stim_in_s = 0.8
binsize_in_ms = 1
slide_by_in_ms = -10

# SMOOTHING PARAMETERS
apply_smoothing = True
sampling_period = 10
kernel_width = 30
kernel_size = kernel_width * 8
kernel = scipy.signal.gaussian(kernel_size, kernel_width, sym=True)
kernel = 1000 * kernel / kernel.sum()

preprocess = 'z_score'
discrimination_score = 'auc'#'rf_cross_val'

# PLOTTING PARAMETERS
plot_unit_factors_overlap = False
sort_unit_factors_by_depth = False
color_units_by = 'area' # layer, area
plot_format = 'png'
dpi = 300
save_plots = True
plot_folder = os.path.join(DATA_FOLDER, 'plots', 'TCA', settings_name, 'rank_{}'.format(tca_rank))


if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder, exist_ok=True)


# --- create trial type combinations ------------------------------------------
responses = [0, 1]
modalities = ['X', 'Y']
stimuli = [(1, 2), (3, 4)]
stimulus_changes = [2, 3]
trial_types = list(itertools.product(responses, modalities, stimuli,
                                     stimulus_changes))
probe_trials = [(0, 'P', 0, 0), (1, 'P', 0, 0)]
trial_types = probe_trials + trial_types

# --- PREPARE DATA ------------------------------------------------------------

if session_id is None:
    sessions = select_sessions(min_units=min_units_per_area)

    if area_spikes == 'all':
        sessions = sessions.drop_duplicates(subset=['animal_id', 'session_id'])
    else:
        sessions = sessions.drop_duplicates(subset=['animal_id', 'session_id',
                                                    'area'])

    if animal_id is not None:
        sessions = sessions[sessions['animal_id'] == animal_id]

    if area_spikes is not None and area_spikes != 'all':
        sessions = sessions[sessions['area'] == area_spikes]

else:
    sessions = pd.DataFrame(columns=['animal_id', 'session_id', 'area'],
                            data=[(animal_id, session_id, area_spikes)])


import numpy as np
y = np.random.randint(0, 10, 100)



for i, row in sessions.iterrows():

    animal_id = row['animal_id']
    session_id = row['session_id']
    if not area_spikes == 'all':
        area_spikes = row['area']

    session = Session(animal_id=animal_id, session_id=session_id)
    session.load_data(load_lfp=False)

    selected_unit_ind = session.select_units(area=area_spikes)
    selected_unit_id = session.get_cell_id(selected_unit_ind, shortened_id=False)
    unit_area = session.get_cell_area(selected_unit_id)
    unit_depth = session.get_cell_depth(selected_unit_id)
    unit_layers = session.get_cell_layer(selected_unit_id)

    binned_spikes_all = {}
    n_trials_all = {}
    trial_nums = []

    for response, modality, stimulus, change in trial_types:

        if modality == 'X':
            trial_numbers = session.select_trials(response=response,
                                                  trial_type=modality,
                                                  visual_post_norm=stimulus,
                                                  visual_change=change)
        elif modality == 'Y':

            trial_numbers = session.select_trials(response=response,
                                                  trial_type=modality,
                                                  audio_post_norm=stimulus,
                                                  auditory_change=change)

        elif modality == 'P':

            trial_numbers = session.select_trials(response=response,
                                                  trial_type=modality,
                                                  audio_post_norm=stimulus,
                                                  auditory_change=change,
                                                  visual_post_norm=stimulus,
                                                  visual_change=change)

        trial_nums.append(trial_numbers)
        n_trials_all[(response, modality, stimulus, change)] = len(trial_numbers)

        if len(trial_numbers) > 0:
            trial_times = session.get_aligned_times(trial_numbers,
                                                    time_before_in_s=time_before_stim_in_s,
                                                    time_after_in_s=time_after_stim_in_s,
                                                    event=align_event)


            binned_spikes, spike_bin_centers = session.bin_spikes_per_trial(binsize_in_ms, trial_times,
                                                                            sliding_window=sliding_window,
                                                                            slide_by_in_ms=slide_by_in_ms)




            binned_spikes = [s[selected_unit_ind, :] for s in binned_spikes]

            if apply_smoothing:
                binned_spikes = [convolve_chunk(arr, kernel, sampling_period) for
                                 arr in binned_spikes]

            binned_spikes_all[(response, modality, stimulus, change)] = binned_spikes


            n_time_bins_per_trial = spike_bin_centers[0].shape[0]

            time_bin_times = (spike_bin_centers[0] - spike_bin_centers[0][0]).rescale(pq.s) \
                             + (binsize_in_ms*pq.ms).rescale(pq.s)/2 - time_before_stim_in_s*pq.s
            time_bin_times = time_bin_times.__array__().round(5)
            if apply_smoothing:
                time_bin_times = time_bin_times[int(kernel_size / 2):-int(kernel_size / 2)]
                time_bin_times = time_bin_times[::sampling_period]


    # --- PREPARE TENSOR ----------------------------------------------------------

    n_neurons = binned_spikes_all[trial_types[3]][0].shape[0]
    n_all_trials = np.sum([n_trials_all[k] for k in trial_types])
    n_times = binned_spikes_all[trial_types[3]][0].shape[1]

    X = np.hstack([np.hstack(binned_spikes_all[t]) for t in binned_spikes_all.keys()])
    #[len(binned_spikes_all[t]) for t in trial_groups]

    if preprocess == 'min_max':
        mm = MinMaxScaler(feature_range=(0, 1))
        X = mm.fit_transform(X.T).T
    elif preprocess == 'z_score':
        ss = StandardScaler(with_mean=True, with_std=True)
        X = ss.fit_transform(X.T).T

    X = np.reshape(X, (n_neurons, n_times, n_all_trials), order='F')

    trial_nums = np.hstack(trial_nums)
    trial_df = make_trial_df(session, trial_nums)

    # --- RUN TCA -----------------------------------------------------------------


    U = tt.ncp_bcd(X, rank=tca_rank, verbose=True)

    if fit_ensemble:
        E = tt.Ensemble(nonneg=True, fit_method='ncp_bcd')
        E.fit(X, ranks=[2, 4, 6, 8, 10, 12, 14, 16, 18], replicates=10)

        f, ax = plt.subplots(1, 2, figsize=[6, 3])
        tt.plot_objective(E, ax=ax[0])
        tt.plot_similarity(E, ax=ax[1])
        sns.despine()
        plt.tight_layout()


    unit_factors = U.factors[0]
    n_nonzero_units = []

    for k in range(unit_factors.shape[1]):
        f = unit_factors[:, k]
        n_nonzero_units.append((f > 0).sum())

    unit_factors = preprocessing.normalize(unit_factors, norm='l2', axis=0)
    unit_factor_overlap = np.zeros([unit_factors.shape[1], unit_factors.shape[1]])
    for j in range(unit_factors.shape[1]):
        for k in range(unit_factors.shape[1]):
            d = np.nansum(unit_factors[:, j]*unit_factors[:, k])
            unit_factor_overlap[j, k] = d

    if plot_unit_factors_overlap:
        fig, ax = plt.subplots(1, 1)
        sns.heatmap(unit_factor_overlap, ax=ax, cmap="Blues")
        ax.set_xlabel('Unit factors')
        ax.set_ylabel('Unit factors')
        ax.set_title('Unit factor overlap')
        plot_name = 'TCA_{}_{}_{}_rank_{}_factors_overlap.{}'.format(animal_id, session_id,
                                                     area_spikes, tca_rank, plot_format)
        fig.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)


    # --- compute discrimination score ----------------------------------------

    trial_factors = U.factors[2]
    # targets = [response_list, modality_list, stimulus_list_simple,
    #            stimulus_change_list]
    target_names = ['rew', 'lick', 'side', 'mod', 'stim', 'stim_ch']
    df = pd.DataFrame(columns=['factor', 'target', 'score'])

    for target_name in target_names:
        if np.isin(target_name, ['mod', 'stim', 'stim_ch']):
            mask = np.isin(trial_df['mod'], ['X', 'Y'])
        elif target_name == 'side':
            mask = trial_df['lick'] == 1
        else:
            mask = np.isin(trial_df['mod'], ['P', 'X', 'Y'])

        for k in range(trial_factors.shape[1]):
            X = trial_factors[:, k][mask, np.newaxis]
            y = trial_df[mask][target_name].values

            ll = LabelEncoder()
            y = ll.fit_transform(y)


            score = compute_factor_discrimination_score(X, y,
                                                        score_name=discrimination_score)
            df.loc[df.shape[0], :] = [k, target_name, score]


    # --- PLOT TCA ------------------------------------------------------------

    if color_units_by == 'area':
        unit_color = [area_palette[k] for k in unit_area]
    elif color_units_by == 'layer':
        unit_color = [layer_palette[k] for k in unit_layers]

    mod_color = [modality_palette[k] for k in trial_df['mod']]
    stim_color = [stimulus_palette[k] for k in trial_df['stim']]
    stim_ch_color = [stimulus_change_palette[k] for k in trial_df['stim_ch']]
    rew_color = [reward_palette[k] for k in trial_df['rew']]
    lick_color = [lick_palette[k] for k in trial_df['lick']]
    side_color = [lick_side_palette[k] for k in trial_df['side']]

    trial_inds = (np.hstack(trial_nums) - 1).astype(int)
    trial_inds = trial_inds.argsort()

    titles = ['Unit factors', 'Time factors', 'Trial factors\nReward',
              'Trial factors\nLick', 'Trial factors\nLick side',
              'Trial factors\nModality', 'Trial factors\nStimulus identity',
              'Trial factors\nStimulus change', 'Trial factors\nReal order',
              'Discrimination']

    xlabels = ['Units', 'Time (s)', 'Trials', 'Trials', 'Trials', 'Trials', 'Trials',
               'Trials', 'Trials', '']


    trial_scatter_colors = {'rew' : rew_color,
                            'lick' : lick_color,
                            'side' : side_color,
                            'mod'  : mod_color,
                            'stim' : stim_color,
                            'stim_ch' : stim_ch_color,
                            'time' : sns.xkcd_rgb['grey']}

    axis_for_labels = [2, 3, 4, 5, 6, 7, 9]
    palette_titles =  ['Reward', 'Lick', 'Lick side', 'Modality', 'Stimulus identity',
                       'Stimulus change', 'Target']
    legend_palettes = [reward_palette, lick_palette, lick_side_palette, modality_palette,
                       stimulus_palette, stimulus_change_palette, targets_palette]
    legend_labels = [reward_labels, lick_labels, lick_side_labels, modality_labels,
                     stimulus_labels, stimulus_change_labels, targets_labels]

    n_trial_scatters = len(trial_scatter_colors)
    n_factor_plots = 2 + n_trial_scatters
    ndim = 2 + n_trial_scatters + 1

    for r in range(U.factors.rank):
        print(get_factor_quality_metrics(U, r))

    fig, axes = plt.subplots(U.factors.rank, ndim,
                             figsize=[ndim*2.5, U.factors.rank*1.5],
                             sharex='col')
    # for r in range(U.factors.rank):
    #     result = adfuller(X)
    #     p_val = result[1]
    #
    #     if is_bad:
    #         axes[r, 0].set_ylabel('BAD')

    # plot unit factors
    unit_factors = U.factors[0]

    if sort_unit_factors_by_depth:
        sort_ind = unit_depth.argsort()[::-1]
        unit_factors = unit_factors[sort_ind]
        unit_color = np.array(unit_color)[sort_ind]

    x = np.arange(1, unit_factors.shape[0] + 1)
    for r in range(U.factors.rank):
        axes[r, 0].bar(x, unit_factors[:, r], color=unit_color)
    axes[r, 0].set_xlim(0, f.shape[0] + 1)

    # plot time factors
    time_factors = U.factors[1]
    for r in range(U.factors.rank):
        axes[r, 1].plot(time_bin_times, time_factors[:, r], '-')



    # plot trial factors
    trial_factors = U.factors[2]
    x = np.arange(1, trial_factors.shape[0] + 1)
    for i, scatter_type in enumerate(trial_scatter_colors.keys()):
        color = trial_scatter_colors[scatter_type]
        for r in range(U.factors.rank):
            if scatter_type == 'time':
                y = trial_factors[trial_inds, r]
            else:
                y = trial_factors[:, r]
            axes[r, i+2].scatter(x, y, s=8, c=color)

    # # plot distribution of unit factors
    # all_factors = U.factors[0].flatten()
    # bins = np.linspace(all_factors.min(), all_factors.max(), 20)
    # for r in range(U.factors.rank):
    #     sns.distplot(unit_factors[:, r],ax=axes[r, -2], bins=bins, kde=False)
    #     axes[r, -2].set_xlim([-0.1, axes[r, -2].get_xlim()[1]])

    # plot discrimination score
    for r in range(U.factors.rank):
        xx = df[df.factor == r]
        sns.barplot(data=xx, x='target', y='score', ax=axes[r, -1],
                    order=target_names,
                    palette=[targets_palette[k] for k in target_names])
        axes[r, -1].set_xticklabels([])
        axes[r, -1].set_ylabel('')
        axes[r, -1].set_xlabel('')
        #axes[r, -1].set_ylim([0.45, axes[r, -1].get_ylim()[1]])
    axes[r, -1].set_xlabel('')


    for i in range(ndim):
        axes[0, i].set_title(titles[i])
        axes[-1, i].set_xlabel(xlabels[i])


    # format axes
    for r in range(U.factors.rank):
        for i in range(n_factor_plots):
            axes[r, i].locator_params(nbins=4)
            axes[r, i].spines['top'].set_visible(False)
            axes[r, i].spines['right'].set_visible(False)
            axes[r, i].xaxis.set_tick_params(direction='out')
            axes[r, i].yaxis.set_tick_params(direction='out')
            axes[r, i].yaxis.set_ticks_position('left')
            axes[r, i].xaxis.set_ticks_position('bottom')

            # remove xticks on all but bottom row
            if r != U.factors.rank - 1:
                plt.setp(axes[r, i].get_xticklabels(), visible=False)


    # format y-ticks
    for r in range(U.factors.rank):
        for i in range(ndim):
            # only two labels
            ymin, ymax = np.round(axes[r, i].get_ylim(), 2)
            axes[r, i].set_ylim((ymin, ymax))
            # remove decimals from labels
            if ymin.is_integer():
                ymin = int(ymin)
            if ymax.is_integer():
                ymax = int(ymax)
            # update plot
            axes[r, i].set_yticks([ymin, ymax])

    # add legends
    for i, (axis_ind, pal, lab) in enumerate(zip(axis_for_labels,
                                                 legend_palettes, legend_labels)):
        print(axis_ind)
        markers, labels = get_markers_and_labels(pal, lab)

        axes[-1, axis_ind].legend(markers, labels, loc='upper center',
                           title=palette_titles[i],
                           bbox_to_anchor=(0.5, -1), frameon=False,
                           ncol=1)
    sns.despine()
    plt.tight_layout(rect=[0,0.27,1,1])


    plot_name = 'TCA_{}_{}_{}_rank_{}.{}'.format(animal_id, session_id,
                                                 area_spikes, tca_rank, plot_format)
    fig.savefig(os.path.join(plot_folder, plot_name), dpi=dpi,
                bbox_inches="tight")





    # --- PLOT TCA SELECTIVE --------------------------------------------------

    if plot_selected:

        modality_palette = {'X': sns.xkcd_rgb['sea blue'],
                            'Y': sns.xkcd_rgb['orange yellow']}

        legend_palettes_dict = {'rew': reward_palette,
                                'lick': lick_palette,
                                'side': lick_side_palette,
                                'mod': modality_palette,
                                'stim': stimulus_palette,
                                'stim_ch': stimulus_change_palette}


        # PARAMETERS
        factors_to_plot = [3, 7, 2, 6, 5]
        scatter_contrasts = ['mod', 'stim_ch', 'stim_ch', 'stim_ch', 'lick']

        if color_units_by == 'area':
            unit_color = [area_palette[k] for k in unit_area]
        elif color_units_by == 'layer':
            unit_color = [layer_palette[k] for k in unit_layers]


        trial_scatter_colors_sel = [trial_scatter_colors[s] for s in scatter_contrasts]
        palette_titles = [targets_labels[s] for s in scatter_contrasts]
        legend_palettes = [legend_palettes_dict[s] for s in scatter_contrasts]
        legend_labels = [legend_labels_dict[s] for s in scatter_contrasts]

        trial_inds = (np.hstack(trial_nums) - 1).astype(int)
        trial_inds = trial_inds.argsort()

        xlabels = ['Units', 'Time (s)', 'Trials']

        n_trial_scatters = len(trial_scatter_colors)
        n_factor_plots = 2 + n_trial_scatters
        ndim = 2 + n_trial_scatters + 1

        for r in range(U.factors.rank):
            print(get_factor_quality_metrics(U, r))

        fig, axes = plt.subplots(len(factors_to_plot), 3,
                                 figsize=[3*2.5, len(factors_to_plot)],
                                 sharex='col', sharey=False)

        unit_factors = U.factors[0]
        if sort_unit_factors_by_depth:
            sort_ind = unit_depth.argsort()[::-1]
            unit_factors = unit_factors[sort_ind]
            unit_color = np.array(unit_color)[sort_ind]

        x = np.arange(1, unit_factors.shape[0] + 1)
        for ii, r in enumerate(factors_to_plot):
            axes[ii, 0].bar(x, unit_factors[:, r], color=unit_color)
        axes[ii, 0].set_xlim(0, f.shape[0] + 1)

        # plot time factors
        time_factors = U.factors[1]
        for ii, r in enumerate(factors_to_plot):
            axes[ii, 1].plot(time_bin_times, time_factors[:, r], '-')

        # plot trial factors
        trial_factors = U.factors[2]
        x = np.arange(1, trial_factors.shape[0] + 1)

        for ii, (r, color) in enumerate(zip(factors_to_plot, trial_scatter_colors_sel)):
            y = trial_factors[trial_inds, r]
            color = np.array(color)[trial_inds]
            axes[ii, 2].scatter(x, y, s=8, c=color)

        for i in range(3):
            axes[-1, i].set_xlabel(xlabels[i])

        for i in range(3):
            axes[i, 1].axvline(0, c='grey', alpha=0.8, ls='--')

        # format axes
        for r in range(axes.shape[0]):
            for i in range(axes.shape[1]):
                axes[r, i].locator_params(nbins=3)
                axes[r, i].spines['top'].set_visible(False)
                axes[r, i].spines['right'].set_visible(False)
                # axes[r, i].xaxis.set_tick_params(direction='out')
                # axes[r, i].yaxis.set_tick_params(direction='out')
                # axes[r, i].yaxis.set_ticks_position('left')
                # axes[r, i].xaxis.set_ticks_position('bottom')

        sns.despine()
        plt.tight_layout()

        for i, (pal, lab) in enumerate(zip(legend_palettes, legend_labels)):
            markers, labels = get_markers_and_labels(pal, lab)
            axes[i, 2].legend(markers, labels, loc='upper center',
                               title=palette_titles[i],
                               bbox_to_anchor=(1.5, 0.8), frameon=False,
                               ncol=1)
        plt.tight_layout(rect=[0,0,0.8,1])

        # # format y-ticks
        # for r in range(U.factors.rank):
        #     for i in range(ndim):
        #         # only two labels
        #         ymin, ymax = np.round(axes[r, i].get_ylim(), 2)
        #         axes[r, i].set_ylim((ymin, ymax))
        #         # remove decimals from labels
        #         if ymin.is_integer():
        #             ymin = int(ymin)
        #         if ymax.is_integer():
        #             ymax = int(ymax)
        #         # update plot
        #         axes[r, i].set_yticks([ymin, ymax])

        # add legends



        plot_name = 'TCA_selected_{}_{}_{}_rank_{}.{}'.format(animal_id, session_id,
                                                     area_spikes, tca_rank, plot_format)
        fig.savefig(os.path.join(plot_folder, plot_name), dpi=dpi,
                    bbox_inches="tight")

