from constants import *
import pickle
from plotting_style import *
import matplotlib.pyplot as plt
import scipy.stats
from sklearn.preprocessing import LabelEncoder
from tca.tca_utils import *
from sklearn.metrics.pairwise import cosine_similarity
from scipy.ndimage.filters import gaussian_filter1d


"""
Load TCA results and generate various plots

We first generate a dataframe where we characterize each TCA component 
in various ways (e.g. number of nonzero units/trials, kurtosis, ability
to discriminate trials of different type). 

Secondly, we have the option to filter out 'bad' factors. That is because 
in the meta analysis of TCA components we may want to remove components 
which only capture noise drifts, or only capture activity in very few 
units/trials. 

When looking at the discrimination scores, you can set in the parameters 
whether you want to include separately audio and visual stimulus identity/change,
or whether they should be aggregated in stimulus identity/stimulus change. 
This applies to all the plots. 

0. Pairplot of single unit discrimination for all targets. For every unit,
we compute a discrimination score which is how much it contributes to a component
(divided by how much it contributes to all components) times the scores of
that component for each target. 
1. Plot discrimination scores. We aggregate factors per area an look at their
discrimination scores for each target.
2. Plot kurtosis scores. We plot kurtosis scores for all factors split by
area and by preferred target (i.e. for every factor we keep the target
which it discriminates best).
3. Per area, plot the percentage of factors for each target (percentages are
computed separately for each area so as to alleviate the fact that an area
can have overall many more components just because there is more data, 
whereas we are interested in the relative distribution).
4. Overlap (of unit factors) by preferred target. Do factors which discriminate
modality recruit the same units as those which discriminate between e.g. 
stimulus identity?
5. Overlap by peak time: do factors which peak early recruit the same
units as later factors?
6. Plot factor metrics (e.g. kurtosis) split by time (split specified by the
split_intervals parameters)
7. Plot discrimination scores splitting components by time (split specified 
by the split_intervals parameters)
8. Pairplot of everything (discrimination scores and factor metrics, e.g. 
kurtosis)
9. Smaller pairplot with only unit factor metrics.
10. Jointplot of peak time vs discrimination score (one panel per target)
11. Plot factor lambdas by target / area. Here for every factor we keep the 
target that it discriminates best, and associate the factor lambda with that 
target. 
12. Plot the factor weighted lambdas by target / area. Here we weigh the lambda
by the score for each factor before aggregating. 
13. Discrimination score by peak time. Here for every target we take all the
discrimination scores and associate them with the peak times of each factor. 
We then interpolate the peak times and smooth a bit. 
14. Same as 13 but instead of the discimination score we plot factor metrics. 
15. For every unit we compute a score which its relative participation in a 
factor with a certain preferred target times the discrimination score of that
target (relative to the total unit scores for that unit). We then average
this score across all the factors with the same preferred target. We then
plot this score split by target, area, and layer. 
16. Unit factors by depth for early and late-peaking components. 
For every factor, we see what its peak time is. If it's early, we take its 
unit factors, together with the unit depths, and put them in a dataframe with
the label 'early'. This allows us to later select only unit factors for the 
early components, and bin their unit factors by ranges of depth. 
17. Unit factors by depth by target. Same as 16. but instead of grouping
components by early and late peak times, we split them based on their preferred
target. 
"""

# TODO: note that in not all the plots we save the discrimination score name
# that we used


settings_name = 'mmm_nov11'

plotting_settings_name = 'mmm_nov11_5'

tca_rank = 12


# FILTERING FACTORS PARAMETERS
# if a parameter is None, e.g. min_score=None, min_score is set as the
# lowest quantile, with the number of quantiles set by q
filter_factors = True
min_perc_nonzero_units = 2
min_perc_nonzero_trials = 10
min_variation = None
min_score = 0.6
q = 10

discrimination_score = 'auc' #'auc', 'rf_cross_val'
discrimination_by_unit = False

# PLOTTING PARAMETERS
plot_format = 'png'
dpi = 400
save_plots = True

# for the lineplots of scores and factor metrics
smoothing_sigma = 20
dt = 0.001

#target_names = ['rew', 'lick', 'side', 'mod', 'stim', 'stim_ch']
#target_names = ['rew', 'lick', 'side', 'mod', 'X_stim', 'Y_stim', 'X_stim_ch', 'Y_stim_ch']
target_names = ['lick', 'mod', 'X_stim', 'X_stim_ch', 'Y_stim', 'Y_stim_ch']

split_intervals = [(0, 0.22), (0.22, 0.5)]

# --- LOAD RESULTS ------------------------------------------------------------

results_folder = os.path.join(DATA_FOLDER, 'results', 'TCA', settings_name)
results_file_name = 'TCA_settings_{}_rank_{}.pkl'.format(settings_name, tca_rank)
pars_file_name = 'TCA_settings_{}_rank_{}_pars.pkl'.format(settings_name, tca_rank)


rs = pickle.load(open(os.path.join(results_folder, results_file_name), 'rb'))
pars = pickle.load(open(os.path.join(results_folder, pars_file_name), 'rb'))
time_before_stim_in_s = pars['time_before_stim_in_s']
time_after_stim_in_s = pars['time_after_stim_in_s']

# --- SET UP PATHS ------------------------------------------------------------

plot_folder = os.path.join(DATA_FOLDER, 'plots', 'TCA_agg', settings_name,
                           'rank_{}'.format(tca_rank), plotting_settings_name)

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder, exist_ok=True)


# --- PREPARE DATAFRAME -------------------------------------------------------

# TODO here we manually redefine this
# could be ok cause we make the df with all the possible contrasts,
# and the target_names above (user defined) is for what we actually plot
_target_names = ['rew', 'lick', 'side', 'mod', 'stim', 'stim_ch',
                 'X_stim', 'Y_stim', 'X_stim_ch', 'Y_stim_ch']

df = pd.DataFrame(columns=['animal_id', 'session_id', 'area',
                           'factor', 'lambda', 'peak_time', 'kurtosis',
                           'perc_nonzero_units', 'perc_nonzero_trials',
                           'max_variation', 'nonzero_mean_unit_factor',
                           'target', 'score', 'unit_factors', 'unit_ids',
                           'unit_depth', 'unit_layer'])



for (animal_id, session_id, area_spikes) in rs.keys():
    print(animal_id, session_id, area_spikes)

    key = (animal_id, session_id, area_spikes)
    U = rs[key]['factors']
    time_bin_times = rs[key]['time_bin_times']
    unit_ids = rs[key]['selected_unit_id']
    unit_depth = rs[key]['unit_depth']
    unit_layer = rs[key]['unit_layer']

    unit_factors = U.factors[0]
    time_factors = U.factors[1]
    trial_factors = U.factors[2]

    trial_df = rs[key]['trial_df']

    lambdas = U.factors.factor_lams()
    lambdas = lambdas / lambdas.sum()

    for rnk in range(tca_rank):

        # COMPUTE FACTOR PROPERTIES
        perc_nonzero_units = get_perc_nonzero_units(U, rnk)
        perc_nonzero_trials = get_perc_nonzero_trials(U, rnk)
        max_variation = get_max_variation(U, rnk)
        nonzero_mean = get_mean_of_nonzero_unit_factors(U, rnk)
        peak_time = time_bin_times[time_factors[:, rnk].argmax()]
        kurtosis = scipy.stats.kurtosis(unit_factors[:, rnk])
        factor_lambda = lambdas[rnk]

        for target_name in _target_names:
            if np.isin(target_name, ['mod', 'stim', 'stim_ch']):
                mask = np.isin(trial_df['mod'], ['X', 'Y'])
                target_col = target_name
            elif np.isin(target_name, ['X_stim', 'X_stim_ch']):
                mask = np.isin(trial_df['mod'], ['X'])
                target_col = '_'.join(target_name.split('_')[1:])
            elif np.isin(target_name, ['Y_stim', 'Y_stim_ch']):
                mask = np.isin(trial_df['mod'], ['Y'])
                target_col = '_'.join(target_name.split('_')[1:])
            elif target_name == 'side':
                mask = trial_df['lick'] == 1
                target_col = target_name
            elif np.isin(target_name, ['lick', 'rew']):
                mask = np.isin(trial_df['mod'], ['P', 'X', 'Y'])
                target_col = target_name
            else:
                raise ValueError

            X = trial_factors[:, rnk][mask, np.newaxis]
            y = trial_df[target_col][mask].values
            ll = LabelEncoder()
            y = ll.fit_transform(y)
            score = compute_factor_discrimination_score(X, y,
                                score_name=discrimination_score)
            ind = df.shape[0]
            df.loc[ind, :] = [animal_id, session_id, area_spikes,
                              rnk, factor_lambda, peak_time, kurtosis,
                              perc_nonzero_units,
                              perc_nonzero_trials, max_variation,
                              nonzero_mean,
                              target_name, score, None, None, None, None]
            df.loc[ind, 'unit_factors'] = unit_factors[:, rnk]
            df.loc[ind, 'unit_ids'] = np.array(unit_ids)
            df.loc[ind, 'unit_depth'] = np.array(unit_depth)
            df.loc[ind, 'unit_layer'] = np.array(unit_layer)


numeric_cols = ['score', 'peak_time', 'kurtosis', 'perc_nonzero_units',
                'lambda']
for col in numeric_cols:
    df[col] = pd.to_numeric(df[col])

print('SANITY CHECK: number of components')
print(df.groupby(['area'])['target'].value_counts())


df_sel = df.loc[df.groupby(['animal_id', 'session_id',
                            'area', 'factor'])['score'].idxmax()]

print('\n\n PREFERRED TARGETS FOR FACTORS BEFORE FILTERING\n\n')
print(df_sel.groupby(['area'])['target'].value_counts())

# --- FILTER OUT BAD FACTORS --------------------------------------------------

if filter_factors:

    if min_perc_nonzero_units is None:
        min_perc_nonzero_units = pd.qcut(df['perc_nonzero_units'], q).cat.categories[0].right

    if min_perc_nonzero_trials is None:
        min_perc_nonzero_trials = pd.qcut(df['perc_nonzero_trials'], q).cat.categories[0].right

    if min_variation is None:
        min_variation = pd.qcut(df['max_variation'], q).cat.categories[0].right

    if min_score is None:
        min_score = pd.qcut(df['score'], q).cat.categories[0].right

    groupbys = ['animal_id', 'session_id', 'area', 'factor']

    df = df.groupby(groupbys).filter(lambda x: x['perc_nonzero_units'].max() >= min_perc_nonzero_units)
    df = df.groupby(groupbys).filter(lambda x: x['perc_nonzero_trials'].max() >= min_perc_nonzero_trials)
    df = df.groupby(groupbys).filter(lambda x: x['max_variation'].max() >= min_variation)
    df = df.groupby(groupbys).filter(lambda x: x['score'].max() >= min_score)


df_sel = df.loc[df.groupby(['animal_id', 'session_id',
                            'area', 'factor'])['score'].idxmax()]
print('\n\n PREFERRED TARGETS FOR FACTORS AFTER FILTERING\n\n')
print(df_sel.groupby(['area'])['target'].value_counts())

# --- DF BY UNIT --------------------------------------------------------------

unit_ids_all = []
unit_depth_all = []
unit_layer_all = []
scores_all = []
targets_all = []
areas_all = []

# here since we iterate over all the rows of df, we take into account the fact
# that a target discriminates multiple things,
# for i, row in df.iterrows():
#
#     unit_ids = row['unit_ids']
#     unit_depth = row['unit_depth']
#     unit_layer = row['unit_layer']
#     unit_factors = row['unit_factors']
#     area = row['area']
#     target = row['target']
#     score = row['score']
#     animal_id = row['animal_id']
#     session_id = row['session_id']
#     #print(i)
#
#     # here we extract the unit factors across all components of the session
#     dfx = df[(df['animal_id'] == animal_id) &
#              (df['session_id'] == session_id) &
#              (df['area'] == area)]
#     dfx = dfx.drop_duplicates(subset=['animal_id', 'session_id', 'area', 'factor'])
#     unit_factors_all = np.vstack(dfx['unit_factors'])
#
#     # for every unit, the total amount of unit factors (across factors)
#     total_factors = unit_factors_all.sum(axis=0)
#     # so we can express a weighted score as the score of the factor
#     # (for the given target) times
#     # the relative involvement of a unit in that factor
#     weighted_score = score * (unit_factors / total_factors)
#     # TODO how do we compute the score?
#     #weighted_score = score * (unit_factors)
#
#     unit_ids_all.append(unit_ids)
#     unit_depth_all.append(unit_depth)
#     unit_layer_all.append(unit_layer)
#     scores_all.append(weighted_score)
#     targets_all.append([target]*len(unit_ids))
#     areas_all.append([area]*len(unit_ids))
#
for i, row in df.iterrows():

    unit_ids = row['unit_ids']
    unit_depth = row['unit_depth']
    unit_layer = row['unit_layer']
    unit_factors = row['unit_factors']
    area = row['area']
    target = row['target']
    score = row['score']
    animal_id = row['animal_id']
    session_id = row['session_id']
    #print(i)

    dfx = df[(df['animal_id'] == animal_id) &
             (df['session_id'] == session_id) &
             (df['area'] == area)]
    dfx = dfx.drop_duplicates(subset=['animal_id', 'session_id', 'area', 'factor'])

    # for every unit, the total amount of unit factors (across factors)
    total_factors = np.vstack(dfx['unit_factors']).sum(axis=0)
    # so we can express a weighted score as the score of the factor times
    # the relative involvement of a unit in that factor
    weighted_score = score * (unit_factors / total_factors)
    # TODO how do we compute the score?
    #weighted_score = score * (unit_factors)

    unit_ids_all.append(unit_ids)
    unit_depth_all.append(unit_depth)
    unit_layer_all.append(unit_layer)
    scores_all.append(weighted_score)
    targets_all.append([target]*len(unit_ids))
    areas_all.append([area]*len(unit_ids))

dfu = pd.DataFrame(columns=['unit_id', 'area', 'target', 'score'])
dfu['unit_id'] = np.hstack(unit_ids_all)
dfu['area'] = np.hstack(areas_all)
dfu['layer'] = np.hstack(unit_layer_all)
dfu['depth'] = np.hstack(unit_depth_all)
dfu['target'] = np.hstack(targets_all)
dfu['score'] = np.hstack(scores_all)

numeric_cols = ['score']
for col in numeric_cols:
    dfu[col] = pd.to_numeric(dfu[col])

dfu = dfu.groupby(['unit_id', 'target', 'area', 'layer']).mean().reset_index()

dfu_pivot = dfu.sort_values(['unit_id', 'target'])
area_col = dfu_pivot[dfu_pivot['target'] == 'rew']['area']
layer_col = dfu_pivot[dfu_pivot['target'] == 'rew']['layer']
depth_col = dfu_pivot[dfu_pivot['target'] == 'rew']['depth']

unit_index = dfu_pivot[dfu_pivot['target'] == 'rew']['unit_id']
dfu_pivot = dfu_pivot.pivot(index='unit_id', columns='target', values='score')
dfu_pivot['area'] = area_col.as_matrix()
dfu_pivot['layer'] = layer_col.as_matrix()
dfu_pivot['depth'] = depth_col.as_matrix()

# -----------------------------------------------------------------------------



# --- 0. PAIRPLOT SINGLE UNITS ALL TARGETS ------------------------------------

np.testing.assert_array_equal(unit_index, dfu_pivot.index)
if False:
    g = sns.pairplot(dfu_pivot, vars=target_names, hue='area', hue_order=AREAS,
                     palette=[area_palette[a] for a in AREAS],
                     plot_kws=dict(s=20))

    labels = [targets_labels[t].replace(" ", "\n") for t in target_names]
    for i, lab in enumerate(labels):
        g.axes[i, 0].set_ylabel(lab)
        g.axes[-1, i].set_xlabel(lab)
    plt.tight_layout(rect=(0, 0, 0.94, 1))

# 1. --- PLOT DISCRIMINATION SCORES --------------------------------------------

# eliminate target scores that we don't want in the plot
df_sel = df[np.isin(df['target'], target_names)]

labels = [targets_labels[t].replace(" ", "\n") for t in target_names]

f, ax = plt.subplots(1, 1, figsize=[4, 4])

sns.barplot(data=df_sel, x='target', y='score', ax=ax,
            order=target_names, palette=[area_palette[a] for a in AREAS],
            hue='area', hue_order=AREAS)
ax.get_legend().remove()
ax.set_xticklabels(labels, rotation=90)
ax.set_xlabel('')
if discrimination_score == 'auc':
    ax.set_ylim([0.5, ax.get_ylim()[1]])
ax.set_ylabel('Discrimination score')
sns.despine()
plt.tight_layout()

plot_name = 'TCA_scores_global_{}_{}_{}.{}'.format(
    settings_name, discrimination_score, tca_rank, plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi, bbox_inches="tight")



# 2. --- PLOT KURTOSIS SCORES BY PREFERRED TARGET -----------------------------
# it's important that we first subselect the targets we want and find the
# preferred ones only among those
df_sel = df[np.isin(df['target'], target_names)]
# pick the best out of the ones we want
df_sel = df_sel.loc[df_sel.groupby(['animal_id', 'session_id',
                                    'area', 'factor'])['score'].idxmax()]

labels = [targets_labels[t].replace(" ", "\n") for t in target_names]

df_sel['weighted_kurtosis'] = df_sel['kurtosis'] * df_sel['score']

f, ax = plt.subplots(1, 1, figsize=[4, 4])

sns.barplot(data=df_sel, x='target', y='weighted_kurtosis', ax=ax,
            order=target_names, palette=[area_palette[a] for a in AREAS],
            hue='area', hue_order=AREAS)
ax.get_legend().remove()
ax.set_xticklabels(labels, rotation=90)
ax.set_xlabel('')
ax.set_ylabel('Kurtosis score')
sns.despine()
plt.tight_layout()


# 3. --- PLOT PERCENTAGE OF FACTORS BY AREA AND PREFERRED TARGET --------------

df_sel = df[np.isin(df['target'], target_names)]
# pick the best out of the ones we want
df_sel = df_sel.loc[df_sel.groupby(['animal_id', 'session_id',
                                    'area', 'factor'])['score'].idxmax()]


prop_df = (df_sel['target']
           .groupby(df_sel['area'])
           .value_counts(normalize=True)
           .rename('prop')
           .reset_index())
prop_df['prop'] = prop_df['prop'] * 100
f, ax = plt.subplots(1, 1, figsize=[4, 4])
sns.barplot(x='target', y='prop', hue='area', data=prop_df, ax=ax,
            hue_order=AREAS,order=target_names,
            palette=[area_palette[a] for a in AREAS])
ax.get_legend().remove()
ax.set_xticklabels(labels, rotation=90)
ax.set_xlabel('')
ax.set_ylabel('% components by preferred target')
sns.despine()
plt.tight_layout()


plot_name = 'TCA_percentage_of_factors_by_area_and_target_{}_{}_{}.{}'.format(
            settings_name, discrimination_score, tca_rank, plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi, bbox_inches="tight")



# f, ax = plt.subplots(1, 1, figsize=[4, 4])
#
# sns.countplot(data=df_sel, x='target', ax=ax,
#               order=target_names, palette=[area_palette[a] for a in AREAS],
#               hue='area', hue_order=AREAS)
#
# ax.get_legend().remove()
# ax.set_xticklabels(labels, rotation=90)
# ax.set_xlabel('')
# ax.set_ylabel('# components by preferred target')
# sns.despine()
# plt.tight_layout()



# 4. --- OVERLAP BY PREFERRED TARGET -------------------------------------------

# eliminate target scores that we don't want in the plot
df_sel = df[np.isin(df['target'], target_names)]
# pick the best out of the ones we want
df_sel = df_sel.loc[df_sel.groupby(['animal_id', 'session_id', 'area', 'factor'])['score'].idxmax()]

# alternative way gives same result - good!
#groupbys = ['animal_id', 'session_id', 'area', 'factor']
#dfx = df[np.isin(df['target'], target_names)].sort_values('score', ascending=False).drop_duplicates(groupbys)
#dfx = dfx.sort_values(groupbys)
#df_sel = df_sel.sort_values(groupbys)
#np.testing.assert_array_equal(dfx['score'], df_sel['score'])

for area in df_sel['area'].unique():

    keys = [k for k in rs.keys() if k[2] == area]
    overlap_mat = np.zeros((len(target_names), len(target_names)))
    n_mat = np.zeros_like(overlap_mat)

    for (animal_id, session_id, area_spikes) in keys:

        dfx = df_sel[(df_sel['animal_id'] == animal_id) &
                     (df_sel['session_id'] == session_id) &
                     (df_sel['area'] == area_spikes)]


        for i in range(dfx.shape[0]):

            for j in range(dfx.shape[0]):
                un1 = dfx.iloc[i]['unit_factors']
                un2 = dfx.iloc[j]['unit_factors']

                overlap = cosine_similarity(un1.reshape(-1, 1).T,
                                               un2.reshape(-1, 1).T)[0, 0]
                tar1 = dfx.iloc[i]['target']
                tar2 = dfx.iloc[j]['target']

                # un1 = preprocessing.normalize(un1.reshape(-1, 1), norm='l2', axis=0)
                # un2 = preprocessing.normalize(un2.reshape(-1, 1), norm='l2', axis=0)
                # overlap = np.nansum(un1 * un2)
                # np.testing.assert_array_almost_equal(overlap0, overlap)

                overlap_mat[target_names.index(tar1), target_names.index(tar2)] += overlap
                n_mat[target_names.index(tar1), target_names.index(tar2)] += 1

    overlap_mat_mean = overlap_mat / n_mat

    labels = [targets_labels[t].replace(" ","\n") for t in target_names]
    f, ax = plt.subplots(1, 1)
    sns.heatmap(overlap_mat_mean, ax=ax, cmap=area_cmap[area], annot=True,  linewidths=.8)
    ax.set_xticklabels(labels, rotation=40)
    ax.set_yticklabels(labels, rotation=0)
    ax.tick_params(axis=u'both', which=u'both',length=0)
    plt.tight_layout()


    plot_name = 'TCA_factor_overlap_by_preferred_target_{}_{}_{}_{}.{}'.format(settings_name, area, discrimination_score,
                                                                            tca_rank, plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi,bbox_inches="tight")

    from scipy.cluster.hierarchy import dendrogram, linkage
    from scipy.spatial.distance import squareform

    # distance_matrix = (1-overlap_mat_mean)
    # np.fill_diagonal(distance_matrix, 0)
    # dists = squareform(distance_matrix)
    # linkage_matrix = linkage(dists, "single")
    #
    # f, ax = plt.subplots(1, 1)
    # dendrogram(linkage_matrix, color_threshold=1, labels=labels,
    #            show_leaf_counts=True, ax=ax)
    # ax.set_title(area)
    # sns.despine()



# 5. --- OVERLAP BY PEAK TIME --------------------------------------------------

df_sel = df.drop_duplicates(subset=['animal_id', 'session_id', 'area', 'factor'])

bins = np.arange(-time_before_stim_in_s, time_after_stim_in_s, 0.1).round(2)

df_sel['time_bin'] = pd.cut(df_sel['peak_time'], bins=bins).cat.codes

overlap_mat = np.zeros((len(bins)-1, len(bins)-1))
n_mat = np.zeros_like(overlap_mat)

for (animal_id, session_id, area_spikes) in rs.keys():

    dfx = df_sel[(df_sel['animal_id'] == animal_id) &
                 (df_sel['session_id'] == session_id) &
                 (df_sel['area'] == area_spikes)]

    for i in range(dfx.shape[0]):
        for j in range(dfx.shape[0]):
            un1 = dfx.iloc[i]['unit_factors']
            un2 = dfx.iloc[j]['unit_factors']

            overlap = cosine_similarity(un1.reshape(-1, 1).T,
                                        un2.reshape(-1, 1).T)[0, 0]

            tb1 = dfx.iloc[i]['time_bin']
            tb2 = dfx.iloc[j]['time_bin']

            overlap_mat[tb1, tb2] += overlap
            n_mat[tb1, tb2] += 1

n_mat[n_mat==0] = 1
overlap_mat_mean = overlap_mat / n_mat

labels = ['{} to {}s'.format(bins[i], bins[i+1]) for i in range(len(bins)-1)]
f, ax = plt.subplots(1, 1)
sns.heatmap(overlap_mat_mean, ax=ax, cmap='Blues', annot=False, linewidths=.8,
            yticklabels=labels, xticklabels=labels)
ax.tick_params(axis=u'both', which=u'both',length=0)
plt.tight_layout()

plot_name = 'TCA_factor_overlap_by_peak_time_{}_{}.{}'.format(settings_name,
                                                              tca_rank, plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi,bbox_inches="tight")



# 6. --- PLOT FACTOR METRICS SPLIT BY TIME -------------------------------------

df_sel = df.copy()

split_labels = ['Peak\nbetween\n{} and {} s'.format(k[0], k[1]) for k in split_intervals]

time_split = []
for t in df_sel['peak_time']:
    if (t > split_intervals[0][0]) & (t <= split_intervals[0][1]):
        time_split.append(0)
    elif (t > split_intervals[1][0]) & (t <= split_intervals[1][1]) :
        time_split.append(1)
    else:
        time_split.append(2)
df_sel['time_split'] = time_split

df_sel = df_sel[np.isin(df_sel['time_split'], [0, 1])]

# df_sel still has, for every factor, one row per target (all of course
# with the same factor metric values), so we reduce the dataframe to 1 row
# per factor with groupby

df_sel = df_sel.groupby(['animal_id', 'session_id', 'area',
                         'time_split', 'factor']).mean().reset_index()

for metric in ['perc_nonzero_units', 'kurtosis']:
    f, ax = plt.subplots(1, 1, figsize=[small_square_side, small_square_side])
    sns.barplot(data=df_sel, x='time_split', y=metric, ax=ax,
                hue='area', hue_order=AREAS, palette=[area_palette[a] for a in AREAS])
    ax.set_xlabel('')
    ax.set_ylabel(factor_metric_labels[metric])
    ax.set_xticklabels(split_labels)
    ax.get_legend().remove()
    sns.despine()
    plt.tight_layout()


# 7. --- PLOT DISCRIMINATION SCORE SPLITTING COMPONENTS BY TIME ----------------

df1 = df[(df['peak_time'] > split_intervals[0][0]) & (df['peak_time'] <= split_intervals[0][1])]
df2 = df[(df['peak_time'] > split_intervals[1][0]) & (df['peak_time'] <= split_intervals[1][1])]

f, ax = plt.subplots(1, 2, figsize=[3*small_square_side, 1.5*small_square_side],
                     sharex=True, sharey=True)
for i, dfx in enumerate([df1, df2]):
    sns.barplot(data=dfx, x='target', y='score', ax=ax[i],
                order=target_names, hue='area', hue_order=AREAS,
                palette=[area_palette[k] for k in AREAS])
    lab = [targets_labels[k._text].replace(" ","\n") for k in ax[i].get_xticklabels()]
    ax[i].set_xticklabels(lab, rotation=40)
    ax[i].set_xlabel('')
    ax[i].set_ylabel('')
    if discrimination_score == 'auc':
        ax[i].set_ylim([0.5, ax[i].get_ylim()[1]])
    ax[i].get_legend().remove()
ax[1].legend(bbox_to_anchor=(1.04,0.5), loc="center left", borderaxespad=0,
             frameon=False)
ax[0].set_ylabel('Discrimination score')
ax[0].set_title('Components with peak\nbetween {} and {} s'.format(split_intervals[0][0],
                                                                   split_intervals[0][1]))
ax[1].set_title('Components with peak\nbetween {} and {} s'.format(split_intervals[1][0],
                                                                   split_intervals[1][1]))
sns.despine()
plt.tight_layout(rect=[0,0,0.87,1])

plot_name = 'TCA_components_split_{}s_{}_rank_{}.{}'.format(split_intervals[0][1],
                                                            discrimination_score, tca_rank,
                                                            plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi,
            bbox_inches="tight")



# 8. --- PAIRPLOT OF UNIT FACTOR DISTRIBUTION AND DISCRIMINATION SCORES --------
# TODO: this breaks
if False:
    # eliminate target scores that we don't want in the plot
    df_sel = df.drop_duplicates(subset=['animal_id', 'session_id', 'area', 'factor'])

    for target in target_names:
        df_alt = df[df['target'] == target]
        for col in ['session_id', 'animal_id', 'area']:
            np.testing.assert_array_equal(df_sel[col], df_alt[col])

    vars = []
    for target in ['lick', 'mod', 'stim']:
        df_alt = df[df['target'] == target]
        df_sel['score_{}'.format(target)] = df_alt['score'].as_matrix()
        vars.append('score_{}'.format(target))
    vars = vars + ['peak_time', 'kurtosis', 'perc_nonzero_units']

    target_and_scores_labels = {'score_rew' : 'Discrimination score\n(reward)',
              'score_mod'  : 'Discrimination score\n(modality)',
              'score_lick'  : 'Discrimination score\n(lick)',
              'score_side'  : 'Discrimination score\n(side)',
              'score_X_stim' : 'Discrimination score\n(orientation)',
              'score_Y_stim': 'Discrimination score\n(frequency)',
              'score_X_stim_ch' : 'Discrimination score\n(orientation change)',
              'score_Y_stim_ch': 'Discrimination score\n(frequency change)',
              'peak_time' : 'Peak time of\ntime factor',
              'kurtosis'  : 'Kurtosis of\nunit factor',
              'perc_nonzero_units' : '% of nonzero entries\nin unit factor'}

    g = sns.pairplot(df_sel, vars=vars, hue='area', hue_order=AREAS,
                 palette=[area_palette[a] for a in AREAS], kind='reg',
                 plot_kws=dict(scatter_kws=dict(edgecolor='w')))

    for i in range(g.axes.shape[0]):
        g.axes[i, 0].set_ylabel(target_and_scores_labels[vars[i]])
        g.axes[-1, i].set_xlabel(target_and_scores_labels[vars[i]])

    # for i in range(g.axes.shape[0]-1):
    #     for j in range(g.axes.shape[0]-1):
    #         g.axes[i, j].set_xlim([0, 1])
    #         g.axes[i, j].set_ylim([0, 1])
    # for i in range(g.axes.shape[0]):
    #     g.axes[-1, i].set_ylim(-time_before_stim_in_s, time_after_stim_in_s)
    #     g.axes[i, -1].set_xlim(-time_before_stim_in_s, time_after_stim_in_s)
    plt.tight_layout(rect=(0, 0, 0.94, 1))

    plot_name = 'TCA_pairplot_discrimination_vs_factor_properties_' \
                'settings_{}_rank_{}.{}'.format(settings_name, tca_rank, plot_format)
    g.savefig(os.path.join(plot_folder, plot_name), dpi=dpi,bbox_inches="tight")


# 9. --- PARIPLOT OF FACTORS METRICS -------------------------------------------
df_sel = df.drop_duplicates(subset=['animal_id', 'session_id', 'area', 'factor'])

vars = ['kurtosis', 'perc_nonzero_units', 'peak_time']
g = sns.pairplot(df_sel, vars=vars, hue='area', hue_order=AREAS,
             palette=[area_palette[a] for a in AREAS], kind='reg',
             plot_kws=dict(scatter_kws=dict(edgecolor='w')))
for i in range(g.axes.shape[0]):
    g.axes[i, 0].set_ylabel(factor_metric_labels[vars[i]])
    g.axes[-1, i].set_xlabel(factor_metric_labels[vars[i]])
plt.tight_layout(rect=(0, 0, 0.94, 1))

plot_name = 'TCA_pairplot_unit_factors_vs_peak_' \
            'time_settings_{}_rank_{}.{}'.format(settings_name, tca_rank, plot_format)
g.savefig(os.path.join(plot_folder, plot_name), dpi=dpi,bbox_inches="tight")



# 10. --- JOINTPLOT OF PEAK TIME VS DISCRIMINATION SCORE -----------------------

df_sel = df.copy()
#do any preselction here

bw = 0.05
for target in target_names:
    g = sns.jointplot(x=[], y=[], height=small_square_side*1.5)

    for i, area in enumerate(AREAS):
        dfx = df_sel[(df_sel['target'] == target) & (df_sel['area'] == area)]
        g.ax_joint.scatter(dfx['peak_time'], dfx['score'],
                           color=area_palette[area], s=40, edgecolor='w')


    xlim = (-time_before_stim_in_s, time_after_stim_in_s)
    g.ax_joint.set_xlim(xlim)
    ylim = g.ax_joint.get_ylim()

    for i, area in enumerate(AREAS):
        dfx = df_sel[(df_sel['target'] == target) & (df_sel['area'] == area)]
        sns.kdeplot(ax=g.ax_marg_x, data=dfx['peak_time'], bw=bw,
                    color=area_palette[area])
        sns.kdeplot(ax=g.ax_marg_y, data=dfx['score'], bw=bw,
                    color=area_palette[area], vertical=True)
        g.ax_marg_x.get_legend().remove()
        g.ax_marg_y.get_legend().remove()
        g.ax_marg_x.set_xlim(xlim)
        g.ax_marg_y.set_ylim(ylim)

    g.set_axis_labels('Peak time', 'Discrimination score\n({})'.format(targets_labels[target]))
    g.ax_joint.axvline(0, c='grey', ls='--')
    g.ax_joint.axhline(0.5, c='grey', ls=':')
    plt.tight_layout()

    plot_name = 'TCA_components_peak_vs_discr_{}_{}_rank_{}.{}'.format(target,
                                                             discrimination_score, tca_rank,
                                                             plot_format)
    g.savefig(os.path.join(plot_folder, plot_name), dpi=dpi,
              bbox_inches="tight")



# 11. --- PLOT FACTOR LAMBDAS BY TARGET / AREA --------------------------------

labels = [targets_labels[t].replace(" ", "\n") for t in target_names]

df_sel = df[np.isin(df['target'], target_names)]
# pick the best out of the ones we want
df_sel = df_sel.loc[df_sel.groupby(['animal_id', 'session_id',
                                    'area', 'factor'])['score'].idxmax()]
print(df_sel.groupby(['area'])['target'].value_counts())
f, ax = plt.subplots(1, 3, figsize=[12, 4])

for i, area in enumerate(AREAS):
    sns.barplot(data=df_sel[df_sel['area'] == area], x='target', y='lambda', ax=ax[i],
                order=target_names, color=area_palette[area])
    ax[i].set_xticklabels(labels, rotation=90)
    ax[i].set_xlabel('')
    ax[i].set_ylabel('')
ax[0].set_ylabel('Factor importance')
sns.despine()
plt.tight_layout()

plot_name = 'TCA_factor_importance_{}_{}_{}.{}'.format(
             settings_name, discrimination_score, tca_rank, plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi, bbox_inches="tight")


# 12. --- PLOT FACTOR WEIGHTED LAMBDAS BY TARGET / AREA -----------------------

df_sel = df.copy()

labels = [targets_labels[t].replace(" ", "\n") for t in target_names]

df_sel['weighted_lambda'] = df_sel['lambda'] * df_sel['score']
# pick the best out of the ones we want

f, ax = plt.subplots(1, 3, figsize=[12, 4])

for i, area in enumerate(AREAS):
    sns.barplot(data=df_sel[df_sel['area'] == area], x='target', y='weighted_lambda', ax=ax[i],
                order=target_names, color=area_palette[area])
    ax[i].set_xticklabels(labels, rotation=90)
    ax[i].set_xlabel('')
    ax[i].set_ylabel('')
    #ax[i].set_ylim([0.10, ax[i].get_ylim()[1]])

ax[0].set_ylabel('Weighted factor importance')
sns.despine()
plt.tight_layout()

plot_name = 'TCA_weighted_factor_importance_{}_{}_{}.{}'.format(
             settings_name, discrimination_score, tca_rank, plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi, bbox_inches="tight")


# --- PLOT THINGS AS A LINE OVER TIME ------------------------------------------
# with seaborn lineplot you can double check that we are getting the same
# results but we cannot smooth, so we have to interpolate manually
# df_sel = df.copy()
# f, ax = plt.subplots(1, 1, figsize=[4, 4])
# sns.lineplot(data=df_sel[df_sel['target']=='Y_stim'], x='peak_time', y='score',
#              hue='area', ax=ax, hue_order=AREAS, palette=[area_palette[k] for k in AREAS])
# plt.tight_layout()


# 13. --- DISCRIMINATION SCORE BY PEAK TIME LINE PLOT -------------------------

df_sel = df.copy()

f, axes = plt.subplots(2, 4, figsize=[10, 3], sharex=False, sharey=True)

for i, target in enumerate(target_names):
    ax = axes.flatten()[i]
    for area in AREAS:
        dfx = df_sel.loc[(df_sel['target']==target) & (df_sel['area'] == area)]
        dfx = dfx.sort_values('peak_time')
        xp = dfx['peak_time']
        fp = dfx['score']
        times = np.arange(xp.min(), xp.max(), dt)
        interp_score = np.interp(times, xp=xp, fp=fp)
        smooth_score = gaussian_filter1d(interp_score, sigma=20)
        ax.plot(times, smooth_score, c=area_palette[area])
        ax.set_xlim(-time_before_stim_in_s, time_after_stim_in_s)
    ax.set_title('{}'.format(targets_labels[target]))
    ax.set_xlabel('Time (s)')
    ax.title.set_size(10)
axes[0, 0].set_ylabel('Discrimination\nscore')
axes[1, 0].set_ylabel('Discrimination\nscore')
sns.despine()
plt.tight_layout()

plot_name = 'TCA_factor_disc_scores_by_peak_time_lineplot_{}_{}.{}'.format(discrimination_score, tca_rank, plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi, bbox_inches="tight")

# 14. --- FACTOR METRICS BY PEAK TIME LINE PLOT -------------------------------

df_sel = df.copy()

vars = ['kurtosis', 'perc_nonzero_units', 'lambda']
f, axes = plt.subplots(1, 3, figsize=[10, 2])
for i, var in enumerate(vars):
    ax = axes.flatten()[i]
    for area in AREAS:
        dfx = df_sel.loc[(df_sel['target']=='rew') & (df_sel['area'] == area)]
        dfx = dfx.sort_values('peak_time')
        xp = dfx['peak_time']
        fp = dfx[var]
        times = np.arange(xp.min(), xp.max(), dt)
        interp_score = np.interp(times, xp=xp, fp=fp)
        smooth_score = gaussian_filter1d(interp_score, sigma=smoothing_sigma)
        ax.plot(times, smooth_score, c=area_palette[area])
        ax.set_xlim(-time_before_stim_in_s, time_after_stim_in_s)
    ax.set_ylabel('{}'.format(factor_metric_labels[var]))
    ax.set_xlabel('Time (s)')
sns.despine()
plt.tight_layout()

plot_name = 'TCA_factor_metrics_by_peak_time_lineplot_{}.{}'.format(tca_rank, plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi, bbox_inches="tight")



# 15. --- AVERAGE WEIGHTED UNIT SCORE BY TARGET BY LAYER ----------------------

dfu_sel = dfu.copy()

f, axes = plt.subplots(1, len(AREAS), figsize=[10, 4], sharex=True, sharey=True)

for i, area in enumerate(AREAS):
    dfu_area = dfu_sel[(dfu_sel['area'] == area)]
    ax = axes[i]

    sns.barplot(data=dfu_area, x='target', y='score', hue='layer',
                order=target_names, hue_order=LAYERS,
                palette=[layer_palette[l] for l in LAYERS], ax=ax)

    labels = [targets_labels[t].replace(" ", "\n") for t in target_names]
    ax.set_xlabel('')
    ax.set_xticklabels(labels, rotation=90)
    ax.set_title(area)
    ax.legend().remove()
axes[0].set_ylabel('Unit discrimination score')

sns.despine()
axes[-1].legend(bbox_to_anchor=(1.04,0.5), loc="center left", borderaxespad=0,
                 frameon=False)
plt.tight_layout()
plt.tight_layout(rect=(0, 0, 0.9, 1))

plot_name = 'TCA_unit_score_by_layer_{}_{}_{}.{}'.format(settings_name, discrimination_score, tca_rank, plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi, bbox_inches="tight")



# 16. --- UNIT FACTORS BY DEPTH FOR EARLY AND LATE-PEAKING COMPONENTS ---------

"""
For every factor, we see what its peak time is. If it's early, we take its 
unit factors, together with the unit depths, and put them in a dataframe with
the label 'early'. This allows us to later select only unit factors for the 
early components, and bin their unit factors by ranges of depth. 
"""

depths = []
factors = []
bumps = []

for (animal_id, session_id, area_spikes) in rs.keys():
    print(animal_id, session_id, area_spikes)

    key = (animal_id, session_id, area_spikes)
    U = rs[key]['factors']
    time_bin_times = rs[key]['time_bin_times']
    unit_ids = rs[key]['selected_unit_id']
    unit_depth = rs[key]['unit_depth']
    unit_layer = rs[key]['unit_layer']

    unit_factors = U.factors[0]
    time_factors = U.factors[1]
    trial_factors = U.factors[2]
    trial_df = rs[key]['trial_df']

    for rnk in range(tca_rank):
        peak_time = time_bin_times[time_factors[:, rnk].argmax()]

        if 0 < peak_time <= 0.22:
            depths.append(unit_depth)
            factors.append(unit_factors[:, rnk])
            #factors.append(unit_factors[:, rnk] / unit_factors.sum(axis=1))
            bumps.append(['early']*len(unit_depth))
        elif 0.22 < peak_time <= 0.5:
            depths.append(unit_depth)
            #factors.append(unit_factors[:, rnk])
            factors.append(unit_factors[:, rnk] / unit_factors.sum(axis=1))
            bumps.append(['late']*len(unit_depth))

print(len(depths))
print(len(bumps))

de = pd.DataFrame(columns = ['depth', 'factors', 'bump'])
de['depth'] = np.hstack(depths)
de['factors'] = np.hstack(factors)
de['bump'] = np.hstack(bumps)

linestyles = ['--', '-']

f, ax = plt.subplots(1, 1, figsize=[4, 3])

for i, peak_time in enumerate(['early', 'late']):
    dxx = de[de['bump'] == peak_time]
    dxx = dxx.groupby(pd.cut(dxx["depth"], np.arange(0, 1000, 50))).mean()
    ax.plot(dxx['depth'], dxx['factors'], c='k', ls=linestyles[i],
            label='{} components'.format(peak_time.capitalize()))
ax.legend()
ax.set_ylabel('Average participation\nin components')
ax.set_xlabel('Cortical depth')
sns.despine()
plt.tight_layout()
plot_name = 'TCA_unit_scores_by_depth_early_vs_late_{}_{}.{}'.format(
             settings_name, tca_rank, plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi, bbox_inches="tight")


# 17. --- UNIT FACTORS BY DEPTH FOR COMPONENTS SPLIT BY TARGET ----------------

for area in AREAS:
    df_sel = df.copy()
    df_sel = df_sel[df_sel['area'] == area]
    df_sel = df_sel.loc[df_sel.groupby(['animal_id', 'session_id',
                                        'area', 'factor'])['score'].idxmax()]

    depths = []
    factors = []
    targets = []

    for i, row in df_sel.iterrows():

        unit_ids = row['unit_ids']
        unit_depth = row['unit_depth']
        unit_layer = row['unit_layer']
        unit_factors = row['unit_factors']
        area = row['area']
        target = row['target']
        score = row['score']

        depths.append(unit_depth)
        factors.append(unit_factors)
        targets.append([target]*len(unit_depth))

    print(len(depths))
    #print(len(bumps))

    dt = pd.DataFrame(columns = ['depth', 'factors', 'target'])
    dt['depth'] = np.hstack(depths)
    dt['factors'] = np.hstack(factors)
    dt['target'] = np.hstack(targets)


    #target_names_sub = ['lick', 'rew', 'mod']
    f, ax = plt.subplots(1, 1, figsize=[6, 3])

    for i, target_name in enumerate(target_names):
        dxx = dt[dt['target'] == target_name]
        dxx = dxx.groupby(pd.cut(dxx["depth"], np.arange(0, 1000, 50))).mean()
        xp = dxx['depth']
        fp = dxx['factors']
        # ax.plot(xp, fp, c=targets_palette[target_name],
        #         label='{}'.format(targets_labels[target_name]))
        depths = np.arange(0, 1000, 10)
        interp_score = np.interp(depths, xp=xp, fp=fp)
        smooth_score = gaussian_filter1d(interp_score, sigma=4)
        ax.plot(depths, smooth_score, c=targets_palette[target_name],
                label='{}'.format(targets_labels[target_name]))

    ax.legend(bbox_to_anchor=(1.04,0.5), loc="center left", borderaxespad=0,
              frameon=False)
    ax.set_ylabel('Average participation\nin components')
    ax.set_xlabel('Cortical depth')
    sns.despine()
    plt.tight_layout(rect=(0, 0, 0.7, 1))
    ax.set_title(area)

    plot_name = 'TCA_unit_scores_by_depth_by_target_{}_{}_{}_{}.{}'.format(area,
                 discrimination_score, settings_name, tca_rank, plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi, bbox_inches="tight")



# 18. --- MEDIAN DEPTH OF COMPONENT BY PREFERRED TARGET -----------------------

for area in AREAS:
    df_sel = df[np.isin(df['target'], target_names)]
    df_sel = df_sel[df_sel['area'] == area]

    # pick the best out of the ones we want
    df_sel = df_sel.loc[df_sel.groupby(['animal_id', 'session_id',
             'area', 'factor'])['score'].idxmax()].sort_values(['session_id', 'area', 'factor'])


    f, ax = plt.subplots(1, 1, figsize=[4, 5])
    plt.gca().invert_yaxis()

    ax.set_title(area)
    for target_name in target_names:
        dfx = df_sel[df_sel['target'] == target_name]

        median_depths = []
        for i, row in dfx.iterrows():
            mask = row['unit_factors'] > 0
            depths = row['unit_depth'][mask]
            median_depths.append(np.median(depths))

        # sns.kdeplot(median_depths,  c=targets_palette[target_name], clip=[0, 1300],
        #             ax=ax, vertical=True)
        sns.distplot(median_depths, kde=True, rug=True, hist=False,
                     kde_kws={'clip' : [0, 1300]}, color=targets_palette[target_name],
                     vertical=True)

    custom_lines = [Line2D([0], [0], color=targets_palette[t], lw=4) for t in target_names]
    labels = [targets_labels[k].replace(' ', '\n') for k in
              target_names]
    ax.legend(custom_lines, labels,
              bbox_to_anchor=(1.04, 0.5), loc="center left", borderaxespad=0,
              frameon=False)

    ax.set_ylim([1400, 0])
    ax.set_ylabel('Depth (μm)')
    ax.set_xlabel('Density')
    sns.despine()
    plt.tight_layout(rect=(0, 0, 0.7, 1))

    plot_name = 'TCA_factor_median_depth_by_preferred_{}_{}_{}_{}.{}'.format(
                area, discrimination_score, settings_name, tca_rank, plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi, bbox_inches="tight")



# 18. --- MEDIAN DEPTH OF COMPONENT BY TIME SPLIT -----------------------------

time_splits = [[0, 0.22], [0.22, 0.5]]

time_palette = [sns.xkcd_rgb['teal'], sns.xkcd_rgb['pastel orange']]

for area in AREAS:
    df_sel = df[np.isin(df['target'], target_names)]
    df_sel = df_sel[df_sel['area'] == area]

    # pick the best out of the ones we want
    df_sel = df_sel.loc[df_sel.groupby(['animal_id', 'session_id',
             'area', 'factor'])['score'].idxmax()].sort_values(['session_id', 'area', 'factor'])


    f, ax = plt.subplots(1, 1, figsize=[4, 5])
    plt.gca().invert_yaxis()

    ax.set_title(area)
    for kk, time_split in enumerate(time_splits):
        dfx = df_sel[(df_sel['peak_time'] >= time_split[0]) &
                     (df_sel['peak_time'] < time_split[1])]

        median_depths = []
        for i, row in dfx.iterrows():
            mask = row['unit_factors'] > 0
            depths = row['unit_depth'][mask]
            median_depths.append(np.median(depths))

        # sns.kdeplot(median_depths,  c=targets_palette[target_name], clip=[0, 1300],
        #             ax=ax, vertical=True)
        sns.distplot(median_depths, kde=True, rug=True, hist=False,
                     kde_kws={'clip' : [0, 1300]}, color=time_palette[kk],
                     vertical=True)

    custom_lines = [Line2D([0], [0], color=c, lw=4) for c in time_palette]
    labels = ['Early\ncomponents', 'Late\ncomponents']
    ax.legend(custom_lines, labels,
              bbox_to_anchor=(1.04, 0.5), loc="center left", borderaxespad=0,
              frameon=False)

    ax.set_ylim([1400, 0])
    ax.set_ylabel('Depth (μm)')
    ax.set_xlabel('Density')
    sns.despine()
    plt.tight_layout(rect=(0, 0, 0.7, 1))

    plot_name = 'TCA_factor_median_depth_by_time_split_{}_{}_{}_{}.{}'.format(
                area, discrimination_score, settings_name, tca_rank, plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi, bbox_inches="tight")