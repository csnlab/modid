# IDEAS
- idea: see if decoding error are correlated in A1 V1, that is, does audio
information in V1 correspond to A1 information? You could train two decoders
on the same subset of correct trials, decode audio frequency in both areas,
see if the errors are correlated. For this, we can use the code almost as is,
except that we need to save not only true and predicted value for all trials
and all reps of cross validation (which we already do) but an additional
array containing the trial ids, so that we later load the two decoding from
A1 and V1 and match by trial id.
- Deep learning on LFP
- Encoding with many predictors





# TODO

0. Add an option to select the dataset that you want to use

0. overview of temporal coding analysis so far

0. Characterize inter-area relationships with TCA
- Find a metric of co-presence of two areas in a component

0. Square root firing rate to stabilize variance?

0. Plot results of decoding with phase where we picked really small
time bins (25 and 50 ms), and we decode all targets in all areas. The
settings are june28phase and june28phase2.

1. Two sessions are being manually excluded in dec_standalone, why?

0. Filtering takes forever in session 2003 2018-02-09_14-30-19:
Figure out why, potentially downsample more, cause otherwise this
will take weeks.

0. [IDEA]: do decoding after shifting/warping?

1. [QUESTION]: in warping, could it be that neurons which fire more spikes
dominate the reconstruction error?

3. Run TCA with fit ensemble for all sessions to get an idea of a
reasonable amount of components.
[Question]: do I then pick one number for all sessions, or do I set up a way
to determine per session what is the optimal number? Probably it would
be best, since sessions can have quite different numbers of units, to
allow different ranks, determined individually. If I find a way to
determine the number of components from the ensemble, then in
compute_tca.py I could have tca_rank = 'automatic' which chooses the
right rank per session. First of all: run with ensemble and plot the
error/similarity for all sessions, because if they are all similar we
can also just pick one rank and go with it. [update]: sessions
look quite similar.
[ISSUE] running on mac, if fit ensemble is set to true, code gets
stuck at some point (always the same). Partially solved if you only fit
lower rank. [RUNNING NOW]: up to 12 components should work hopefully.
No doesn't work [ISSUE ON GITHUB]

5. Discuss the decoding with Matthijs/Cyriel
- Present results
- How to select the channel to use?
> Check if the choice of channel can make a big difference in the decoding,
run a decoding experiment for one area with a few different channels
and compare.

6. In the decoding, we do improvement over shuffled, so that should
partially take care of the imbalance, but should we also add the
balanced_accuracy_score instead of normal accuracy?

7. Also think whether it may be a good idea to consider the overlap
not only for the best discriminated target, because sometimes you have
components which pick out more than one thing, so you could have a
an overlap weighted by the discrimination score of each target.


13. Z-score LFP spectrogram with a window (for every frequency you
compute the average power and standard deviation of the power, over
a given window, and then use that to normalize the data).

14. Random forest is initialized randomly now, it should have its own
random state

15. Add method to generate spectrograms. We essentially have all this
code in spectrogram_overview.py, but it would be nice for it to be a
method of session to which you pass a list of trials and a list of
channels and it makes the spectrograms with the desired method.

16. Interneurons vs pyramidal neurons?

17. In the TCA components meta analysis, add a measure to identify a
 component which only appears over time (drift component).


# LOW PRIORITY

1. When you prepare interpolated lfp data, you could get the bin centers
from the spike binning without actually binning the spikes, which will
save some time

2. As in a previous study, we set the starting point of the first bin to
the preferred phase of firing for each neuron. This makes the binning a bit
complicated though!

3. CIRCULAR INTERPOLATION OF PHASE
the phase should be interpolated in a way that take care of the
periodicity. This is only essential if we do information theoretic stuff
where we need the bins, so it's paused for now.

4. Look into detrending the data?

5. In tca_compute_and_factor_plot, the targets palette has more elements
than are actually plotted in the discrimination barplot

6. Add function to session called get_within_trial_spike_bin_times
which gives you the times of the centers of the bins relative to the
event. This allows you to get rid of the few lines of code for example
in the decoding scripts.

7. Include jean's data in the analysis?

8. Decoding on naive animals

10. Idea: venn diagram of all units indicating that it belong to a
component that discriminates modality and one that discriminates response

12. Dendrogram - half done



# DONE
- Include left vs right response
- Align to first lick instead of stimulus change
- Distribution of unit weights
- Overlap of unit factors
- Discrimination of trial factors
- Split components based on time
- Look at decoding of correct/side/stimulus id/stimulus change/modality
in the first 200 and second 200 ms
- Scatter plot of neurons first bump vs second bump (average with error bars)
- Idea: V1 is performing a transformation whereby complex sensory
information is reduced into a simple go-no go signal.
- Estimate dimensionality over time (how many PCA components) do I need
to explain 90% of variance over time?
- Overlap of unit factors across components
based on preferred discrimination and peak time
- Restrict meta analysis to meaningful components, remove components based on
1. number of units which have nonzero weight
2. number trials which have nonzero weight
3. variation in the time factor
4. discrimination score for at least one contrast must be above a threshold
- PCA component normalized by number of units
- overlap of components between separate audio and visual
- overlap of components split by area
- split component 0-220ms 220-500ms
- Plot discrimination scores overall
- Duplicate unit indices - check if it is fixed: added
an assert in session class
- Include probe trials in TCA, with coloring based on presence
of any response (noResponse column)
- Add method to plot impulse response of the filter
- Session 2009 2018-08-24_11-56-35 visualOriPostNorm is wrong: we
exclude it in session selection but only the visual part
- In compute_dec_standalone you should have the possibility to set the
targets, which are now set automatically when you get the sessions
- Like in TCA where we added the lick-nolick separation, we should
add that as a target in the decoding!
[NOTE]: for the decoding of Lick, we use also probe trials. Is that good?
Otherwise, we need to change the code in session.get_session_overview_trials
where we compute the number of trials, and in compute_dec_standalone.py,
where we determine which trials to use. [Improvement]: the trial_type
necessary for the target is now determined exclusively in
session.get_session_overview_trials, and compute_dec_standalone.py reads
it from there. some lines kept for testing need to be removed.
- Idea: the decoding gives you for every target a set of unit scores.
You can consider these scores analogous to the unit factors of TCA and
build a heatmap to see if the units which decode best e.g. orientation
overlap with those which decode best e.g. response.
To get a single score per unit, I first average across repeats then
I take the maximum over time.
- In load_dec_combined.py, we could add a plot in which we aggregate
all decoding by input, regardless of animal/session
- In load_dec_single_sess.py and maybe also in load_dec_combined.py,
we could also not force the user to specify target and areas,
and if they are set to None, plot everything
- In load_tca_aggregate.py, we should compute the overlap with the
cosine_similarity function from sklearn, so we do it in the exact same
way for decoding and components (although probably we are already
doing it in the same way). [edit:] we are doing it in the same way,
the cosine similarity is the same as if we first normalize to unit
vectors then take the dot product.
- [BUG] in load_tca_aggregate.py, if I want to make the overlap
heatmaps with separate audio and visual stim/stim ch, I get a lot of
white combinations, even when I am filtering.
if you look at df, it seems like the score for X_stim and Y_stim is
not computed for all components! strange!
[FIXED] we were missing a for loop to loop over components where
we compute the discrimination scores, so it was being computed only for
the last component
- In load_tca_aggregate.py, plot factor parameters (kurtosis,
perc_nonzero_units, etc.) in barplot divided by preferred target.
- Variance explained by factors? [asked]
[update] you can now run factors.factor_lams() to get the size of each
factor, which gives an idea of its relative importance (more interpretable)
for nonnegative decomposition. -> Computed and plotted now in load_tca_aggregate.py
- [BUG] Overlap by peak time is very diagonal, something's wrong. Seems
to be due to a bug in the loop where we compute the discrimination scores,
where we were looping twice over the components. Results now look ok.
- In compute_tca.py save parameters. Done but needs to happen
also in load_tca_aggregate.py
- Read again the main loops in load_tca_aggregate.py
- Why do we compute the average of the nonzero unit factors in
load_tca_aggregate.py? Not sure but we keep it anyways
- load_tca_aggregate.py: each subsection where we generate plots should
begin by defining its own df_sel from the main df. [BUG] plot 8. now is
breaking. [solved]
- When I plot factor metrics split by time I am discretizing the split,
but I could also think of a way to interpolate and plot metrics as a line,
so you would see for instance kurtosis as it evolves over time. Attempted
at the end of the load_tca_aggregate.py, does not look great so far,
would probably need some smoothing. [UPDATE]: could look good to plot
the score for the different targets over time, but we would have to
smooth somehow. [DONE]: interpolated manually and smoothed, looks great!
- For affinewarp, rather than the binned spike count, what I want is the bin
that each spike falls in, so we keep all the spikes, and we allow for multiple
spikes to fall in the same bin. time_bin_times does not need to change.
Fixed by using the binning of SpikeData by affinewarp
- For the warping, does the event_transform method support the case
for which you have multiple events per trial? So far only working with
one event per trial. No, not really, for the shift only case it would
be easy to adapt, but for the other warping models no.
- [BUG] in the decoding standalone we end up with an area_spikes
which is different from area_lfp, which should not happen, and raises
a value error. Why? [FIXED] it was because if area_lfp was 'same_as_spikes'
it would get set to 'V1' and then would stay V1 cause at the next loop
it would not be 'same_as_spikes' anymore
- visualOriPostNorm has become visualOriPostChangeNorm and same for audio
- tca_compute_and_factor_plot seems broken with the new data
- Figure out a way to only compute unit scores
- Scatterplot of auc scores of audio vs freq (identity or change)
to validate the TCA result that in V1 audio and freq are less coupled
than in PPC or CG1.
- [IDEA]: We have that nice scatter+kde plot of discrimination score
versus peak time, but we can't show one per target, and it's less easy
to read. We could still show one, and then show all the lineplots,
which is a more compact representation of the same thing.
- TCA with layer/depth information for the units
[QUESTION]: can I use ChannelY across sessions? Yes
- Decoding as function of depth [wait to see if we have enough data]
-- In decoding_standalone: add a decode_by_layer flag
-- if decode_by_layer flag is True, the call to select_sessions has
to use the min_units_per_layer kwarg instead of min_units.
-- The definition of the file name has to be changed so that it includes the layer
(and says all_layers or something if decode_by_layer is False)
-- Do we exclude/include the NA units? Probably exclude
- in load_tca_aggregate not all the plots we save the discrimination score name
that we used
- Do discrimination separately for audio and visual in tca factor plot?


# DONE MATTHIJS
1. Add channel depth in the spike data, so we don't have to load the LFP.
2. Channel of units is with respect to area, not global! IDs have the
animal and session, but then remaining integer part is sometimes
duplicate (sometimes not). I think the channel_ID string should identify
a channel in a unique way.
4. Separate LFP and LFP attributes in the data files. This is to have
a way to load the LFP metadata really quickly (session could have
an argument like load_lfp_metadata). This just requires creating a copy
of the lfp data file without the actual signal.
