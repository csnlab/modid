import numpy as np
from matplotlib import pyplot as plt
from session import Session
from mne import create_info, EpochsArray
from plotting_style import *
from constants import *
import pandas as pd
from session_info import select_sessions
from mne.baseline import rescale
from mne.time_frequency import (tfr_multitaper, tfr_stockwell, tfr_morlet,
                                tfr_array_morlet, tfr_array_stockwell)


"""
Thanks to: https://martinos.org/mne/stable/auto_examples/time_frequency
/plot_time_frequency_simulated.html
"""

# select animal and session (if only the animal is specified, run for all
# sessions of that animal), if both are not, run for all available sessions
animal_id = '2009'
session_id = '2018-08-22_14-05-50'

# SELECT CHANNELS
area_lfp = 'V1'
ch_in_the_middle_of_the_probe = True
binsize_in_ms = 20

time_before_stim_in_s = 1.5
time_after_stim_in_s = 3
trial_types = ['X', 'Y']
only_correct_trials = False
layers = ['IG', 'SG']
freqs = np.arange(1., 80., 3.)
fmin, fmax = freqs[[0, -1]]
width = 0.5
vmin, vmax = None, None
tfr_method = 'morlet' # 'stockwell or morlet
baseline_correction_mode = 'percent'
baseline_times = (0, 1)
plot_times = (0.5, 4)
chop_t_on_sides = 0.5
average_over_channels = True

plot_format = 'png'
dpi = 400
plot_folder = os.path.join(DATA_FOLDER,  'plots', 'spectrograms_bigchange')

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder)



sessions = select_sessions(min_trials_per_class=0,
                           only_correct=False)
sessions = sessions.drop_duplicates(subset=['animal_id', 'session_id'])


if animal_id is not None:
    sessions = sessions[sessions['animal_id'] == animal_id]

if session_id is not None:
    sessions = sessions[sessions['session_id'] == session_id]

np.random.seed(43)

for i, row in sessions.iterrows():

    animal_id = row['animal_id']
    session_id = row['session_id']

    session = Session(animal_id=animal_id, session_id=session_id)
    session.load_data(load_spikes=False)

    power_matrices = {tri : {lay : None for lay in layers} for tri in trial_types}

    for trial_type in trial_types:

        if trial_type == 'X':
            trial_numbers = session.select_trials(only_correct=only_correct_trials,
                                              trial_type=trial_type,
                                              visual_change=2)
        elif trial_type == 'Y':
            trial_numbers = session.select_trials(only_correct=only_correct_trials,
                                              trial_type=trial_type,
                                              auditory_change=2)
        trial_times = session.get_aligned_times(trial_numbers,
                                                time_before_in_s=time_before_stim_in_s,
                                                time_after_in_s=time_after_stim_in_s)

        for layer in layers:

            if average_over_channels:
                channel_ids = session.select_channels(area=area_lfp, layer=layer,
                                        in_the_middle=ch_in_the_middle_of_the_probe)

            else:
                channel_ids = session.get_random_channel_id(area=area_lfp,
                                                            layer=layer,
                                                            in_the_middle=ch_in_the_middle_of_the_probe)
            if len(channel_ids) == 0:
                continue


            if isinstance(channel_ids, str):
                channel_ids = [channel_ids]

            data = session.prepare_lfp_for_mne(channel_ids, trial_times)

            ch_types = ['grad' for _ in channel_ids]
            info = create_info(ch_names=channel_ids, sfreq=session.sampling_rate,
                               ch_types=ch_types)
            epochs = EpochsArray(data=data, info=info)

            if tfr_method == 'stockwell':

                tfr = tfr_stockwell(epochs, fmin=fmin, fmax=fmax,
                                      width=width, return_itc=False)
                power = tfr.data
                freqs = tfr.freqs


            elif tfr_method == 'morlet':
                power = tfr_array_morlet(epochs.get_data(), sfreq=epochs.info['sfreq'],
                                 freqs=freqs, n_cycles=freqs / 2,
                                 output='avg_power')

            rescale(power, epochs.times, baseline_times,
                    mode=baseline_correction_mode, copy=False)

            power_matrices[trial_type][layer] = power

        # f = power.plot([0], baseline=baseline_times, mode=baseline_correction_mode,
        #                show=False, colorbar=True,
        #                tmin=plot_times[0], tmax=plot_times[1], fmin=fmin,
        #                fmax=fmax, vmin=vmin, vmax=vmax)


    power_list = []
    for j, trial_type in enumerate(trial_types):
        for k, layer in enumerate(layers):
            try:
                power_list.append(power_matrices[trial_type][layer].mean(axis=0))
            except AttributeError:
                pass

    if len(power_list) > 0:
        power_all = np.hstack(power_list)
        vmax = np.abs(power_all).max()
        vmin = -np.abs(power_all).max()

        # plot_times = np.arange(-time_before_stim_in_s, time_before_stim_in_s,
        #                        1.0/session.sampling_rate)
        # plot_times =

        times = epochs.times
        times = times - time_before_stim_in_s

        time_ind = np.logical_and((times >= -time_before_stim_in_s+chop_t_on_sides),
                                  (times < time_after_stim_in_s - chop_t_on_sides))
        times = times[time_ind]


        f, ax = plt.subplots(2, 2, sharex=True, sharey=True)

        for j, trial_type in enumerate(trial_types):
            for k, layer in enumerate(layers):
                try:
                    power = power_matrices[trial_type][layer]
                    power = power.mean(axis=0)
                    power = power[:, time_ind]
                    mesh = ax[k, j].pcolormesh(times, freqs, power,
                                         cmap='RdBu_r', vmin=vmin, vmax=vmax)
                    ax[k, j].set_title('{}, {}'.format(trial_type_strings[trial_type], layer))
                    ax[k, j].set(ylim=freqs[[0, -1]], xlabel='Time (s)')
                    #plt.colorbar(mesh)
                except AttributeError:
                    pass
        sns.despine(offset=8)
        plt.tight_layout()


        plot_name = '{}_{}_{}_spectrogram_{}_{}_corrected.{}'.format(animal_id, session_id,
                                                                  area_lfp, baseline_correction_mode,
                                                                  tfr_method,
                                                                  plot_format)
        plot_path = os.path.join(plot_folder, plot_name)
        f.savefig(plot_path, dpi=dpi)

