import scipy
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import matplotlib.cm
from session import Session
from mpl_toolkits.axes_grid1 import make_axes_locatable


# set animal_id and session_id to None if you want to run for all sessions
animal_id = '2012'
session_id = '2018-08-14_14-30-15'


# SELECT CHANNELS
area_lfp = 'V1'
lfp_channel_id = None
layer_lfp = None
ch_in_the_middle_of_the_probe = True
binsize_in_ms = 20
max_freq = 100

time_before_stim_in_s = 0.0
time_after_stim_in_s = 0.5

baseline_start_before_stim_in_s = 0.8
baseline_stop_before_stim_in_s = -0.3 # this needs to be negative because we add to stim time

session = Session(animal_id=animal_id, session_id=session_id)
session.load_data()

lfp_channel_id = session.get_random_channel_id(area=area_lfp,
                                               layer=layer_lfp,
                                               in_the_middle=ch_in_the_middle_of_the_probe)


# VISUAL TRIALS
trial_numbers = session.select_trials(only_correct=False, trial_type='X')
visual_trial_times = session.get_aligned_times(trial_numbers,
                                     time_before_in_s=time_before_stim_in_s,
                                     time_after_in_s=time_after_stim_in_s)

# AUDIO TRIALS
trial_numbers = session.select_trials(only_correct=False, trial_type='Y')
audio_trials_times = session.get_aligned_times(trial_numbers,
                                     time_before_in_s=time_before_stim_in_s,
                                     time_after_in_s=time_after_stim_in_s)

# BASELINES
trial_numbers = session.select_trials(only_correct=False)
baseline_times = session.get_aligned_times(trial_numbers,
                                     time_before_in_s=baseline_start_before_stim_in_s,
                                     time_after_in_s=baseline_stop_before_stim_in_s)

channel_ind = session.get_lfp_channel_index_from_channel_id(lfp_channel_id)
signal = session.get_lfp_signal_from_channel_index(channel_ind)



# TODO introduce slice by time function


f, ax = plt.subplots(1, 3, figsize=[7, 3], sharey=True, sharex=True)

for i, times in enumerate([baseline_times, visual_trial_times, audio_trials_times]):
    ax[i].set_xlim([0, max_freq])

    for time in times:
        trial_signal = signal[(session.lfp_times >= time[0]) &
                              (session.lfp_times < time[1])]
        spec, psd = scipy.signal.welch(trial_signal, session.sampling_rate,
                                       nperseg=250)

        ax[i].semilogy(spec, psd, 'k', linewidth=0.2)
ax[0].set_ylabel('power ($uV^{2}/Hz$)')
for axx in ax:
    axx.set_xlabel('frequency (Hz)')

ax[0].set_title('Baseline')
ax[1].set_title('Visual trials')
ax[2].set_title('Audio trials')
sns.despine()
plt.tight_layout()



# VISUAL TRIALS
trial_numbers = session.select_trials(only_correct=False, trial_type='X')
visual_trial_times = session.get_aligned_times(trial_numbers,
                                     time_before_in_s=time_before_stim_in_s,
                                     time_after_in_s=time_after_stim_in_s)
# VISUAL BASELINES
visual_baseline_times = session.get_aligned_times(trial_numbers,
                                     time_before_in_s=baseline_start_before_stim_in_s,
                                     time_after_in_s=baseline_stop_before_stim_in_s)



f, ax = plt.subplots(1, 1, figsize=[4, 3], sharey=True, sharex=True)

for trial, base in zip(visual_trial_times[10:20], visual_baseline_times[10:20]):
    trial_signal = signal[(session.lfp_times >= trial[0]) &
                          (session.lfp_times < trial[1])]
    baseline_signal = signal[(session.lfp_times >= base[0]) &
                          (session.lfp_times < base[1])]

    spec, psd_t = scipy.signal.welch(trial_signal, session.sampling_rate,
                                   nperseg=250)

    spec, psd_b = scipy.signal.welch(baseline_signal, session.sampling_rate,
                                   nperseg=250)

    ax.plot(spec, (psd_t - psd_b)/psd_b, 'k', linewidth=0.2)

ax.set_ylabel('power ($uV^{2}/Hz$)')
sns.despine()
plt.tight_layout()




time_before_stim_in_s = 0
time_after_stim_in_s = 0.5
# VISUAL TRIALS
trial_numbers = session.select_trials(only_correct=False, trial_type='X')
visual_trial_times = session.get_aligned_times(trial_numbers,
                                     time_before_in_s=time_before_stim_in_s,
                                     time_after_in_s=time_after_stim_in_s)
# VISUAL BASELINES
visual_baseline_times = session.get_aligned_times(trial_numbers,
                                     time_before_in_s=baseline_start_before_stim_in_s,
                                     time_after_in_s=baseline_stop_before_stim_in_s)

trial_ind = 14
nperseg = 250
noverlap = nperseg-1

trial_signal = signal[(session.lfp_times >= visual_trial_times[trial_ind][0]) &
                      (session.lfp_times < visual_trial_times[trial_ind][1])]
baseline_signal = signal[(session.lfp_times >= visual_baseline_times[trial_ind][0]) &
                         (session.lfp_times < visual_baseline_times[trial_ind][1])]

spec, psd_b = scipy.signal.welch(baseline_signal, session.sampling_rate,
                                 nperseg=nperseg)

vals, t_spec, x_spec = scipy.signal.spectrogram(trial_signal,
                                                fs=session.sampling_rate,
                                                window='hanning', nperseg=nperseg,
                                                noverlap=noverlap, mode='psd')

# NORMALIZE BY BASELINE:
x_spec = (x_spec - psd_b.reshape(-1, 1)) / psd_b.reshape(-1, 1)
x_mesh, y_mesh = np.meshgrid(t_spec, vals[vals < max_freq])

f, ax = plt.subplots(1, 1, figsize=[4, 3])
me = ax.pcolormesh(x_mesh - time_before_stim_in_s, y_mesh,
                   (x_spec[vals < max_freq]),
                   cmap=matplotlib.cm.jet)#, vmin=vmin, vmax=vmax)
ax.set_ylabel('Frequency (Hz)')
ax.set_xlabel('Time [s]')
divider = make_axes_locatable(ax)
cax = divider.append_axes('right', size='5%', pad=0.05)
f.colorbar(me, cax=cax)
sns.despine(offset=8)
plt.tight_layout()

#plt.colorbar()




time_before_stim_in_s = 1
time_after_stim_in_s = 3
baseline_start_before_stim_in_s = 1
baseline_stop_before_stim_in_s = -0.5
trial_ind = 38


trial_numbers = session.select_trials(only_correct=False, trial_type='X')
visual_trial_times = session.get_aligned_times(trial_numbers,
                                     time_before_in_s=time_before_stim_in_s,
                                     time_after_in_s=time_after_stim_in_s)
# VISUAL BASELINES
visual_baseline_times = session.get_aligned_times(trial_numbers,
                                     time_before_in_s=baseline_start_before_stim_in_s,
                                     time_after_in_s=baseline_stop_before_stim_in_s)



trial_signal = signal[(session.lfp_times >= visual_trial_times[trial_ind][0]) &
                      (session.lfp_times < visual_trial_times[trial_ind][1])]
baseline_signal = signal[(session.lfp_times >= visual_baseline_times[trial_ind][0]) &
                         (session.lfp_times < visual_baseline_times[trial_ind][1])]

# Wavelet transform, i.e. scaleogram
import pywt

scales = np.arange(1, 512)

t = np.arange(-time_before_stim_in_s,time_after_stim_in_s, 1 / session.sampling_rate)
cwtmatr, freqs = pywt.cwt(trial_signal, scales=scales, wavelet="morl",
                          sampling_period = 1 / session.sampling_rate)

cwtmatr_b, freqs_b = pywt.cwt(baseline_signal, scales=scales, wavelet="morl",
                          sampling_period = 1 / session.sampling_rate)
cwtmatr_b = cwtmatr_b.mean(axis=1)

# norm = 1 / freqs
# f, ax = plt.subplots()
# ax.plot(norm)

#cwtmatr = (cwtmatr - cwtmatr_b.reshape(-1, 1)) #/ cwtmatr_b.reshape(-1, 1)

#cwtmatr = (cwtmatr - norm.reshape(-1, 1))/norm.reshape(-1, 1)

f, ax = plt.subplots(1, 1, figsize=[4, 3])
im2 = ax.pcolormesh(t, freqs, cwtmatr, cmap = "inferno" )
ax.set_ylim(0,50)
ax.set_ylabel("Frequency in [Hz]")
ax.set_xlabel("Time in [s]")
ax.set_title("Scaleogram using wavelet GAUS1")
plt.tight_layout()



from spectrum import data_cosine, dpss, pmtm

data = data_cosine(N=2048, A=0.1, sampling=1024, freq=200)
# If you already have the DPSS windows
[tapers, eigen] = dpss(2048, 2.5, 4)
res = pmtm(data, e=eigen, v=tapers, show=False)
# You do not need to compute the DPSS before end
res = pmtm(data, NW=2.5, show=False)
res = pmtm(data, NW=2.5, k=4, show=True)