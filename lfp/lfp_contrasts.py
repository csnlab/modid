import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from constants import *
from utils import to_long_form
from loadmat import *
from session import Session
from sklearn.model_selection import StratifiedKFold
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score, recall_score
from matplotlib.ticker import MaxNLocator
import pickle
from utils import shuffle_array_rows_within_labels
from mpl_toolkits.axes_grid1 import make_axes_locatable
from session_info import select_sessions

"""
This script generates plots of the lfp (raw, filtered, energy, or phase)
which contrast two subsets of trials. Raw lfp is always plotted, 
you can set the filtered_data_to_plot to specify which filtered data 
(filtered lfp, energy, or phase) you also want to plot. The contrasts
to plot are set with the split_bys parameter. 
"""



# set animal_id and session_id to None if you want to run for all sessions
animal_id = '2012'
session_id = '2018-08-14_14-30-15'

# SETTINGS NAME - to create output folder
settings_name = 'june6'

# SELECT CHANNELS
area_lfp = 'V1'
lfp_channel_id = None
layer_lfp = None
ch_in_the_middle_of_the_probe = True

# TIME PARAMETERS
binsize_in_ms = 50
time_before_stim_in_s = 0.5
time_after_stim_in_s = 1

# FILTERING PARAMETERS
lfp_downsample_factor = 5
transition_width = 5  # Hz
bandpass_attenuation = 60  # dB
low_freq = 3
mid_freq = 33
high_freq = 71
spacing_low = 10
spacing_high = 10
n_phase_bins = 4
n_energy_bins = 4

session = Session()

freq_bands = session.make_frequency_bands(low_freq, mid_freq, high_freq,
                                          spacing_low, spacing_high)

str_freqs = ['low_freq', 'high_freq']
low_freqs = freq_bands[0:3]
high_freqs = freq_bands[3:]
freqs = [low_freqs, high_freqs]
time_freqs = [1.2, 1.2]
binsize_freqs = [20, 6, 2]

print(freq_bands)

only_correct_trials = True
plot_colorbars = False

# if do not want to plot filtered data, set filtered_data_to_plot to []
filtered_data_to_plot = ['filtered_lfp', 'energy', 'phase']

# the generated plots already compare two sets of trial with different e.g.
# audio stimuli. split_bys is a list of the criteria to split trials
# The full list is the following:
# split_bys = ['audio_post_norm',
#              'visual_post_norm',
#              'auditory_change',
#              'visual_change']

split_bys = ['audio_post_norm']

time_before_stim_in_s = 0.5
time_after_stim_in_s = 0.5
binsize_raw = 50

plot_format = 'png'
dpi = 400
plot_folder = os.path.join(DATA_FOLDER,  'plots', 'lfp_exploratory', settings_name)

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder)

# -----------------------------------------------------------------------------


if animal_id is None and session_id is None:
    sessions = select_sessions(only_correct=only_correct_trials)

    sessions = sessions.drop_duplicates(subset=['animal_id', 'session_id'])


else:
    sessions = pd.DataFrame(columns=['animal_id', 'session_id'],
                            data=[(animal_id, session_id)])


for i, row in sessions.iterrows():

    animal_id = row['animal_id']
    session_id = row['session_id']

    session = Session(animal_id=animal_id, session_id=session_id)
    session.load_data()
    session.quick_downsample_lfp(factor=lfp_downsample_factor)

    freq_bands = session.make_frequency_bands(low_freq, mid_freq, high_freq, spacing_low, spacing_high)

    session.set_filter_parameters(freq_bands, transition_width, bandpass_attenuation)

    lfp_channel_id = session.get_random_channel_id(area=area_lfp,
                                               layer=layer_lfp,
                                               in_the_middle=ch_in_the_middle_of_the_probe)

    if len(filtered_data_to_plot) > 0:
        session.bandpass_filter(lfp_channel_id)


    for split_by in split_bys:

        if split_by == 'audio_post_norm':
            split_by_values = [[1, 2], [3, 4]]
            trial_type = 'Y'

        if split_by == 'auditory_change':
            split_by_values = [1, 2]
            trial_type = 'Y'

        if split_by == 'visual_post_norm':
            split_by_values = [[1, 2], [3, 4]]
            trial_type = 'X'

        if split_by == 'visual_change':
            split_by_values = [1, 2]
            trial_type = 'X'


        # --- RAW LFP ----------------------------------------------------------

        arrays, extents = [], []
        for j, split in enumerate(split_by_values) :

            arr, extent = session.get_data_for_time_trial_plot(
                data='lfp',
                lfp_channel_id=lfp_channel_id,
                trial_type=trial_type, only_correct_trials=only_correct_trials,
                time_before_stim_in_s=time_before_stim_in_s,
                time_after_stim_in_s=time_after_stim_in_s,
                binsize_in_ms=binsize_raw,  **{split_by:split})

            arrays.append(arr)
            extents.append(extent)

        vmin = np.vstack(arrays).min()
        vmax = np.vstack(arrays).max()

        f, axes = plt.subplots(1, 3, figsize=[3 * 3, 4], sharex=True)

        for j, split in enumerate(split_by_values):
            ax = axes[j]
            arr = arrays[j]
            extent = extents[j]
            ax.imshow(arr, extent=extent, origin='upper', aspect='auto',
                      vmin=vmin, vmax=vmax)
            ax.set_ylabel('Trials')
            ax.yaxis.set_major_locator(MaxNLocator(integer=True))
            sns.despine(offset=8)
            plt.tight_layout()
            ax.set_title('Raw lfp, {} {}'.format(split_by, split), fontsize=10)

            if plot_colorbars:
                divider = make_axes_locatable(ax)
                cax = divider.append_axes('right', size='5%', pad=0.05)
                f.colorbar(im, cax=cax)

        time = np.linspace(-time_before_stim_in_s, time_after_stim_in_s, arr.shape[1])
        for j, split in enumerate(split_by_values):
            ax = axes[2]
            arr = arrays[j]
            df = to_long_form(time, arr)
            sns.lineplot(x='time', y='values', data=df, ax=ax, label=split)

            #ax.plot(time, arr.mean(axis=0), label=split)
        axes[2].legend()
        axes[2].set_title('Raw lfp', fontsize=10)
        axes[2].set_ylabel('mV')

        for ax in axes:
            ax.set_xlabel('Time [s]')

        plt.tight_layout(h_pad=1)

        name = '{}_{}_{}_raw_lfp_split_by_{}_lfpch_{}.{}'.format(animal_id,
                session_id, area_lfp, split_by, lfp_channel_id, plot_format)

        plot_path = os.path.join(plot_folder, name)
        f.savefig(plot_path, dpi=dpi)



        # --- FILTERED LFP / PHASE / ENERGY -----------------------------------

        for data in filtered_data_to_plot:
            for strf, bands, secs, binsize in zip(str_freqs, freqs, time_freqs, binsize_freqs):

                arrays = {str(b) : {} for b in bands}
                extents = {str(b) : {} for b in bands}

                for i, band in enumerate(bands):
                    for j, split in enumerate(split_by_values):
                        arr, extent = session.get_data_for_time_trial_plot(data=data,
                                                                           lfp_channel_id=lfp_channel_id,
                                                                           freq_band=band,
                                                                           trial_type=trial_type, only_correct_trials=only_correct_trials,
                                                                           discretize_phase=False, discretize_energy=True,
                                                                           n_phase_bins=4, n_energy_bins=8,
                                                                           time_before_stim_in_s=secs, time_after_stim_in_s=secs,
                                                                           binsize_in_ms=binsize, **{split_by:split})
                        arrays[str(band)][j] = arr
                        extents[str(band)][j] = extent

                f, axes = plt.subplots(len(bands), 2, figsize=[2 * 3, len(bands) * 2],
                                       sharex=True, sharey=False)

                for i, band in enumerate(bands) :
                    vmin = np.vstack((arrays[str(band)][0], arrays[str(band)][1])).min()
                    vmax = np.vstack((arrays[str(band)][0], arrays[str(band)][1])).max()

                    for j, split in enumerate(split_by_values) :
                        ax = axes[i, j]

                        arr = arrays[str(band)][j]
                        extent = extents[str(band)][j]

                        im = ax.imshow(arr, extent=extent, origin='upper', aspect='auto',
                                       vmin=vmin, vmax=vmax)
                        ax.set_ylabel('Trials')

                        ax.yaxis.set_major_locator(MaxNLocator(integer=True))
                        sns.despine(offset=8)
                        plt.tight_layout()
                        ax.set_title('Band: {}-{} Hz, {} {}'.format(band[0], band[1], split_by, split),
                                     fontsize=10)

                        if plot_colorbars:
                            divider = make_axes_locatable(ax)
                            cax = divider.append_axes('right', size='5%', pad=0.05)
                            f.colorbar(im, cax=cax)

                axes[-1, 0].set_xlabel('Time [s]')
                axes[-1, 1].set_xlabel('Time [s]')

                plt.tight_layout(h_pad=3)

                name = '{}_{}_{}_{}_split_by_{}_lfpch_{}_{}.{}'.format(
                    animal_id, session_id, area_lfp, data, split_by, lfp_channel_id, strf,
                    plot_format)

                plot_path = os.path.join(plot_folder, name)
                f.savefig(plot_path, dpi=dpi)



