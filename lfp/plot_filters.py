import os
from constants import *
from session import Session


# FILTERING PARAMETERS
filter_type = 'kaiser'
butterworth_order = 2
transition_width = 20  # Hz
bandpass_attenuation = 60  # dB
low_freq = 3
mid_freq = 43
high_freq = 83
spacing_low = 10
spacing_high = 10

save_plots = False
plot_format = 'png'
dpi = 400
plot_folder = os.path.join(DATA_FOLDER,  'plots', 'filters')

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder)

session = Session()
session.sampling_rate = 1000

freq_bands = session.make_frequency_bands(low_freq, mid_freq, high_freq,
                                          spacing_low, spacing_high)

session.set_filter_parameters(freq_bands, transition_width,
                              bandpass_attenuation, order=butterworth_order)

session.make_filters(filter_type=filter_type)

# PLOT FREQUENCY RESPONSE
f = session.plot_frequency_response_filters()
if save_plots:
    plot_name = 'freq_response_ba_{}_tw_{}_sl_{}_sh_{}.{}'.format(bandpass_attenuation,
                                                               transition_width,
                                                               spacing_low,
                                                               spacing_high,
                                                               plot_format)
    f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)

# PLOT IMPULSE RESPONSE
f2 = session.plot_impulse_response_filters()
if save_plots:
    plot_name = 'imp_response_ba_{}_tw_{}_sl_{}_sh_{}.{}'.format(bandpass_attenuation,
                                                               transition_width,
                                                               spacing_low,
                                                               spacing_high,
                                                               plot_format)
    f2.savefig(os.path.join(plot_folder, plot_name), dpi=dpi)