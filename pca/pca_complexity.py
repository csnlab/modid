from constants import *
from loadmat import *
from session import Session
import quantities as pq
from plotting_style import *
import matplotlib.pyplot as plt
from session_info import select_sessions
from sklearn.decomposition import PCA

settings_name = 'May21'

animal_id = None
session_id = None
min_units = 20

# animal_id = '2003'
# session_id = '2018-02-07_14-28-57'
# area_spikes = 'V1'


# TIME PARAMETERS
align_event = 'stimChange'
sliding_window = True
time_before_stim_in_s = 0.4
time_after_stim_in_s = 1.4
binsize_in_ms = 200
slide_by_in_ms = 50


# plotting parameters
ci=95
plot_format = 'png'
dpi = 400
save_plots = True

# TODO collect pars and save as well

# --- SET UP PATHS ------------------------------------------------------------

plot_folder = os.path.join(DATA_FOLDER, 'plots', 'PCA_complexity', settings_name)

if not os.path.isdir(plot_folder):
    os.makedirs(plot_folder, exist_ok=True)



# --- IDENTIFY SESSIONS -------------------------------------------------------

if session_id is None:
    sessions = select_sessions(min_units=min_units, min_perc_correct=30)


    sessions = sessions.drop_duplicates(subset=['animal_id', 'session_id' ,'area'])

    if animal_id is not None:
        sessions = sessions[sessions['animal_id'] == animal_id]


else:
    sessions = pd.DataFrame(columns=['animal_id', 'session_id', 'area'],
                            data=[(animal_id, session_id, area_spikes)])


# --- RUN TCA -----------------------------------------------------------------

response = (0, 1)
modality = ['X', 'Y']
stimulus = (1, 2, 3, 4)
stimulus_change = [1, 2]


df = pd.DataFrame(columns=['animal_id', 'session_id', 'area',
                           'time', 'n_units', 'varexp_ratio'])


for i, row in sessions.iterrows():

    animal_id = row['animal_id']
    session_id = row['session_id']
    area_spikes = row['area']


    session = Session(animal_id=animal_id, session_id=session_id)
    session.load_data(load_lfp=False)


    trial_numbers = session.select_trials(response=response,
                                          trial_type=modality)


    trial_times = session.get_aligned_times(trial_numbers,
                                            time_before_in_s=time_before_stim_in_s,
                                            time_after_in_s=time_after_stim_in_s,
                                            event=align_event)

    binned_spikes, spike_bin_centers = session.bin_spikes_per_trial(
                                            binsize_in_ms, trial_times,
                                            sliding_window=sliding_window,
                                            slide_by_in_ms=slide_by_in_ms)

    selected_unit_ind = session.select_units(area=area_spikes)
    binned_spikes = [s[selected_unit_ind, :] for s in binned_spikes]


    n_time_bins_per_trial = spike_bin_centers[0].shape[0]
    time_bin_times = (spike_bin_centers[0] - spike_bin_centers[0][0]).rescale(pq.s) \
                     + (binsize_in_ms*pq.ms).rescale(pq.s)/2 - time_before_stim_in_s*pq.s
    time_bin_times = time_bin_times.__array__().round(5)


    for time_bin in range(n_time_bins_per_trial):
        time = time_bin_times[time_bin]
        S = np.vstack([s[:, time_bin] for s in binned_spikes])
        n_units = S.shape[1]

        pca = PCA(n_components=min_units)
        pca.fit(S)

        row = [animal_id, session_id, area_spikes, time, n_units, None]
        ind = df.shape[0]
        df.loc[ind, :] = row
        df.loc[ind, 'varexp_ratio'] = pca.explained_variance_ratio_



# --- PLOT --------------------------------------------------------------------

variance_threshold = 0.9
n_components_variance = 3

df_sel = df.copy()

nc = []

for x in df_sel['varexp_ratio']:
    try:
        n = np.where(x.cumsum() > variance_threshold)[0][0]
    except IndexError:
        n = np.nan
    nc.append(n)

df_sel['nc'] = nc
df_sel['ve'] = [x.cumsum()[n_components_variance-1] for x in df_sel['varexp_ratio']]

df_sel['nc'] = df_sel['nc'] / df_sel['n_units']
df_sel['ve'] = df_sel['ve'] / df_sel['n_units']

df_sel = df_sel.drop('varexp_ratio', axis=1)

numeric_cols = ['nc', 've']
for col in numeric_cols:
    df_sel[col] = pd.to_numeric(df_sel[col])



f, ax = plt.subplots(1, 1, figsize=[1.4*small_square_side, small_square_side])

sns.lineplot(data=df_sel, x='time', y='nc', hue='area', hue_order=AREAS,
             palette=[area_palette[a] for a in AREAS],
             markers=True, style='area', ax=ax, ci=ci)

ax.get_legend().remove()
ax.set_xlabel('Time (s)')
ax.axvline(0, c='grey', ls=':')

ax.set_ylabel('# PCA components (as fraction of\n number of units) required\n to explain '
                 '{}% of variance'.format(int(variance_threshold*100)))
sns.despine()
ax.legend(bbox_to_anchor=(1.04, 0.5), frameon=False).texts[0].set_text("")
plt.tight_layout(rect=(0, 0, 0.8, 1))

plot_name = 'PCA_complexity_over_time_settings_{}.{}'.format(settings_name,
                                                             plot_format)
f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi,
            bbox_inches="tight")



# f, ax = plt.subplots(1, 2, figsize=[2.4*small_square_side, small_square_side])
#
# sns.lineplot(data=df_sel, x='time', y='ve', hue='area', hue_order=AREAS,
#              palette=[area_palette[a] for a in AREAS],
#              markers=True, style='area', ax=ax[0], ci=ci)
#
# sns.lineplot(data=df_sel, x='time', y='nc', hue='area', hue_order=AREAS,
#              palette=[area_palette[a] for a in AREAS],
#              markers=True, style='area', ax=ax[1], ci=ci)
# for axx in ax:
#     axx.get_legend().remove()
#     axx.set_xlabel('Time (s)')
#     axx.axvline(0, c='grey', ls=':')
# ax[0].set_ylabel('Variance explained by top\n {} '
#                  'PCA components'.format(n_components_variance))
# ax[1].set_ylabel('# PCA components (as fraction of\n number of units)required\n to explain '
#                  '{}% of variance'.format(int(variance_threshold*100)))
# sns.despine()
# ax[1].legend(bbox_to_anchor=(1.04, 0.5), frameon=False).texts[0].set_text("")
# plt.tight_layout(rect=(0, 0, 0.88, 1))
#
# plot_name = 'PCA_complexity_over_time_settings_{}.{}'.format(settings_name,
#                                                              plot_format)
# f.savefig(os.path.join(plot_folder, plot_name), dpi=dpi,
#             bbox_inches="tight")
