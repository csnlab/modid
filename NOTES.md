
Everything is in microseconds

- For MODID: ‘X’ is visual change, ‘Y’ is audio change,
‘C’ is conflict trial (both change), ‘P’ is probe trial (none change).


In order to restrict the
analysis only to units actually ‘responding’ to the acoustic stimulus, we discarded units providing less
than 1/Δt bit of information (corresponding to an information rate of 1 bit/sec).



We bin spikes in xx ms bins. Then the phase and energy are linearly
interpolated at the centers of the spike bins, then discretized.


- SHUFFLE PHASE PER FREQUENCY
This means that the information contained in the phase is in itself
maintained (if phase was contributing independent information, e.g.
with a certain audio frequency the phase gets set in a certain way),
but we destroy the interaction between spikes and lfp phase.

# [NEW VERSION OF THE DATA]
in lfpData, you now have
- a better estimation of layer
- lfpgood and hasMovemenArtefacts to subselect channels
- HF_PWR and L4SingNormDepth (not necessary)
in SpikeData
- layer per cell
- should I use lfpgood and hasMovementArtefacts for the single cells?
- cell type
- burst metric


# Meeting monday 19
- finish TCA
- look at phase coding
- multi area
- transformation from 1st to 2nd bump
- other avenues to expand? (jorrit)


