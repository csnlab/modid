import numpy as np
import pandas as pd
import seaborn as sns
from constants import *
from loadmat import *
from session import Session

animal_id = '2012'
session_id = '2018-08-14_14-30-15'

binsize_in_ms = 20
transition_width = 1  # Hz
bandpass_attenuation = 60  # dB
low = 4
mid = 12
high = 18
spacing_low = 4
spacing_high = 6
n_phase_bins = 4
n_energy_bins = 4
time_before_stim_in_s = 0
time_after_stim_in_s = 1

# SELECT TRIALS
only_correct_trials = True
trial_type = 'Y'
auditory_change=None

# SELECT UNITS
area_spikes = 'V1'
min_isolation_distance = 10
min_coverage = 0.9
max_perc_isi_spikes = 1

# SELECT CHANNELS
area_lfp = 'V1'
n_channels = 4

# SELECT TARGET VARIABLE
target_name = 'audioFreqPostNorm'




# TODO
lfp_channel_ids = ['10', '15', '18']
# -----------------------------------------------------------------------------

# TODO select area

session = Session(animal_id=animal_id, session_id=session_id)
session.load_data()
session.quick_downsample_lfp(factor=3)

freq_bands = session.make_frequency_bands(low, mid, high, spacing_low, spacing_high)
session.set_filter_parameters(freq_bands, transition_width, bandpass_attenuation)

for indx in lfp_channel_ids:

    session.bandpass_filter(indx)

# TODO is 0 no split?
trial_numbers = session.select_trials(only_correct=only_correct_trials,
                                      trial_type=trial_type,
                                      auditory_change=auditory_change)

trial_times = session.get_aligned_times(trial_numbers,
                                        time_before_in_s=time_before_stim_in_s,
                                        time_after_in_s=time_after_stim_in_s)

binned_spikes, bin_spike_centers = session.bin_spikes_per_trial(binsize_in_ms,trial_times)

interp_phase, interp_energy = session.interpolate_phase_and_energy_per_trial(bin_spike_centers)

# TODO there seems to be something wrong with the energy
binned_energy = session.discretize_energy(interp_energy, n_bins=n_energy_bins)

binned_phase = session.discretize_phase(interp_phase, n_bins=n_phase_bins)

selected_unit_ind = session.select_units(area=area_spikes,
                                         min_isolation_distance=min_isolation_distance,
                                         min_coverage=min_coverage,
                                         max_perc_isi_spikes=max_perc_isi_spikes)

n_time_bins_per_trial = bin_spike_centers[0].shape[0]



cell_ids = session.cell_id[selected_unit_ind]
S = np.hstack(binned_spikes).T[:, selected_unit_ind]
P = np.hstack(binned_phase).T
F = session.make_target_per_time_point(trial_numbers, target_name=target_name,
                                      n_time_bins_per_trial=n_time_bins_per_trial)

df = pd.DataFrame(columns=['unit_id', 'lfp_freq', 'info_measure', 'value'])


for j, id in enumerate(cell_ids):

    print('Computing information for unit {}'.format(id))
    x = S[:, j]

    for k, band in enumerate(freq_bands):
        print(' - Considering LFP in the {} Hz frequency band'.format(band))

        y = P[:, k]


        def compute_info(var1, var2, estimator, n_surr=10, conditional=False,
                         condvar=None, base1=None, base2=None, condbase=None,
                         kraskov=1):