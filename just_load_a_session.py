import numpy as np
import pandas as pd
from session import Session
from constants import *

animal_id = '2012'
session_id = '2018-08-14_14-30-15'

# session = Session(animal_id=animal_id, session_id=session_id,
#                   load_lfp=False, load_spikes=True)

for animal_id in decoding_sessions.keys():
    for session_id in decoding_sessions[animal_id]:
        session = Session(animal_id=animal_id, session_id=session_id)
        session.load_data(load_lfp=False, load_lfp_lazy=False, load_spikes=False)

        print(session.trial_data[session.trial_data['trialType'] == 'X'].shape[0],
              session.trial_data[session.trial_data['trialType'] == 'Y'].shape[0])



        selected_unit_ind = session.select_units(area='V1')
        selected_unit_id = session.get_cell_id(selected_unit_ind,
                                               shortened_id=False)

        cell_layer = session.get_cell_layer(selected_unit_id)

        print(pd.value_counts(cell_layer))



import numpy as np
import pandas as pd
from session import Session
from constants import *
import matplotlib.pyplot as plt
from sklearn.preprocessing import LabelEncoder

animal_id = '2009'
session_id = '2018-08-23_13-20-25'

# session = Session(animal_id=animal_id, session_id=session_id,
#                   load_lfp=False, load_spikes=True)

for animal_id in decoding_sessions.keys():
    for session_id in decoding_sessions[animal_id]:
        session = Session(animal_id=animal_id, session_id=session_id)
        session.load_data(load_lfp=False, load_lfp_lazy=False, load_spikes=False)

        trial_numbers = session.select_trials(only_correct=True,
                                              trial_type=['X', 'Y'])

        y1 = session.make_target(trial_numbers, target_name='responseSide',
                                coarse=True)
        y1 = LabelEncoder().fit_transform(y1)

        y2 = session.make_target(trial_numbers, target_name='trialType',
                                coarse=True)
        y2 = LabelEncoder().fit_transform(y2)

        f, ax = plt.subplots(1, 1)
        ax.plot(y1)
        ax.plot(y2)
        selected_unit_ind = session.select_units(area='V1')
        selected_unit_id = session.get_cell_id(selected_unit_ind,
                                               shortened_id=False)

        #cell_layer = session.get_cell_layer(selected_unit_id)

        #print(pd.value_counts(cell_layer))