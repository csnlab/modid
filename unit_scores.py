import numpy as np
from sklearn.model_selection import StratifiedKFold
from sklearn.model_selection import KFold
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score
from sklearn.metrics import roc_auc_score
import pandas as pd
import rpy2
from rpy2.robjects import pandas2ri


class SingleUnitDiscriminationScores():

    def __init__(self, decoder='random_forest', n_estimators=100):

        self.decoder = decoder
        self.n_estimators = n_estimators


    def make_decoder(self):

        if self.decoder == 'random_forest':
            dec = RandomForestClassifier(n_estimators=self.n_estimators)
        else:
            raise ValueError

        return dec


    def performance_metric(self, y_test, y_pred):

        return accuracy_score(y_true=y_test, y_pred=y_pred)


    def compute_dec_performance(self, X, y, n_splits, kfold_shuffle=True,
                                random_state=None):

        if random_state is not None:
            kf = KFold(n_splits=n_splits, shuffle=kfold_shuffle,
                       random_state=random_state)
        else:
            kf = KFold(n_splits=n_splits, shuffle=kfold_shuffle)

        scores = []
        for fold, (training_ind, testing_ind) in enumerate(kf.split(X)):

            X_train = X[training_ind, :]
            X_test = X[testing_ind, :]

            y_train = y[training_ind]
            y_test = y[testing_ind]

            dec = self.make_decoder()

            dec.fit(X_train, y_train)
            y_pred = dec.predict(X_test)

            score = self.performance_metric(y_test, y_pred)

            scores.append(score)

        return np.mean(scores)



    def compute_jackknife_scores(self, X, y, n_splits=3,
                                random_state=92, return_population_score=False):

        # same random state so that whatever splits we use to compute
        # the performance of all neurons, we also use to compute the
        # performance after removing individual selected_unit_id

        unit_scores = np.zeros(X.shape[1])

        score_all_neurons = self.compute_dec_performance(X, y, n_splits=n_splits,
                                                         kfold_shuffle=True,
                                                         random_state=random_state)

        for unit in range(X.shape[1]):
            X_jack = np.delete(X, unit, axis=1)

            score_all_minus_one = self.compute_dec_performance(X_jack, y, n_splits=n_splits,
                                                         kfold_shuffle=True,
                                                         random_state=random_state)
            unit_scores[unit] = score_all_neurons - score_all_minus_one


        if return_population_score:
            return score_all_neurons, unit_scores
        else:
            return unit_scores
    # TODO GET POPULATION SCORE



    def compute_importance_scores(self, X, y, n_estimators, random_state,
                                  return_population_score=False):

        forest = RandomForestClassifier(n_estimators=n_estimators,
                                        random_state=random_state,
                                        oob_score=return_population_score)
        forest.fit(X, y)
        unit_scores = forest.feature_importances_

        if return_population_score:
            return forest.oob_score_, unit_scores
        else:
            return unit_scores



    def compute_auc_scores_r(self, X, y):

        auc_ci = '''unit_scores
        auc_ci = function(df) {
          library(pROC)
          rocobj = roc(df$y_true, df$y_score, direction='>')
          if (rocobj$auc[1] < 0.5) {
            rocobj = roc(df$y_true, df$y_score, direction='<')
            }  
          CI = ci.auc(rocobj, conf.level=0.95, method="delong")
          out = c(rocobj$auc[1], CI[1], CI[3])
          return(out)
          }
        '''

        rpy2.robjects.r(auc_ci)
        r_f = rpy2.robjects.globalenv['auc_ci']
        pandas2ri.activate()

        unit_scores = np.zeros(X.shape[1])

        for i in range(X.shape[1]):
            spikes = X[:, i]
            df = pd.DataFrame(columns=['y_true', 'y_score'])
            df['y_true'] = y
            df['y_score'] = spikes
            auc, ci1, ci2 = pandas2ri.conversion.rpy2py(r_f(df))

            unit_scores[i] = auc

        return unit_scores


    def compute_auc_scores(self, X, y, debias_with_shuffle=False, n_shuffles=500):

        unit_scores = np.zeros(X.shape[1])

        for i in range(X.shape[1]):
            spikes = X[:, i]

            score = roc_auc_score(y, spikes)
            if score < 0.5:
                score = 1 - score
            unit_scores[i] = score

        if debias_with_shuffle:
            unit_scores_shuffle = np.zeros([n_shuffles, X.shape[1]])

            for j in range(n_shuffles):
                ys = np.random.permutation(y)

                for i in range(X.shape[1]):
                    spikes = X[:, i]
                    score = roc_auc_score(ys, spikes)
                    if score < 0.5:
                        score = 1 - score
                    unit_scores_shuffle[j, i] = score

            p_vals = [(unit_scores_shuffle[:, k] >= unit_scores[k]).sum() / n_shuffles
                      for k in range(unit_scores.shape[0])]

            unit_scores_deb = unit_scores - unit_scores_shuffle.mean(axis=0)

        if debias_with_shuffle:
            return unit_scores, unit_scores_deb, p_vals
        else:
            return unit_scores, None



# rpy2.robjects.r(auc_ci)
# r_f = rpy2.robjects.globalenv['auc_ci']
# pandas2ri.activate()
#
# unit_scores = np.zeros(X.shape[1])
#
# for i in range(X.shape[1]):
#
#     spikes = X[:, i]
#     df = pd.DataFrame(columns=['y_true', 'y_score'])
#     df['y_true'] = y
#     df['y_score'] = spikes
#     auc, ci1, ci2 = pandas2ri.conversion.rpy2py(r_f(df))
#
#     unit_scores[i] = auc
