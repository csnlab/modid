import numpy as np
import pandas as pd
from session import Session
from constants import *
import quantities as pq
from midiutil import MIDIFile
import elephant
import scipy
from sklearn.preprocessing import minmax_scale

animal_id = '2003'
session_id = '2018-02-07_14-28-57'

session = Session(animal_id=animal_id, session_id=session_id)
session.load_data(load_lfp=True, load_lfp_lazy=False, load_spikes=True)

#channel_id = session.lfp_data['channel_ID'][0]

# --- PICK START AND END TIME -------------------------------------------------
# pick 10 random (but consecutive) trials, t1 is 6 seconds before stim change
# of first trial
trial_numbers = session.select_trials()
trial_numbers = trial_numbers[10:20]

sel_trial_ind = np.isin(session.trial_data['trialNum'], trial_numbers)
selected_trial_data = session.trial_data.loc[sel_trial_ind, :]

time_before_in_s, time_after_in_s = 6, 6
time_before_in_us = time_before_in_s * 1e6
time_after_in_us = time_after_in_s * 1e6
t1 = selected_trial_data['stimChange'].iloc[0] - time_before_in_s
t2 = selected_trial_data['stimChange'].iloc[-1] - time_after_in_us
stim_change_times = selected_trial_data['stimChange']
time_in_minutes = ((t2 - t1)*pq.us).rescale(pq.min)
print('You have selected {:.2f} mins of data'.format(time_in_minutes))
trial_times = [[t1, t2]]



# --- BIN SPIKES --------------------------------------------------------------
binned_spikes_all, spike_bin_centers = session.bin_spikes_per_trial(binsize_in_ms=1,
                                                                trial_times=trial_times,
                                                                sliding_window=False)
binned_spikes_all = [np.clip(s, 0, 1) for s in binned_spikes_all]
pyramidal_unit_ind = session.select_units(area='V1', celltype=1)
interneuron_unit_ind = session.select_units(area='V1', celltype=2)
binned_spikes_pyr = binned_spikes_all[0][pyramidal_unit_ind, :]
binned_spikes_int = binned_spikes_all[0][interneuron_unit_ind, :]
binned_spikes_pyr = binned_spikes_pyr[:, 3000:-3000]
binned_spikes_int = binned_spikes_int[:, 3000:-3000]

# --- TIMING CONVERSIONS ------------------------------------------------------

time_elapsed = ((t2*pq.us-3*pq.s) - (t1*pq.us+3*pq.s)).rescale(pq.min)

8.04 * pq.min / time_elapsed

ticks_per_quarternote = 24
microseconds_per_beat = 500000
tick_duration = ((microseconds_per_beat / ticks_per_quarternote)*pq.us).rescale(pq.ms)

total_time = (binned_spikes_pyr.shape[1] * tick_duration).rescale(pq.min)

total_time / time_elapsed

# --- BIN EVENT TIMES ---------------------------------------------------------
spike_bin_centers = spike_bin_centers[0][3000:-3000]
event_times = selected_trial_data['stimChange'].values * pq.us
trial_types = selected_trial_data['trialType'].values

audio_change_binned = np.zeros(binned_spikes_int.shape[1])
visual_change_binned = np.zeros(binned_spikes_int.shape[1])

for t, trial_type in zip(event_times[1:-1], trial_types[1:-1]):
    ind = np.argmin(np.abs(spike_bin_centers-t))
    print(t, ind, trial_type)
    if trial_type == 'X':
        visual_change_binned[ind] = 1
    elif trial_type == 'Y':
        audio_change_binned[ind] = 1
    elif trial_type == 'C':
        visual_change_binned[ind] = 1
        audio_change_binned[ind] = 1

# --- PROCESS LFP -------------------------------------------------------------
transition_width = 1  # Hz
bandpass_attenuation = 60  # dB
channel_ids = session.select_channels(area='V1')[5]
lfp_slice = session.slice_lfp_by_time(channel_ids, trial_times)[0][0]

bands = [[0.5, 4], [4, 8], [8, 12], [12, 30], [30, 60]]
filtered_lfp, lfp_energy = [], []
for lowcut, highcut in bands:
    y, b = session._bandpass_filter(lfp_slice, lowcut, highcut,
                                    session.sampling_rate,
                                    bandpass_attenuation, transition_width)
    analytic_signal = scipy.signal.hilbert(y)
    energy = np.square(np.abs(analytic_signal))
    filtered_lfp.append(y)
    lfp_energy.append(energy)

lfp_slice = lfp_slice[3000:-3000]
filtered_lfp = [l[3000:-3000] for l in filtered_lfp]
lfp_energy = [l[3000:-3000] for l in lfp_energy]


np.save('raw_lfp.npy', lfp_slice)
np.save('filtered_lfp.npy', np.array(filtered_lfp))
np.save('lfp_power.npy', np.array(lfp_energy))
np.save('binned_spikes_pyramidal.npy', binned_spikes_pyr)
np.save('binned_spikes_interneurons.npy', binned_spikes_int)


# --- PLOT --------------------------------------------------------------------

import matplotlib.pyplot as plt
import seaborn as sns
f, ax = plt.subplots(7, 2, figsize=[10, 20])
ax[0, 0].imshow(binned_spikes_pyr, aspect='auto')
ax[0, 1].imshow(binned_spikes_int, aspect='auto')
ax[1, 0].plot(lfp_slice)
for i, lfp in enumerate(filtered_lfp):
    ax[i+2, 0].plot(lfp)


for i, energy in enumerate(lfp_energy):
    ax[i + 2, 1].plot(energy)
plt.tight_layout()
sns.despine()


# --- LFP TO VELOCITY ---------------------------------------------------------

filtered_lfp_vel = [minmax_scale(s, [0, 127]).astype(int) for s in filtered_lfp]
lfp_energy_vel = [minmax_scale(s, [0, 127]).astype(int) for s in lfp_energy]


# ---- MAKE MIDI FILE SPIKES --------------------------------------------------
n_times = binned_spikes_pyr.shape[1]

tempo=120
for neuron_type, binned_spikes in zip(['pyramidal', 'interneuron'],
                                       [binned_spikes_pyr, binned_spikes_int]):

    n_neurons = binned_spikes.shape[0]
    ticks_per_quarternote = 24

    MyMIDI = MIDIFile(numTracks=n_neurons,
                      removeDuplicates=False,
                      deinterleave=False,
                      adjust_origin=True,
                      ticks_per_quarternote=ticks_per_quarternote,
                      eventtime_is_ticks=True)

    lfp_controllers = np.arange(len(bands)) + 20
    lfp_energy_controllers = np.arange(len(bands)) + 30
    # MAKE SURE WE START AT 0
    MyMIDI.addNote(track=0, channel=0, pitch=80, time=0, duration=1,
                   volume=100)

    for nind in range(n_neurons):

        MyMIDI.addTempo(nind, 0, 120)
        spiketimes = np.where(binned_spikes[nind, :])[0]

        for i, time in enumerate(spiketimes):
            MyMIDI.addNote(track=nind, channel=0, pitch=60, time=time, duration=1,
                           volume=100)

            for controller, signal in zip(lfp_controllers, filtered_lfp_vel):
                MyMIDI.addControllerEvent(track=nind, channel=0, time=time,
                                          controller_number=controller, parameter=signal[time])


            for controller, signal in zip(lfp_energy_controllers, lfp_energy_vel):
                MyMIDI.addControllerEvent(track=nind, channel=0, time=time,
                                          controller_number=controller, parameter=signal[time])


    with open('v1_spikes_{}.mid'.format(neuron_type), 'wb') as output_file:
        MyMIDI.writeFile(output_file)



# --- LFP STUFF ---------------------------------------------------------------

ticks_per_quarternote = 24


MyMIDI = MIDIFile(numTracks=len(bands),
                  removeDuplicates=False,
                  deinterleave=False,
                  adjust_origin=True,
                  ticks_per_quarternote=ticks_per_quarternote,
                  eventtime_is_ticks=True)


for miditrack, (signal, energy) in enumerate(zip(filtered_lfp_vel, lfp_energy_vel)):

    MyMIDI.addTempo(miditrack, 0, 120)

    MyMIDI.addNote(track=miditrack, channel=0, pitch=60, time=0, duration=n_times,
                   volume=100)

    for time in range(n_times)[0::10]:

        MyMIDI.addControllerEvent(track=miditrack, channel=0, time=time,
                                  controller_number=20, parameter=signal[time])


    for time in range(n_times)[0::10]:

        MyMIDI.addControllerEvent(track=miditrack, channel=0, time=time,
                                  controller_number=30, parameter=energy[time])

with open("v1_lfp.mid", "wb") as output_file:
    MyMIDI.writeFile(output_file)



# ---- MAKE MIDI FILE EVENTS --------------------------------------------------

n_times = visual_change_binned.shape[0]
ticks_per_quarternote = 24

MyMIDI = MIDIFile(numTracks=1,
                  removeDuplicates=False,
                  deinterleave=False,
                  adjust_origin=True,
                  ticks_per_quarternote=ticks_per_quarternote,
                  eventtime_is_ticks=True)

MyMIDI.addNote(track=0, channel=3, pitch=40, time=0,
               duration=1,
               volume=100)

for midichan, pitch, binned_events in zip([0, 1], [60, 80], [visual_change_binned, audio_change_binned]):

    eventtimes = np.where(binned_events)[0]

    for i, time in enumerate(eventtimes):
        MyMIDI.addNote(track=0, channel=midichan, pitch=pitch, time=time, duration=1,
                       volume=100)

with open('v1_events.mid', 'wb') as output_file:
    MyMIDI.writeFile(output_file)



